#WEBGL

Pour une expérience optimale, utilisez le navigateur Google Chrome dans une résolution 1920x1080 (full HD)

Utilisation des dernières technologies web permettant d'exploiter au maximum les performances des navigateurs :


- ThreeJS - Gestion complexe d'un environnement 3D pour navigateurs (récents)
- Firebase - Accès aux bases de données en temps réel pour gérer les données de l'utilisateur de manière fiable et sécurisé grâce aux technologies Google.
- Indico - Exploitation d'une intelligence artificielle basée sur un réseau de neuronnes pour restituer la personnalité de l'utilisateur.
 
