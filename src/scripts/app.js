import AppController from './controllers/AppController';

window.MODELS = [];
window.SHADERS = [];
window.JSONMODELS = [];
window.PLANETS = [];
document.body.onload = initApp();
document.addEventListener('contextmenu', (event) => {event.preventDefault()});

let debug = true;

if(!debug){
    console.log = () => {};
    console.info = () => {};
}else{
    console.log("debug mode ", debug);
}


function initApp() {
    console.warn('Team Jaune © WebGL');
    let selector = document.querySelector('#app');
    new AppController({
        selector: selector
    });
}