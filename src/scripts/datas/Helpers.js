export const getAstroSign = (day, month) => {
    if((month === 1 && day <= 20) || (month === 12 && day >=22)) {
        return 'capricorne';
    } else if ((month === 1 && day >= 21) || (month === 2 && day <= 18)) {
        return 'verseau';
    } else if((month === 2 && day >= 19) || (month === 3 && day <= 20)) {
        return 'poisson';
    } else if((month === 3 && day >= 21) || (month === 4 && day <= 20)) {
        return 'belier';
    } else if((month === 4 && day >= 21) || (month === 5 && day <= 20)) {
        return 'taureau';
    } else if((month === 5 && day >= 21) || (month === 6 && day <= 20)) {
        return 'gemeaux';
    } else if((month === 6 && day >= 21) || (month === 7 && day <= 22)) {
        return 'cancer';
    } else if((month === 7 && day >= 23) || (month === 8 && day <= 23)) {
        return 'lion';
    } else if((month === 8 && day >= 24) || (month === 9 && day <= 23)) {
        return 'vierge';
    } else if((month === 9 && day >= 24) || (month === 10 && day <= 23)) {
        return 'balance';
    } else if((month === 10 && day >= 24) || (month === 11 && day <= 22)) {
        return 'scorpion';
    } else if((month === 11 && day >= 23) || (month === 12 && day <= 21)) {
        return 'sagittaire';
    }
    return false;
};

export const getFamilyByAstroSign = (astroSign) => {
    if (astroSign === 'belier' || astroSign === 'lion' || astroSign === 'sagittaire') {
        return 'feu';
    } else if (astroSign === 'taureau' || astroSign === 'vierge' || astroSign === 'capricorne') {
        return 'terre';
    } else if (astroSign === 'gemeaux' || astroSign === 'balance' || astroSign === 'verseau') {
        return 'air';
    } else if (astroSign === 'cancer' || astroSign === 'scorpion' || astroSign === 'poisson') {
        return 'eau';
    }
    return false;
};