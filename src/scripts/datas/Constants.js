export default {
    ASTRO: ['belier', 'taureau', 'gemeaux', 'cancer', 'lion', 'vierge', 'balance', 'scorpion', 'sagittaire', 'capricorne', 'verseau', 'poisson'],
    PATHS: {
        users: 'users/',
        verseau: 'users/verseau/',
        poisson: 'users/poisson/',
        belier: 'users/belier/',
        taureau: 'users/taureau/',
        gemeaux: 'users/gemeaux/',
        cancer: 'users/cancer/',
        lion: 'users/lion/',
        vierge: 'users/vierge/',
        balance: 'users/balance/',
        scorpion: 'users/scorpion/',
        sagittaire: 'users/sagittaire/',
        capricorne: 'users/capricorne/',
        count: 'count/'
    },
    FAMILYSIGN: {
        'air': ['gemeaux', 'balance', 'verseau'],
        'eau': ['cancer', 'scorpion', 'poisson'],
        'feu': ['belier', 'lion', 'sagittaire'],
        'terre': ['taureau', 'vierge', 'capricorne']
    },
    TIME: {
        MONTHS: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    },
    DESC: {
        'terre': 'Dans la symbolique, la famille Terre suit celle du Feu. \nElle concrétise les conquêtes du feu, les rendant durables, aménageables en matière et en formes.',
        'air': 'Dans la symbolique, la famille Air suit celle de la Terre. Après que la terre ait donné des formes concrètes à l\'énergie du feu, voici l\'esprit donné aux choses : les signes d\'air symbolisent l\'intellect, les échanges avec autrui, les arts.',
        'eau': 'L\'eau entraîne l\'air logique et rationnel vers l\'univers du ressenti, des émotions, des sensations et de l\'instinct.',
        'feu': 'Dans la symbolique, la famille Feu suit celle de l\'Eau. On dit souvent des signes de feu qu\'ils ont un sacré caractère ou un sacré tempérament.'
    },
    FAMILIES: ['feu', 'terre', 'air', 'eau'],
    LIGHT: ['La vallée crépusculaire', 'L\'éclat lumineux', 'La brume obscure'],
    TRADS: {
        'agreeableness': 'Amabilité',
        'conscientiousness': 'Conscientieux',
        'extraversion': 'Extraverti',
        'openness': 'Ouvert d\'esprit'
    },
    SUPPLEMENTS: {
        'terre' : {
            'agreeableness': 'Montagne',
            'conscientiousness': 'Montagne',
            'extraversion': 'Cratère',
            'openness': 'Cratère'
        },
        'air' : {
            'agreeableness': 'Nuage violet',
            'conscientiousness': 'Nuage violet',
            'extraversion': 'Tornade',
            'openness': 'Tornade'
        },
        'eau' : {
            'agreeableness': 'Formes acqueuses',
            'conscientiousness': 'Formes acqueuses',
            'extraversion': 'Lac d\'eau',
            'openness': 'Lac d\'eau'
        },
        'feu' : {
            'agreeableness': 'Volcan',
            'conscientiousness': 'Volcan',
            'extraversion': 'Rocher',
            'openness': 'Rocher'
        }
    },
    COLORS: {
        eau: ['#78A6AD','#547D8E','#0F3942'],
        feu: ['#E04C4A','#7A3236','#42252C'],
        terre: ['#89C4A0','#429168','#0A4F2B'],
        air: ['#AF9DE8','#6C6693','#402968']
    }
}