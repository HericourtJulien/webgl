class User {

    constructor(datas) {
        if (datas) {
            this.updateUser(datas);
        } else {
            console.error('Classe User : Construct without datas.');
            return false;
        }

        return this;
    }

    updateUser(datas) {
        if (datas && datas.id) {
            this.id = datas.id || '';
            this.birthdate = datas.birthdate || '';
            this.name = datas.name || '';
            this.astro = datas.astro || '';
            this.family = datas.family || '';
            this.light = datas.light || '';
            this.lightColor = datas.lightColor || '';
            this.personality = datas.personality || '';
            this.particles = datas.particles || '';
            this.modelId = datas.modelId || '';
            this.finished = datas.finished || false;
            this.attributs = datas.attributs || '';

            this.updateLocalStorage();
        }
    }

    updateLocalStorage() {
        window.localStorage.setItem('byo-id', this.id);
        window.localStorage.setItem('byo-astro', this.astro);
    }

    removeLocalUser() {
        window.localStorage.clear();
    }

}

export default User;
