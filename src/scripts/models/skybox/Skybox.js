import * as THREE from 'three';
import vertexShader from '../../../shaders/compiled/galaxy/shader.galaxyFog.vert';
import fragmentShader from '../../../shaders/compiled/galaxy/shader.galaxyFog.frag';

class Skybox{

    constructor(params){

        this.path = params.path ? params.path : "";
        this.skyboxTexture = params.skyboxTexture ? params.skyboxTexture : null;
        this.datGui = params.datGui ? params.datGui : null;
        this.type = params.type ? params.type : "cube";
        this.hasShader = params.shader ? params.shader : false;
        this.uniforms = null;
        this.shaderScaleFactor = params.shaderScaleFactor ? params.shaderScaleFactor : 1;
        this.textured = params.textured ? params.textured : false;
        this.texture = params.texture ? params.texture : null;


        this.t0 = Date.now();

        if(this.hasShader){
            this.uniforms = params.uniforms ? params.uniforms : console.error("no uniforms for the sky box");
        }

        if(this.type === "sphere"){
            this.createSkySphere();
        }else{
            this.createSkyBox();
        }

        //this.createShadedPlane();

    }

    /**
     * create a skybox shaded planet
     */
    createShadedPlane(){
        const geometry = new THREE.PlaneBufferGeometry(2, 2, 2);

        this.uniforms = {
            u_time: { type: "f", value: 1.0 },
            u_resolution: { type: "v2", value: new THREE.Vector2(window.innerWidth,window.innerHeight) },
            u_mouse: { type: "v2", value: new THREE.Vector2() }
        };

        const customShader = new THREE.ShaderMaterial({
            uniforms: this.uniforms,
            vertexShader: vertexShader,
            fragmentShader: fragmentShader
        });

        this.skybox.add(new THREE.Mesh(geometry, customShader));
    }

    /**
     * setting up the sky box
     */
    createSkyBox(){

        let reflectionCube = this.createSkyBoxMap();

        reflectionCube.format = THREE.RGBFormat;

        let shader = THREE.ShaderLib[ "cube" ];
        shader.uniforms[ "tCube" ].value = reflectionCube;

        let material = new THREE.ShaderMaterial( {
            fragmentShader: shader.fragmentShader,
            vertexShader: shader.vertexShader,
            uniforms: shader.uniforms,
            depthWrite: false,
            side: THREE.BackSide,
            //blending: THREE.AdditiveBlending
        });

        this.skybox = new THREE.Mesh(new THREE.BoxGeometry(100, 100, 100), material);
        this.scaleIt();
        //this.scene.add(this.skybox);
    }

    /**
     * applying textures to the skybox
     */
    createSkyBoxMap(){

        if(this.path === ""){
            console.error("no path setted for the skybox");
            return;
        }

        /**
         px (positive x) : right
         nx (negative x) : left
         py (positive y) : top
         ny (negative y) : bottom
         pz (positive z) : back
         nz (negative z) : front
         */
        const urls = [
            this.skyboxTexture.right,
            this.skyboxTexture.left,
            this.skyboxTexture.top,
            this.skyboxTexture.bottom,
            this.skyboxTexture.back,
            this.skyboxTexture.front,
        ];

        return new THREE.CubeTextureLoader().setPath(this.path).load( urls );
    }

    /**
     * create a sky sphere with a shader
     */
    createSkySphere(){
        this.skybox = window.JSONMODELS["skyboxSphere"];

        if(this.hasShader && this.skybox !== undefined){

            this.vertexShader = window.SHADERS[0]["others"]["galaxy"].vertexShader;
            this.fragmentShader = window.SHADERS[0]["others"]["galaxy"].fragmentShader;
            this.shaderScaleFactor = window.SHADERS[0]["others"]["galaxy"].shaderScaleFactor;

            /*console.log(this.vertexShader);
            console.log(this.fragmentShader);
            console.log(this.shaderScaleFactor);*/

            THREE.ImageUtils.crossOrigin = '';

            let shader = new THREE.ShaderMaterial({
                vertexShader : this.vertexShader,
                fragmentShader : this.fragmentShader,
                uniforms: this.uniforms,
                side: THREE.DoubleSide
            });

            //hard replace
            this.skybox = new THREE.Mesh(new THREE.IcosahedronGeometry(10, 4), shader);

            this.scaleIt();

            console.info("sky sphere shader applied");
            console.log(this.skybox);

        }else if(this.textured && this.texture !== null){

            let textureLoader = new THREE.TextureLoader();
            let texture = textureLoader.load(this.texture);

            let mat = new THREE.MeshPhongMaterial({
                map: texture,
                side: THREE.BackSide
            });

            this.skybox = new THREE.Mesh(new THREE.IcosahedronGeometry(10, 4), mat);
            this.scaleIt();
        }
    }

    scaleIt(){
        if(this.shaderScaleFactor){
            this.skybox.scale.x = this.skybox.scale.x * this.shaderScaleFactor;
            this.skybox.scale.y = this.skybox.scale.y * this.shaderScaleFactor;
            this.skybox.scale.z = this.skybox.scale.z * this.shaderScaleFactor;
        }
    }

    /**
     * updating datGui interface for quickly live editing params
     */
    updateDatGui() {

        if(this.datGui){
            let guiConfig = {
                speed: this.speed,
                scale: this.skybox.scale.x
            };

            let planetOptions = this.datGui.addFolder("Planet parameters");

            planetOptions.add(guiConfig, 'scale', 0.1, 2.5).onChange((v) => {
                this.skybox.scale.x = v;
                this.skybox.scale.y = v;
                this.skybox.scale.z = v;
            }).step(0.005).name('Scale');

            if(this.speed){
                planetOptions.add(guiConfig, 'speed', 0, 0.5).onChange((val) => {
                    this.speed = val;
                }).step(0.005).name('Speed');
            }
        }
    }

    place(scene) {
        scene.add(this.skybox);
    }

    render() {
        if(this.hasShader){
            this.uniforms.time.value = (0.00025 * (Date.now() - this.t0)).toFixed(5);
        }
    }
}

export default Skybox;