import * as THREE from 'three';

class Light {

    constructor(params) {

        //getting camera from parameters
        this.camera = params.camera || console.error("no camera passed in Light.js");

        //adding a spotlight
        this.spotLight = new THREE.SpotLight({color: 0xFFFFFF});
        this.spotLight.position.set(0, 0, 100);

        //setting spotlight parameters
        this.spotLight.castShadow = true;
        this.spotLight.penumbra = 0.8;
        this.spotLight.shadow.camera.near = 5;
        this.spotLight.shadow.camera.far = 80;
        this.spotLight.shadow.camera.fov = 10;

        this.spotLight.angle = Math.PI/4;
        this.spotLight.intensity = 0.0;
        this.spotLight.target = new THREE.Object3D();
        this.spotLight.target.position.set(0, 0, 0);

        //adding an ambient light to be able to see all elements present in the scene
        this.ambientLight = new THREE.AmbientLight({color:0xffffff, intensity:0.01});
        this.ambientLight.position.set(0, 0, 0);

        //datGui settings
        this.datGui = params.datGui || null;

        this.updateDatGui();
    }

    updateDatGui(){

        if(this.datGui){
            let guiConfig = {
                intensity: this.spotLight.intensity,
                ambientIntensity: this.ambientLight.intensity
            };

            let lightOptions = this.datGui.addFolder("Light parameters");

            lightOptions.add(guiConfig, 'intensity', 0.0, 15.0).onChange((val) => {
                this.spotLight.intensity = val
            }).step(0.01).name('light intensity');

            lightOptions.add(guiConfig, 'ambientIntensity', 0.0, 5.0).onChange((val) => {
                this.ambientLight.intensity = val
            }).step(0.01).name("ambient intensity");
        }
    }

    //rendering spotlight with the camera position
    render(){
        this.spotLight.position.copy(this.camera.position);
        this.spotLight.target.position.copy(this.camera.orbitController.target);
    }

    //place it on the scene
    place(scene) {
        scene.add(this.spotLight, this.spotLight.target, this.ambientLight);
    }

}

export default Light;