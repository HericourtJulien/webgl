import * as THREE from 'three';

class Camera {

    constructor(params) {

        this.datGui = params.datGui;

        this.mainCamera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 5000);
        this.mainCamera.position.x = params.position.x;
        this.mainCamera.position.y = params.position.y;
        this.mainCamera.position.z = params.position.z;

        //this.updateDatGui();

        return this;
    }

    getCamera(){
        return this.mainCamera;
    }

    getPosition(){
        return this.mainCamera.position;
    }

    updateDatGui() {

        let guiConfig = {
            cameraX: this.mainCamera.position.x,
            cameraY: this.mainCamera.position.y,
            cameraZ: this.mainCamera.position.z
        };

        this.datGui.add(guiConfig, 'cameraX', -200,200).onFinishChange((v) => {
            this.mainCamera.position.x = v;
        }).step(10);

        this.datGui.add(guiConfig, 'cameraY', -20,20).onFinishChange((v) => {
            this.mainCamera.position.y = v;
        }).step(1);

        this.datGui.add(guiConfig, 'cameraZ', -20,20).onFinishChange((v) => {
            this.mainCamera.position.z = v;
        }).step(1);
    }

    updateAspectRatio(){
        this.mainCamera.aspect = window.innerWidth / window.innerHeight;
        this.mainCamera.updateProjectionMatrix();
    }

}

export default Camera;