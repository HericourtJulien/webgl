import * as THREE from 'three';

class Cube {

    constructor(params) {
        let geometry = new THREE.BoxGeometry(1, 1, 1);
        let material = new THREE.MeshLambertMaterial({color: params.color || 0xe33b3b});

        this.mesh = new THREE.Mesh( geometry, material );

        if(params !== undefined ){
            if(params.position !== undefined) {
                this.mesh.position.x = params.position.x;
                this.mesh.position.y = params.position.y;
                this.mesh.position.z = params.position.z;
            }
        }
    }

    place(scene) {
        scene.add(this.mesh);
    }

    render() {
        this.mesh.position.y -= 0.001;
    }

}

export default Cube;
