import * as THREE from 'three';
import Planet from './Planet';
import THREEx from '../../libs/threex/THREEx';

class GlowingPlanet extends Planet{

    constructor(params){
        super(params);

        this.name = params.name ? params.name : console.error("no planet name specified -> CAN'T LOAD FILE");
        this.createJsonObject(window.JSONMODELS[this.name]);

        this.glowingMesh = this.glowingMesh.length ? this.glowingMesh : null;
    }

    /**
     * create object based on its json file
     * @param object
     */
    createJsonObject(object){

        if(!object){
            console.error("can not create json object : ", object);
            return;
        }

        this.mesh = object.clone();//not similar for .dae files

        //if need to apply a double sided normals to the meshs

            for(let child of this.mesh.children){

                if(this.doubleSided){
                    child.material.side = THREE.DoubleSide;
                }
            }


        if(this.transparentMeshes && this.transparentMeshes.length > 0){
            super.setTransparentMesh();
        }

        if(this.texture && this.texturedMesh){
            //this.createTexture();
        }

        //this.makeItGlow();
        super.updateDatGui();
        super.addItOnGalaxy();
    }

    /**
     * updating dat gui with glow parameters
     */
    updateDatGui(obj){

        if(this.datGui){
            let guiConfig = {
                coeff: this.glow,
                power: this.glowPower,
                outGlow: this.outGlow,
                outPow: this.outPow,
                inColor: this.glowColor,
                outColor: this.outGlowColor
            };

            let planetOptions = this.datGui.__folders.GlowingPlanet ? this.datGui.__folders.GlowingPlanet : this.datGui.addFolder("GlowingPlanet");

            planetOptions.add(guiConfig, 'coeff', 0.1, 5).onChange((v) => {
                this.coeff = v;
                obj.children[0].material.uniforms.coeficient.value = v;
            }).step(0.005).name('In Glow');

            planetOptions.add(guiConfig, 'power', 0.1, 5).onChange((v) => {
                this.power = v;
                obj.children[0].material.uniforms.power.value = v;
            }).step(0.005).name('In Power');

            /*****color*****/
            planetOptions.addColor(guiConfig, 'outColor').onChange((v) => {
                this.outGlowColor = new THREE.Color(v);
                obj.children[0].material.uniforms.glowColor.value = new THREE.Color(v);
            }).name('In Glow');



            planetOptions.add(guiConfig, 'outGlow', 0.1, 5).onChange((v) => {
                this.outGlow = v;
                obj.children[1].material.uniforms.coeficient.value = v;
            }).step(0.005).name('Out Glow');

            planetOptions.add(guiConfig, 'outPow', 0.1, 5).onChange((v) => {
                this.outPow = v;
                obj.children[1].material.uniforms.power.value = v;
            }).step(0.005).name('Out Power');

            /*****color*****/
            planetOptions.addColor(guiConfig, 'inColor').onChange((v) => {
                this.glowColor = new THREE.Color(v);
                obj.children[1].material.uniforms.glowColor.value = new THREE.Color(v);
            }).name('Out Glow');
        }
    }

    /**
     * make the mesh glow ! WOW so amazing -> using ThreeX lib remastered by Benjamin
     */
    makeItGlow(){

        if(!this.glowColor){
            console.error("no glownig color passed");
            return;
        }

        if(!this.outGlowColor){
            console.error("no out glowing color passed");
        }

        let meshWillGlow = super.getMesh(this.glowingMesh);//get the glowing mesh

        this.glowingMesh = THREEx.geometricGlowMesh(
            meshWillGlow,
            this.glowColor,
            this.outGlowColor,
            this.glow,
            this.glowPower,
            this.outGlow,
            this.outPow
        );//setting it's glowing effect

        this.mesh.add(this.glowingMesh);
        this.mesh.remove(meshWillGlow);

        console.info("it glow");

        this.updateDatGui(this.glowingMesh);

        if(this.scaleFactor){
            this.applyScaleFactor();
        }
    }

    /**
     * if there is a texture, apply it
     */
    createTexture(){
        THREE.ImageUtils.crossOrigin = '';

        let textureLoader = new THREE.TextureLoader();
        let baseTexture = textureLoader.load(this.texture);
        let bumpTexture;

        if(this.bumpMap){
            bumpTexture = textureLoader.load(this.bumpMap);
        }

        //setting elements to use texture for the planet
        let texturedMesh = super.getMesh(this.texturedMesh); //getting the mesh to textured

        if(!texturedMesh){
            console.error("no texture mesh found");
        }

        //create a new texture
        //not same material type as the Textured Planet -> not in the parent class
        let textureMaterial = new THREE.MeshLambertMaterial({
            //bumpMap: bumpTexture,
            map: baseTexture,
            shininess: 0,
            transparent : this.meshOpacity < 1, //raccourci de la mort qui tue,
            opacity: this.meshOpacity,
            depthWrite: false
            //bumpScale: this.bumpScaleFactor,
        });

        if(this.textureMeshScale){
            this.applyScaleFactor(texturedMesh);
        }

        texturedMesh.geometry.computeVertexNormals();
        texturedMesh.material = textureMaterial;

    }

    applyScaleFactor(obj = this.glowingMesh){

        if(!this.glowingMesh){
            console.error("can't apply scale on glowing mesh, it's null");
            return;
        }

        obj.scale.x = this.textureMeshScale;
        obj.scale.y = this.textureMeshScale;
        obj.scale.z = this.textureMeshScale;
    }

}

export default GlowingPlanet;