import * as THREE from 'three';
import Planet from './Planet';

class TexturedPlanet extends Planet{

    constructor(params) {
        super(params);

        this.name = params.name ? params.name : console.error("no planet name specified -> CAN'T  LOAD FILE");

        this.createJsonObject(window.JSONMODELS[this.name]);
    }


    /**
     * create object based on json file (not exactly the same as dae file -> containing a 3d scene mesh)
     * @param object from json source
     */
    createJsonObject(object){

        if(!object){
            console.error("can not create json object : ", object);
            return;
        }

        this.mesh = object.clone();//not similar for .dae files

        //if need to apply a double sided normals to the meshs
        if(this.doubleSided){
            for(let child of this.mesh.children){
                child.material.side = THREE.DoubleSide;
            }
        }

        if(this.transparentMeshes && this.transparentMeshes.length > 0){
            super.setTransparentMesh();
        }

        //this.createTexture();
        super.updateDatGui();
        super.addItOnGalaxy();
    }

    /**
     * apply a custom texture to the object
     */
    createTexture(){

        if(!this.texture){
            console.error("no texture passed");
            return;
        }

        if(!this.bumpMap){
            //not necessary need a bump map
            console.warn("no bum map passed");
        }

        if(!this.texturedMesh){
            console.error("no texturedMesh passed");
            return;
        }

        THREE.ImageUtils.crossOrigin = '';

        let textureLoader = new THREE.TextureLoader();
        let baseTexture = textureLoader.load(this.texture);
        let bumpTexture;

        if(this.bumpMap){
            bumpTexture = textureLoader.load(this.bumpMap);
        }

        //setting elements to use texture for the planet
        let texturedMesh = super.getMesh(this.texturedMesh); //getting the mesh to textured

        if(!texturedMesh){
            console.error("no texture mesh found");
        }

        //create a new texture
        let textureMaterial = new THREE.MeshPhongMaterial({
            bumpMap: bumpTexture,
            map: baseTexture,
            shininess: 0,
            transparent: this.meshOpacity < 1, //raccourci de la mort qui tue,
            opacity: this.meshOpacity,
            bumpScale: this.bumpScaleFactor,
            blending: THREE.AdditiveBlending
        });


        texturedMesh.geometry.computeVertexNormals();
        texturedMesh.material.needsUpdate = true;
        texturedMesh.material = textureMaterial;

        console.log(texturedMesh.material);
    }

}
export default TexturedPlanet;
