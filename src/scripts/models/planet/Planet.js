import * as THREE from 'three';
import TWEEN from 'tween.js';

class Planet {

    /**
     * CONSTRUCTOR
     *
     * full param object :
     * params = {
            galaxy: object,
            placeThisPlaneteOnMyGalaxy: callback,
            galaxyOnPick: callback,
            isPickable: bool,

            name: string,
            clock: object,
            datGui: object (parent.datGui),
            id: int,
            planetType: string,
            applyShaderToMesh: string,
            doubleSided: bool,
            replaceMesh: bool to replace mesh or no

            //for others shader only (see ShaderToLoad
            baseUniforms: object,
            needsSceneLight: bool,

            //shader parameters
            hasShader: bool,
            specificShader : string,
            uniforms: object,
            renderUniforms: bool
        }
     *
     * @param params -> object with a lot of different properties, used to add shader, add a corresponding galxy ...
     */
    constructor(params) {
        this.galaxy = params.galaxy ? params.galaxy : null;
        this.placeThisPlaneteOnMyGalaxy = params.placeThisPlaneteOnMyGalaxy ? params.placeThisPlaneteOnMyGalaxy : null ;
        this.galaxyOnPick = params.onPick ? params.onPick : null;
        this.isPickable = params.isPickable ? params.isPickable : false;

        //Gui parameters
        this.datGui = params.datGui || null;
        //this.deltaSpeed = params.clock.getDelta() ||null;
        //this.speed = this.deltaSpeed ? this.deltaSpeed : null;

        //init empty childs array
        this.childs = [];

        //initialise use-full variable for collision detection if needed
        this.geometry = null;
        this.planetBbox = null;
        this.detectCollisions = params.detectCollisions ? params.detectCollisions : false;

        //planet type and ID to load corresponding shaders
        this.planetType = params.planetType ? params.planetType : "none";
        this.applyShaderToMesh = params.applyShaderToMesh ? params.applyShaderToMesh : "";
        this.planetId = params.id ? params.id : null;

        //shader parameters
        this.hasShader = params.hasShader ? params.hasShader : true;
        this.renderUniforms = params.renderUniforms ? params.renderUniforms : false;
        this.specificShader = params.specificShader ? params.specificShader : null;
        this.uniforms = params.uniforms ? params.uniforms : {};
        //initialising variable for shader process
        this.finalUniforms = null;
        this.useSceneLight = false;
        this.shaderTexture = '';

        //other parameters for rendering meshs
        this.mesh = null; //redefine in childs classes
        this.doubleSided = params.doubleSided ? params.doubleSided : false;
        this.replaceMesh = params.replaceMesh ? params.replaceMesh : null;
        this.shaderOpacity = params.shaderOpacity ? params.shaderOpacity : 1;
        this.fogedMesh = params.fogedMesh ? params.fogedMesh : null;
        this.transparentMeshes = params.transparentMeshes ? params.transparentMeshes : null;

        //parameters for textured mesh
        this.texturedMesh = params.texturedMesh ? params.texturedMesh : null;
        this.texture = params.texture ? params.texture : null;
        this.bumpMap = params.bumpMap ? params.bumpMap : null;
        this.bumpScaleFactor = params.bumpScaleFactor ? params.bumpScaleFactor : 1;
        this.meshOpacity = params.meshOpacity ? params.meshOpacity : 1;

        //parameters for glowing planets
        this.glowingMesh = params.glowingMesh ? params.glowingMesh : null;
        this.glowColor = params.glowColor ? params.glowColor : null;
        this.outGlowColor = params.outGlowColor ? params.outGlowColor : null;
        this.textureMeshScale = params.textureMeshScale ? params.textureMeshScale : null;
        this.glow = params.glow ? params.glow : 0.1;
        this.glowPower = params.glowPower ? params.glowPower : 1.2;
        this.outGlow = params.outGlow ? params.outGlow : 0.1;
        this.outPow = params.outPow ? params.outPow : 1.2;
        this.scaleFactor = params.scaleFactor ? params.scaleFactor : 1;

        this.rotate = false;

        //for values that needs a time update (uniforms)
        this.t0 = Date.now();

        //Planet creation properly set in Child class ShadedPlanet and TExturedPlanet

        /*if(params.name === undefined || params.name === null){
            console.error("set a planet name to load the corresponding file");
        }else{
            this.name = params.name;

            //this.createObject(window.MODELS[this.name]); //only for collada models
            this.createJsonObject(window.JSONMODELS[this.name]); //for json models
        }*/
    }

    /**
     * getMesh -> get corresponding mesh associated with a name
     * @param name
     * @param obj -> object to run
     * @returns {*} -> mesh
     */
    getMesh(name, obj = this.mesh){

        if(!name){
            console.error("no name passsed for checking the mesh to add a shader");
            return;
        }

        const self = this;

        if(this.mesh.name === name){
            return obj;
        }else{
            if(obj.children.length > 0){
                for(let child of obj.children){
                    if(child.name === name){
                        return child;
                    }else{
                        self.getMesh(name, child);
                    }
                }
            }else{
                return false;
            }
        }
    }

    /**
     * if a galaxy is specified, add custom parameters to the planet to let them be selectable
     */
    addItOnGalaxy(){
        //galaxy processing
        if(this.galaxy){
            this.placeThisPlaneteOnMyGalaxy(this.mesh);
        }else{
            this.mesh.position.x = 0;
            this.mesh.position.y = 0;
            this.mesh.position.z = 0;
        }

        if(this.isPickable){
            window.mainController.elementsPickables.push(this.mesh);
            this.mesh.onPick = (this.galaxyOnPick) ? this.galaxyOnPick.bind(this, this.mesh) : this.onPick.bind(this);
        }
    }

    /**
     * set a bounding sphere to the current planet object
     * @param obj -> object to run
     * @param key -> property to get for setting the bounding sphere by coumputing it
     */
    setBoundingSphere(obj, key) {
        const self = this;

        if(obj.hasOwnProperty(key)){
            this.geometry = obj[key];
        }else{
            if(obj.children.length > 0){
                for(let child of obj.children){
                    if(child.hasOwnProperty(key)){
                        //now returning only the mesh geometry
                        if(child.type === "Mesh"){
                            //get the correct geometry
                            self.geometry = child.geometry;
                            //compute the boounding sphere
                            self.geometry.computeBoundingSphere();
                            //set it as a boundingBox to detect collisions
                            self.planetBbox = new THREE.Sphere(self.mesh.position, self.geometry.boundingSphere.radius);
                            //self.planetBbox = new THREE.Box3().setFromObject(self.mesh);
                            //self.addChilds();
                            //console.log(self.planetBbox);
                        }
                    }else{
                        self.setBoundingSphere(child, key);
                    }
                }
            }else{
                console.warn("no " + key +" property in the passed object - Planet.setBoundingSphere() |can't set a boundingSphere");
            }
        }
    }

    /**
     * set a bounding element (box by default) to the current planet
     * @param object
     * @param type
     */
    setBoundingBox(object, type){

        if(type === "sphere"){
            object.geometry.computeBoundingSphere();
            object.collider = new THREE.Sphere(object.position, object.geometry.boundingSphere.radius);
        }else{
            object.collider = new THREE.Box3().setFromObject(object.mesh);
        }
    }

    /**
     * add a child to the current mesh
     */
    addChilds(){
        for(let child of this.childs) {
            this.mesh.add(child.mesh);
            this.setBoundingBox(child);
        }
    }

    onPick(){
        console.log("onPick : "+ this.mesh.name);
    }

    /**
     * updating datGui interface for quickly live editing params
     */
    updateDatGui() {

        if(this.datGui){
            let guiConfig = {
                speed: this.speed,
                scale: this.mesh.scale.x,
                positionX: this.mesh.position.x,
                positionY: this.mesh.position.y,
                positionZ: this.mesh.position.z,
            };

            let planetOptions = this.datGui.__folders.Planet ? this.datGui.__folders.Planet : this.datGui.addFolder("Planet");


            planetOptions.add(guiConfig, 'scale', 1.0, 6.0).onChange((v) => {
                this.mesh.scale.x = v;
                this.mesh.scale.y = v;
                this.mesh.scale.z = v;
            }).step(0.1).name('Scale');

            planetOptions.add(guiConfig, 'positionX', -30.0, 30.0).onChange((v) => {
                this.mesh.position.x = v;
            }).step(0.1).name('position X');

            planetOptions.add(guiConfig, 'positionY', -30.0, 30.0).onChange((v) => {
                this.mesh.position.y = v;
            }).step(0.1).name('position Y');

            planetOptions.add(guiConfig, 'positionZ', -30.0, 30.0).onChange((v) => {
                this.mesh.position.z = v;
            }).step(0.1).name('position Z');

            if(this.speed){
                planetOptions.add(guiConfig, 'speed', 0, 0.5).onChange((val) => {
                    this.speed = val;
                }).step(0.005).name('Speed');
            }
        }
    }

    /**
     * adding transparency to the passed meshes
     */
    setTransparentMesh(){
        for(let currentMesh of this.transparentMeshes){
            let mesh = this.getMesh(currentMesh.name);

            mesh.material.transparent = true;
            mesh.material.opacity = currentMesh.opacity;
            //mesh.material.color = new THREE.Color(0xe33b3b);

            console.info("opacity applied to ", mesh);
        }
    }

    //place element on scene
    place(scene) {
        this.addChilds();
        scene.add(this.mesh);
        console.info("elements placed");
    }

    //if has childs, render them (allow multiple animations inside a composed mesh
    renderChilds(){
        //stuff to animate separately childs of the current mesh
        for(let child of this.childs) {
            child.render && child.render();
        }
    }

    /**
     * check collisions for each childs with the planet collider .0-> based on the mesh
     */
    checkCollisions(){
        for(let child of this.childs){
            //check if the child has a collide or no to
            if(child.collider !== undefined){
                //update boundingBox
                this.setBoundingBox(child);

                let collisionHappened = this.planetBbox.intersectsBox(child.collider);

                if(collisionHappened){
                    console.log("colliding");
                }
            }
        }
    }

    orientPlanet(orientationDegree, cb, easing, duration){

        console.log(orientationDegree);

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;
        let _orientationDegree = orientationDegree ? orientationDegree : -Math.PI/2;


        let coord = { rotation: 0 };
        new TWEEN.Tween(coord)
            .to({ rotation: _orientationDegree }, _duration)
            .easing(_easing)
            .onUpdate(() => {
                this.mesh.rotation.z = coord.rotation;
            })
            .onComplete( () => {
                cb && cb();
            })
            .start();

        this.mesh.updateMatrix();
    }

    enablePlanet(position, cb, easing, duration){

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;
        let meshPosition = this.mesh.position;

        let coords = { x: meshPosition.x, y: meshPosition.y, z: meshPosition.z, opacity: -5  };
        new TWEEN.Tween(coords)
            .to({ x: position.x ,y: position.y, z: position.z, opacity: 1 } ,_duration)
            .easing(_easing)
            .onUpdate(() => {
                this.mesh.position.x = coords.x;
                this.mesh.position.y = coords.y;
                this.mesh.position.z = coords.z;

                for(let child of this.mesh.children){
                    child.material.opacity = coords.opacity;
                }
            })
            .onComplete( () => {
                cb && cb();

            })
            .start();
    }

    hidePlanet( cb, easing, duration){

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;

        let coords = { opacity: 1  };
        new TWEEN.Tween(coords)
            .to({ opacity: 0 } ,_duration)
            .easing(_easing)
            .onUpdate(() => {

                for(let child of this.mesh.children){
                    child.material.opacity = coords.opacity;
                }
            })
            .onComplete( () => {
                cb && cb();

            })
            .start();
    }

    tweenPlanet(position, cb, easing, duration, isScale = false){

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;
        let param = isScale ? this.mesh.scale : this.mesh.position;

        new TWEEN.Tween(param)
            .to(new THREE.Vector3(position.x,position.y,position.z),_duration)
            .easing(_easing)
            .onComplete( () => {
                cb && cb();
            })
            .start();
    }

    shakePlanet(positions, cb, easing, duration){

        let _easing = easing || TWEEN.Easing.Bounce.InOut;
        let _duration = duration || 64;
        let param = this.mesh.position;

        // build the tween to go ahead
        let tweenHead	= new TWEEN.Tween(param)
            .to(+param.x, _duration)
            .delay(0)
            .easing(_easing)
            .onUpdate(() => {
                this.mesh.position.x = param.x;
            });

        // build the tween to go backward
        let tweenBack	= new TWEEN.Tween(param)
            .to(-param.x, _duration)
            .delay(0)
            .easing(_easing)
            .onUpdate(() => {
                this.mesh.position.x = param.x;
            });

        // after tweenHead do tweenBack
        tweenHead.chain(tweenBack);
        // after tweenBack do tweenHead, so it is cycling
        tweenBack.chain(tweenHead);

        // start the first
        tweenHead.start();

    }


    /**
     * called at each frame by our mani renderer in the AppController.js
     */
    render() {

        if(this.detectCollisions){
            this.checkCollisions();
        }

        TWEEN.update();

        this.renderChilds();

        this.mesh.rotation.y += this.rotate ? 0.001 : 0;

        //if we need to update the uniforms of the shaded mesh
        if(this.renderUniforms && this.finalUniforms !== null){
            this.finalUniforms.time.value = (0.00025 * (Date.now() - this.t0)).toFixed(5);
        }

    }
}
export default Planet;
