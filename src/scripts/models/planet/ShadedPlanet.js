import * as THREE from 'three';
import Planet from './Planet';

class ShadedPlanet extends Planet{

    constructor(params) {
        super(params);

        if(params.name === undefined || params.name === null){
            console.error("set a planet name to load the corresponding file");
        }else{
            this.name = params.name;

            //this.createObject(window.MODELS[this.name]); //only for collada models
            this.createJsonObject(window.JSONMODELS[this.name]); //for json models
            this.shaderTexture = params.shaderTexture ? params.shaderTexture : console.error("no shader texture passed");
        }
    }

    /**
     * create object based on json file (not exactly the same as dae file -> containing a 3d scene mesh)
     * @param object from json source
     */
    createJsonObject(object){

        if(!object){
            console.error("can not create json object : ", object);
            return;
        }

        this.mesh = object.clone();//not similar for .dae files

        //if need to apply a double sided normals to the meshs
        if(this.doubleSided){
            for(let child of this.mesh.children){
                if(child.name !== this.applyShaderToMesh){
                    child.material.side = THREE.DoubleSide;
                }
            }
        }

        //check if we need to apply a shader
        if(!this.hasShader){
            return;
        }


        if(this.planetType !== "none" && this.applyShaderToMesh !== ""){
            let shadedMesh = super.getMesh(this.applyShaderToMesh);

            //check if the mesh corresponding to the param "applyShaderToMEsh" exists
            if(shadedMesh){
                //this.addShader(shadedMesh);
            }else{
                console.error("no mesh found, can't apply shader");
            }

        }

        super.updateDatGui();
        super.addItOnGalaxy();
    }

    /**
     * add a shader to the planet mesh, based on specific mesh name and parameters for the shader
     * @param obj
     */
    addShader(obj){

        if(!this.applyShaderToMesh){
            console.warn("no specified mesh name to apply the shader");
            return;
        }

        if(!this.uniforms){
            console.warn("no uniforms passed for the shader");
            return;
        }

        if(this.planetType === ""){
            console.warn("no planet type specified for shader");
            return;
        }

        if(!this.planetId){
            console.warn("no planet id passed for shader, impossible to load associated shader");
            return;
        }

        this.settingUniforms();

        //allow origin access status for the shader image
        THREE.ImageUtils.crossOrigin = '';

        //CREATING NEW SHADER MATERIAL
        let customShader = new THREE.ShaderMaterial({
            vertexShader : this.vertexShader,
            fragmentShader : this.fragmentShader,
            uniforms: this.finalUniforms,
            side: THREE.FrontSide,
            lights: this.useSceneLight,
            name: this.planetType + "-planet-" +this.planetId,
            blending: THREE.AdditiveBlending
        });


        console.log("final uniforms");
        console.log(this.finalUniforms);
        //console.warn("replace mesh ?", this.replaceMesh);

        if(this.replaceMesh) {
            this.replaceShadedMesh(obj, customShader);
        }else{
            //check if the object have material to apply or not the shader
            if(obj.material){
                obj.material = customShader;

                this.shaderOpacity < 1 ? obj.material.transparent = true : obj.material.transparent = false;
                obj.material.opacity = this.shaderOpacity;

                console.info("mesh modified (NOT REPLACED ! )");

            }else{
                console.error("can't add shader, obj does not have material. default in the 3D object -> check it in Blender");
            }
        }

        console.log("shader applyed");
    }

    /**
     * setting all uniforms properties to apply when creating the shader
     */
    settingUniforms(){

        //defining all elements to create the shader (based on the global objects
        if(this.specificShader){

            //getting vertex and fragment shader
            this.vertexShader = window.SHADERS[0]["others"][this.specificShader].vertexShader;
            this.fragmentShader = window.SHADERS[0]["others"][this.specificShader].fragmentShader;
            this.shaderScaleFactor = window.SHADERS[0]["others"][this.specificShader].shaderScaleFactor;

            //setting lights parameters to use in the shader material
            if(window.SHADERS[0]["others"][this.specificShader].needsSceneLight){
                this.useSceneLight = true;

                this.finalUniforms = THREE.UniformsUtils.merge([
                    THREE.UniformsLib[ "ambient" ],
                    THREE.UniformsLib[ "lights" ],
                    this.uniforms
                ]);
            }else{
                this.finalUniforms = this.uniforms;
            }

        }else{
            //basic planet with its custom shader
            this.vertexShader = window.SHADERS[0][this.planetType]["planet"+this.planetId].vertexShader;
            this.fragmentShader = window.SHADERS[0][this.planetType]["planet"+this.planetId].fragmentShader;
            this.shaderScaleFactor = window.SHADERS[0][this.planetType]["planet"+this.planetId].shaderScaleFactor;

            this.finalUniforms = this.uniforms;
            this.finalUniforms.texture = {
                type: 't',
                value: new THREE.ImageUtils.loadTexture(this.shaderTexture)
            }
        }
    }

    /**
     * replace the mesh to apply a shader
     * @param obj - mesh getted from the json created object
     * @param shader - custom shader material
     */
    replaceShadedMesh(obj, shader){

        let replaceExistingMesh = new THREE.Mesh(new THREE.IcosahedronGeometry(10, 4), shader);

        //check if the object have material to apply or not the shader
        if(obj.material){
            //remove mesh corresponding to shaded mesh
            this.mesh.remove(obj);

            //if we have a scaling factor for the shader, applying it
            if(this.shaderScaleFactor && typeof (replaceExistingMesh) !== "undefined"){
                console.log("scale factor", this.shaderScaleFactor);

                replaceExistingMesh.scale.x = replaceExistingMesh.scale.x / this.shaderScaleFactor;
                replaceExistingMesh.scale.y = replaceExistingMesh.scale.y / this.shaderScaleFactor;
                replaceExistingMesh.scale.z = replaceExistingMesh.scale.z / this.shaderScaleFactor;
            }

            if(typeof replaceExistingMesh !== "undefined"){
                this.shaderOpacity < 1 ? replaceExistingMesh.material.transparent = true : replaceExistingMesh.material.transparent = false;
                replaceExistingMesh.material.opacity = this.shaderOpacity;
                replaceExistingMesh.name = this.applyShaderToMesh;

                //add to the existing "rest of the mesh" our custom shaded mesh
                this.mesh.add(replaceExistingMesh);
                console.info("mesh replaced");
            }else{
                console.info("no mesh replaced");
            }

        }else{
            console.error("can't add shader, obj does not have material. default in the 3D object -> check it in Blender");
        }
    }
}
export default ShadedPlanet;
