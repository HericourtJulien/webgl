import * as THREE from 'three';
import COLLADALoader from '../../loaders/COLLADALoader';

class Fruits {

    constructor(params) {
        this.datGui = params.datGui;
        this.deltaSpeed = params.clock.getDelta();
        this.speed = this.deltaSpeed;
        this.modelType = "";
        this.childs = [];
        this.collidables = [];
        this.fruit = params.fruit;

        if(this.fruit === undefined){
            console.error("set a fruit name in params");
        }else{
            let fruit = {
                loadingManager : params.loadingManager,
                callback : this.onColladaLoaded.bind(this),
                url : 'fruits/'+ this.fruit +'.dae'
            };

            new COLLADALoader(fruit);
        }

    }

    onColladaLoaded(object){
        this.mesh = object.scene;
        this.modelType = "collada";

        this.mesh.position.x = 0;
        this.mesh.position.y = 0;
        this.mesh.position.z = 0;

        for(let child of this.mesh.children){
            //two level of children generated by collada
            for(let subChild of child.children){
                subChild.material.needUpdate = true;
                subChild.material.skinning = true;
                subChild.material.morphTargets = true;
                subChild.geometry.uvsNeedUpdate = true;
            }
        }

        //this.collisionDetect = new CollisionDetector(this.mesh, this.collidables);

        this.addChilds();

        this.updateDatGui();
    }

    updateDatGui() {

        let guiConfig = {
            speed: this.speed,
            scale: this.mesh.scale.x
        };

        this.datGui.add(guiConfig, 'scale', 0.1, 2.0).onChange((v) => {
            this.mesh.scale.x = v;
            this.mesh.scale.y = v;
            this.mesh.scale.z = v;
        }).step(0.005).name('Scale');

        this.datGui.add(guiConfig, 'speed', 0, 0.5).onChange((val) => {
            this.speed = val;
        }).step(0.005).name('Speed');

    }

    addChilds(){
        for(let child of this.childs) {
            this.mesh.add(child.mesh);
        }
    }

    place(scene) {
        scene.add(this.mesh);
    }

    renderChilds(){
        //stuff to animate separately childs of the current mesh
        for(let child of this.childs) {
            child.render();
        }
    }

    render() {
        this.renderChilds();

        this.mesh.rotation.z += 0.001;
    }
}
export default Fruits;
