import * as THREE from 'three';

class Particle{
    
    
    /*
    * App particle to an object
    * Ex : 
    * let particle = new Particle({
            heartBeats: 10,
            datGui: this.datGui,
        });
    * */

    constructor(params){

        this.datGui = params.datGui ? params.datGui : null;
        this.color = params.color ?  params.color : 0xffffff;
        this.scale = params.scale ? params.scale : 1;

        this.heartBeats = params.heartBeats ? params.heartBeats : 0;

        // Load Textures
        this.uniforms = {
            tParticle: {
                type: 't',
                value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/particle.png')
            }
        };
        //  Config
        this.particles =  ( this.heartBeats && this.heartBeats > 0 ) ? this.heartBeats * 100 : 0;

        // GEOMETRY
        this.geometry = new THREE.BufferGeometry();

        // MATERIAL
        this.material = new THREE.ShaderMaterial( {
            uniforms:       this.uniforms,
            vertexShader:   window.SHADERS[0].particle.vertexShader,
            fragmentShader: window.SHADERS[0].particle.fragmentShader,
            blending:       THREE.AdditiveBlending,
            depthWrite:     false,
            transparent:    true,
            vertexColors:   true
        });

        this.createParticles();
        this.mesh = new THREE.Points( this.geometry , this.material );

        this.mesh.name = "Particles";

        this.datGui && this.updateDatGui();
    }

    createParticles(){

        let color = new THREE.Color();

        let particlePositions = [],
            particleColors = [],
            particlesSizes = [];

        for ( let i = 0; i < this.particles ; i ++ ) {

            // Particle position
            particlePositions.push( this.generateNewPosition() );
            particlePositions.push( this.generateNewPosition() );
            particlePositions.push( this.generateNewPosition() );

            // Particle color
            //color.setHSL( i / this.particles , 1.0, 0.5 );
            color = new THREE.Color( this.color );
            particleColors.push( color.r, color.g, color.b );
            //particleColors.push( 1.0, 1.0, 1.0);

            // Particle size
            let size =  Math.random() * (1 - 3) + 3;
            particlesSizes.push( size * this.scale * 0.2   );
            // size * 0.3 * this.scale
        }

        this.geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( particlePositions, 3 ).setDynamic( true ) );
        this.geometry.addAttribute( 'color', new THREE.Float32BufferAttribute( particleColors, 3 ) );
        this.geometry.addAttribute( 'size', new THREE.Float32BufferAttribute( particlesSizes, 1 ) );

    }

    setHeartBeats(heartBeats){
        this.particles = heartBeats * 50;
        this.createParticles();
    }

    generateNewPosition(){


        let random = Math.random();
        let  position = ((( Math.random() * 2 - 1 ) * (random * 100) ) / 60);
        position = position * 1.1 * this.scale;

        return position;
    }

    /**
     * updating datGui interface for quickly live editing params
     */
    updateDatGui() {

        if(this.datGui){
            let guiConfig = {
                hearthBeats: this.heartBeats
            };

            let particleOptions = this.datGui.__folders.Particle ? this.datGui.__folders.Particle : this.datGui.addFolder("Particle");

            particleOptions.add(guiConfig, 'hearthBeats', 0, 20).onChange((v) => {
                this.setHeartBeats(v);
            }).step(1).name('HearthBeats');
        }
    }

    getChild(){
        return this;
    }

    render(){
        // ANIMATION
        let distance = 0.001;
        let speed = 0.0005;
        let time = Date.now() * distance;
        let positions = this.geometry.attributes.position.array;
        // let sizes = this.geometry.attributes.size.array;
        //
        // for ( let i = 0; i < (this.particles) ; i++ ) {
        //     //sizes[ i ] +=  0.01 * Math.cos( 0.1 * i + time );
        // }

        for ( let i = 0; i < (this.particles * 3) ; i++ ) {
            i % 2 !== 0 ? positions[ i ] -= speed * Math.cos( time ) : positions[ i ] += speed * Math.cos( time );
        }
        this.geometry.attributes.position.needsUpdate = true;
        // this.geometry.attributes.size.needsUpdate = true;

    }

}
export default Particle;