class MovementController {

    constructor(object){
        this.moveObject = object;

        document.addEventListener("keydown", (e) => {
            if(this.moveObject !== undefined){
                this.checkKeyboardInputs(e);
            }else{
                console.warn("MovementController : no mesh object assigned" );
            }
        });

    }

    checkKeyboardInputs(event){
        event.preventDefault();
        let keyCode = event.keyCode;

        switch(keyCode){
            case 90:
                //Z
                this.moveObject.position.y += 0.1;

                break;

            case 83:
                //S
                this.moveObject.position.y -= 0.1;

                break;

            case 81:
                //Q
                this.moveObject.position.x -= 0.1;

                break;

            case 68:
                //D
                this.moveObject.position.x += 0.1;

                break;

            case 65:
                //A
                this.moveObject.position.z += 0.1;

                break;

            case 69:
                //E
                this.moveObject.position.z -= 0.1;

                break;

            default:

        }
    }    
}

export default MovementController;
