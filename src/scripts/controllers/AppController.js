import * as THREE from 'three';
import GalaxyManager from '../managers/GalaxyManager';
import Light from '../models/lights/Light';

import Camera from '../models/camera/Camera';
import OrbitControls from 'three-orbit-controls';
import DataManager from "../managers/DataManager";
import Loader from '../loaders/Loader';
import Particle from '../models/Particle';
import CONSTANTS from '../datas/Constants';

import ShadedPlanet from '../models/planet/ShadedPlanet';
import TexturedPlanet from '../models/planet/TexturedPlanet';
import GlowingPlanet from '../models/planet/GlowingPlanet';

import TWEEN from 'tween.js';

import Skybox from '../models/skybox/Skybox';

import AnimationsManager from '../managers/AnimationsManager';

import AudioManager from '../managers/SoundManager';

/* ====================== */
/* AppController
/* ====================== */

class AppController {

    /**
     * initialise ALL elements for the basic THREE scene
     * @param params
     */
    constructor(params) {

        //main canvas selector
        this.selector = params.selector;

        //elements
        this.elements = [];
        this.collidableMeshList = [];

        //scene
        this.scene = new THREE.Scene();
        window.scene = this.scene;
        this.clock = new THREE.Clock();
        this.clock.start();

        //lights
        this.globalLight = null;
        this.sceneHasLight = false;

        //set a raycasting system for mouse detection
        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();
        this.elementsPickables = [];

        //for vlues that needs a time update (uniforms)
        this.t0 = Date.now();
        this.passed = 0;

        //displaying custom values for elements
        /*this.datGui = new dat.GUI({
            height: 5 * 32 - 1
        });*/

        //main renderer settings
        this.renderer = new THREE.WebGLRenderer({
            antialias: true
        });

        //camera
        this.camera = new Camera({
            position: {
                x: 0,
                y: 0,
                z: 20
            },
            datGui: this.datGui
        });


        // Init orbit controller
        this.orbitController = OrbitControls(THREE);
        this.orbitController = new this.orbitController(this.camera.getCamera(), this.selector);
        this.orbitController.maxDistance = 1500;
        this.orbitController.enablePan = false;
        this.orbitController.autoRotate = false;
        this.orbitController.enableRotate = false;
        this.orbitController.enableZoom = false;
        this.orbitController.enableDamping = true;
        //this.orbitController.autoRotateSpeed = 0.5;

        this.camera.getCamera().orbitController = this.orbitController;


        //launch loading manager
        this.loader = new Loader({
            scope: this,
            elements: this.elements,
            elementsPickables: this.elementsPickables,
            // galaxyManager: this.galaxyManager,
            callback : this.onLoadingManagerReady.bind(this)
        });

        this.initDataManager();

        //preparate background music
        this.audioManager = new AudioManager({
            backgroundMusic: "background.mp3"
        });

        window.audioManager = this.audioManager;
    }

    /**
     * onLoadingManagerREady -> stuffs are loaded and ready to use
     */
    onLoadingManagerReady(){

        //initialising databse system
        this.dataManager.animationsManager.launchNextAnimation(0);

        this.loadModels();

        this.elements.forEach((element) => {

            element.place(this.scene);

            if(element.isPickable){
                this.elementsPickables.push(element.mesh);
            }
        });

        // if(this.galaxyManager.isShow){
        //     this.galaxyManager.onLoadingManagerReady();
        // }

        this.selector.appendChild(this.renderer.domElement);
        this.handleResize();
        window.addEventListener('resize', this.handleResize);
        window.addEventListener( 'mousedown', this.onMouseDown.bind(this), false );

        this.render();

        this.audioManager.playBackground();
    }

    /**
     * using data manager
     */
    initDataManager() {
        this.dataManager = new DataManager({
            mainController: this
        });
    }

    /**
     *  loading passed models and creaing new ones based on our custom classes
     */
    loadModels(el){

        // this.galaxyManager = new GalaxyManager({
        //     orbitController: this.orbitController,
        //     dataManager: this.dataManager,
        //     mainController: this,
        //     datGui: this.datGui,
        //     clock: this.clock,
        //     camera: this.camera.getCamera(),
        //     isShow: true,
        // });

        /************ AIR PLANETS ************/
        /*let airPlanet1 = new GlowingPlanet({
            name: 'air1',
            clock: this.clock,
            datGui: this.datGui,
            id: 1,
            planetType: 'air',
            doubleSided:true,
            texturedMesh: "clouds",
            textureMeshScale: 1.1,
            glow: 0.825,
            glowPower: 1.795,
            outGlow: 0.34,
            outPow: 2.23,
            glowingMesh: "noyau",
            glowColor: "#162ae3",
            outGlowColor: "#162ae3",
            hasShader: false,
            meshOpacity: 0.15,
            texture: "./assets/models/planet/textures/clouds.png"
        });

        let particle = new Particle({
            color: 0xe33b3b,
            heartBeats: 5,
            datGui: this.datGui,
            scale: 1
        });

        airPlanet1.childs.push(particle.getChild());

        this.addElement(airPlanet1);*/  // OK

        /*let airPlanet2 = new GlowingPlanet({
            name: 'air2',
            clock: this.clock,
            datGui: this.datGui,
            id: 2,
            planetType: 'air',
            doubleSided:true,
            texturedMesh: "clouds",
            textureMeshScale: 1.1,
            glow: 0.83,
            glowPower: 1.8,
            outGlow: 0.34,
            outPow: 3.74,
            glowingMesh: "noyau",
            glowColor: "#e4c1e8",
            outGlowColor: "#904de5",
            hasShader: false,
            meshOpacity: 0.15,
            texture: "./assets/models/planet/textures/clouds.png"
        });

        this.addElement(airPlanet2);*/ // OK

        /*let airPlanet3 = new GlowingPlanet({
            name: 'air3',
            clock: this.clock,
            datGui: this.datGui,
            id: 3,
            planetType: 'air',
            doubleSided:true,
            texturedMesh: "clouds",
            textureMeshScale: 1.1,
            glow: 0.83,
            glowPower: 1.8,
            outGlow: 0.34,
            outPow: 3.74,
            glowingMesh: "noyau",
            glowColor: "#dbcfea",
            outGlowColor: "#aa65b2",
            hasShader: false,
            meshOpacity: 0.15,
            texture: "./assets/models/planet/textures/clouds.png"
        });

        this.addElement(airPlanet3);*/ // OK

        /************************************************/

        /************ WATER PLANETS ************/

        /*let waterPlanet1 = new ShadedPlanet({
            name: 'water1',
            clock: this.clock,
            datGui: this.datGui,
            id:1,
            planetType: 'water',
            applyShaderToMesh: 'ocean',
            replaceMesh: true,
            doubleSided: true,
            shaderOpacity: 1,
            uniforms:{
                time:{
                    type: 'f',
                    value: 0.0
                },
                texture:{
                    type: 't',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/vague3.png')
                },
                amplifier:{
                    type: 'f',
                    value: 1.68451
                },
                maxOpacity:{
                    type: 'f',
                    value: 0.85
                }
            },
            renderUniforms:true
        });

        this.addElement(waterPlanet1);*/ //OK

        /*let waterPlanet2 = new ShadedPlanet({
            name: 'water2',
            clock: this.clock,
            datGui: this.datGui,
            id:2,
            planetType: 'water',
            applyShaderToMesh: 'ocean',
            replaceMesh: true,
            doubleSided: true,
            shaderOpacity: 1,
            uniforms:{
                time:{
                    type: 'f',
                    value: 0.0
                },
                texture:{
                    type: 't',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/vague2.png')
                },
                amplifier:{
                    type: 'f',
                    value: 1.986453
                },
                maxOpacity:{
                    type: 'f',
                    value: 0.97
                }
            },
            renderUniforms:true
        });

        this.addElement(waterPlanet2);*/  //OK

        /*let waterPlanet3 = new ShadedPlanet({
            name: 'water3',
            clock: this.clock,
            datGui: this.datGui,
            id:3,
            planetType: 'water',
            applyShaderToMesh: 'ocean',
            replaceMesh: true,
            doubleSided: true,
            shaderOpacity: 1,
            uniforms:{
                time:{
                    type: 'f',
                    value: 0.0
                },
                texture:{
                    type: 't',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/vague3.png')
                },
                amplifier:{
                    type: 'f',
                    value: 1.986453
                },
                maxOpacity:{
                    type: 'f',
                    value: 0.85
                }
            },
            renderUniforms:true
        });

        this.addElement(waterPlanet3);*/ //OK

        /************************************************/

        /************ FIRE PLANETS ************/

        /*let firePlanet1 = new ShadedPlanet({
            name: 'fire1',
            clock: this.clock,
            datGui: this.datGui,
            id:1,
            planetType:'fire',
            applyShaderToMesh:'lava',
            replaceMesh: true,
            shaderOpacity: 1,
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                texture:{
                    type:'t',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/lava.png')
                }
            },
            renderUniforms: true
        });

        this.addElement(firePlanet1);*/  //shaded

        /*let firePlanet2 = new ShadedPlanet({
            name: 'fire2',
            clock: this.clock,
            datGui: this.datGui,
            id:2,
            planetType:'fire',
            applyShaderToMesh:'lava',
            replaceMesh: false,
            shaderOpacity: 1,
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                texture:{
                    type:'t',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/explosion.png')
                }
            },
            renderUniforms: true
        });

        this.addElement(firePlanet2);*/ // OK

        /*let firePlanet3 = new ShadedPlanet({
            name: 'fire3',
            clock: this.clock,
            datGui: this.datGui,
            id:3,
            planetType:'fire',
            replaceMesh:true,
            doubleSided:false,
            applyShaderToMesh:'lava',
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                texture:{
                    type:'t',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/explosion.png')
                }
            },
            renderUniforms:true
        });

        this.addElement(firePlanet3);*/ // OK

        /************************************************/

        /************ DIRT PLANETS ************/
        /*let dirtPlanet1 = new TexturedPlanet({
            name: 'dirt1',
            clock: this.clock,
            datGui: this.datGui,
            id: 1,
            planetType: 'dirt',
            texturedMesh: "ground",
            bumpScaleFactor: 0.25,
            hasShader: false,
            texture: "./assets/models/planet/textures/dirt_1_texture.png",
            bumpMap: "./assets/models/planet/textures/dirt_1_bump.png"
        });

        this.addElement(dirtPlanet1);*/ // OK

        /*let dirtPlanet2 = new TexturedPlanet({
            name: 'dirt2',
            clock: this.clock,
            datGui: this.datGui,
            id: 2,
            planetType: 'dirt',
            doubleSided:true,
            texturedMesh: "ground",
            bumpScaleFactor: 0.35,
            hasShader: false,
            texture: "./assets/models/planet/textures/dirt_2_texture.png",
            bumpMap: "./assets/models/planet/textures/dirt_2_bump.png"
        });

        this.addElement(dirtPlanet2);*/ // OK

        /*let dirtPlanet3 = new TexturedPlanet({
           name: 'dirt3',
            clock: this.clock,
            datGui: this.datGui,
            id: 3,
            planetType: 'dirt',
            doubleSided:true,
            texturedMesh: "ground",
            bumpScaleFactor: 0.1,
            hasShader: false,
            texture: "./assets/models/planet/textures/dirt_3_texture.png",
            bumpMap: "./assets/models/planet/textures/dirt_3_bump.png"
        });

        this.addElement(dirtPlanet3);*/  // OK

        /************************************************/

        /************ OTHERS PLANETS ************/
        /*let fractalPlanet = new Planet({
            name: 'air2',
            clock: this.clock,
            datGui: this.datGui,
            id:2,
            planetType:'air',
            applyShaderToMesh:'clouds',
            specificShader: 'fractalPlanet',
            uniforms:{
                iTime:{
                    type:'f',
                    value:0.0
                },
                iResolution:{
                    type:"v2",
                    value: new THREE.Vector2(window.innerWidth, window.innerHeight)
                }
            },
            renderUniforms:function(scope){
                scope.uniforms.iResolution.value.x = window.innerWidth;
                scope.uniforms.iResolution.value.y = window.innerHeight;
            }
        });

        this.addElement(fractalPlanet)*/

        /*let noisePlanet = new Planet({
            name: 'air2',
            clock: this.clock,
            datGui: this.datGui,
            id:2,
            planetType:'air',
            applyShaderToMesh:'clouds',
            specificShader: 'noisePlanet',
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                texture:{
                    type:'t',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/clouds.png')
                },
                amplifier:{
                    type:'f',
                    value: 1.65413
                },
                opacity:{
                    type:'f',
                    value:0.0
                }
            },
            renderUniforms: true
        });

        this.addElement(noisePlanet);*/

        /************************************************/

        //SKYBOX
        /*const skysphere = new Skybox({
            type: "sphere",
            shader: true,
            datGui: this.datGui,
            shaderScaleFactor: 10,
            uniforms: {
                time: {
                    type: 'f',
                    value: 0.0
                },
                brightness: {
                    type: 'f',
                    value: 1.05
                },
                baseColor: {
                    type: 'v3',
                    value: new THREE.Vector3(0.3686274509803922, 0.21176470588235294, 0.9882352941176471)
                },
                scale: {
                    type: 'f',
                    value: 1.0
                }
            },
            textured: false,
            texture: "./assets/models/skybox/galaxy/galaxy_wall.jpg"
        });

        this.addElement(skysphere);*/

        /*const skysphere = new Skybox({
            type: "sphere",
            shader: true,
            datGui: this.datGui,
            shaderScaleFactor: 10,
            uniforms: {
                time: {
                    type: 'f',
                    value: 0.0
                },
                color3: {
                    type: "c",
                    value: {
                        r: 0.00784313725490196,
                        g: 0.796078431372549,
                        b: 0.8901960784313725
                    },
                },
                permutations: {
                    type: "f",
                    value: 1
                },
                iterations: {
                    value: 2,
                    type: "f"
                },
                Molten_Noise1462568137864_213_color1: {
                    value: {
                        r: 0.7098039215686275,
                        g: 0.3137254901960784,
                        b: 0.8117647058823529
                    },
                    type: "c",
                },
                Molten_Noise1462568137864_213_color2: {
                    value: {
                        r: 0.9333333333333333,
                        g: 0.23921568627450981,
                        b: 0.9764705882352941
                    },
                    type: "c",
                },
                uvScale: {
                    value: {
                        x: 2,
                        y: 1
                    },
                    type: "v2"
                },
                brightness: {
                    value: 0.5,
                    type: "f"
                },
                Molten_Noise1462568137864_213_speed: {
                    value: 0.8,
                    type: "f"
                },
                scale: {
                    value: 1.5,
                    type: "f"
                },
                Circular_Gradient1462568150439_226_color1: {
                    value: {
                        r: 1,
                        g: 1,
                        b: 1
                    },
                    type: "c"
                },
                Circular_Gradient1462568150439_226_color2: {
                    value: {
                        r: 0,
                        g: 0,
                        b: 0
                    },
                    type: "c"
                },
                arms: {
                    value: 1,
                    type: "f"
                },
                color: {
                    value: {
                        r: 0.996078431372549,
                        g: 0.14901960784313725,
                        b: 0
                    },
                    type: "c",
                },
                blur: {
                    value: 0.2,
                    type: "f"
                },
                Follow_the_Spiral_Rabbit1462568364455_332_speed: {
                    value: 0.4,
                    type: "f"
                },
                Follow_the_Spiral_Rabbit1462568364455_332_resolution: {
                    value: {
                        x: 10,
                        y: 10
                    },
                    type: "v2"
                },
                starRadius: {
                    value: 0.75,
                    type: "f"
                },
                starDensity: {
                    value: 5,
                    type: "f"
                },
                starColor: {
                    value: {
                        r: 0.796078431372549,
                        g: 0.9254901960784314,
                        b: 0.9411764705882353
                    },
                    type: "c"
                },
                Parallax_Starfield1462568538439_87_speed: {
                    value: 0.05,
                    type: "f"
                },
                Parallax_Starfield1462568538439_87_resolution: {
                    value: {
                        x: 10,
                        y: 10
                    },
                    type: "v2"
                },
                Vertical_2_Color_Graident1462568589945_110_color1: {
                    value: {
                        r: 0.7568627450980392,
                        g: 0.22745098039215686,
                        b: 1
                    },
                    type: "c"
                },
                Vertical_2_Color_Graident1462568589945_110_color2: {
                    value: {
                        r: 0.03137254901960784,
                        g: 0.2627450980392157,
                        b: 0.6980392156862745
                    },
                    type: "c"
                }
            },
            textured: false,
            texture: "./assets/models/skybox/galaxy/galaxy_wall.jpg"
        });

        this.addElement(skysphere);*/


        const skysphere = new Skybox({
            path:'./assets/models/skybox/galaxy/',
            type: "cube",
            shader: false,
            datGui: this.datGui,
            shaderScaleFactor: 30,
            textured: true,
            skyboxTexture:{
                right: "galaxy_right.png",
                left: "galaxy_left.png",
                top: "galaxy_top.png",
                bottom: "galaxy_bottom.png",
                back: "galaxy_back.png",
                front: "galaxy_front.png"
            }
        });

        this.addElement(skysphere);



        //adding element to te rendering system
        if (el !== undefined) {
            let elementDragged = true;
            this.addElement(el, null, elementDragged);
        }

        //using global scene lights
        if (this.sceneHasLight === false) {
            this.sceneHasLight = true;
            this.globalLight = new Light({
                camera: this.camera.getCamera(),
                datGui:this.datGui
            });

            this.addElement(this.globalLight);
        }
    }

    /**
     *
     * @param element
     * @param collidable
     * @param elementDragged
     */
    addElement(element, collidable, elementDragged){
        this.elements.push(element);

        if(collidable){
            this.collidableMeshList.push(element);
            element.collidables = this.collidableMeshList;
        }


        this.elements.forEach((element) => {
            element.place(this.scene);
        });

    }

    /**
     * resize system
     */
    handleResize(){
        this.renderer && this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.camera && this.camera.updateAspectRatio();

        window.mainController &&  window.mainController.renderer.setSize(window.innerWidth, window.innerHeight);
        window.mainController &&  window.mainController.camera.updateAspectRatio();
    }

    /**
     * for mouse event
     * @param event
     */
    onMouseDown( event ) {

        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components

        this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

        this.renderMouseDown();
    }

    /**
     * using raycaster for determining the vector of the mouse in the 3D space
     */
    renderMouseDown() {

        // update the picking ray with the camera and mouse position
        this.raycaster.setFromCamera( this.mouse, this.camera.getCamera() );

        // calculate objects intersecting the picking ray
        let intersects = this.raycaster.intersectObjects( this.elementsPickables, true );

        if(intersects.length > 0){

            for(let intersect of intersects){

                if( intersect.object.name === 'line1' ||
                    intersect.object.name === 'line2' ||
                    intersect.object.name === 'line3' ){

                    switch (intersect.object.name){
                        case 'line1':
                            this.dataManager.user.light = "0";
                            break;
                        case 'line2':
                            this.dataManager.user.light = "1";
                            break;
                        case 'line3':
                            this.dataManager.user.light = "2";
                            break;
                    }

                    this.dataManager.user.lightColor = CONSTANTS.COLORS[this.dataManager.user.family][this.dataManager.user.light];
                    this.dataManager.firebase.updateUser(this.dataManager.user);

                    if(this.passed > 0){
                        return;
                    }else{
                        this.passed = 1;
                    }
                    this.dataManager.animationsManager.launchNextAnimation(7);
                    this.passed ++;


                } else if(intersect.object.name !== ''){
                     intersect.object.onPick && intersect.object.onPick();
                     intersect.object.parent.onPick && intersect.object.parent.onPick();
                }


            }

            // console.log(intersects);
            // let intersect = intersects[ 0 ].object;
            // while(intersect.type !== "Group"){
            //     intersect = intersect.parent;
            // }
            // console.log(intersect);
            //
            // intersect.galaxyOnPick && intersect.galaxyOnPick();
            // intersect.parent.galaxyOnPick && intersect.parent.galaxyOnPick();
        }
    }

    /**
     * MAIN RENDERING PROCESS
     */
    render(){
        this.elements.forEach((element) => {
            element.render();
        });
        window.mainController &&
        window.mainController.galaxyManager &&
        window.mainController.galaxyManager.isShow &&
        window.mainController.galaxyManager.update();
        this.orbitController.update();
        TWEEN.update();
        this.renderer.render(this.scene, this.camera.getCamera());

        window.mainController = this;

        requestAnimationFrame(this.render.bind(this));
    }

}

export default AppController;