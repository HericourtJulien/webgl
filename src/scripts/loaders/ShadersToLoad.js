import * as THREE from 'three';

/** AIR SHADERS **/
import airPlanet1Vert from '../../shaders/compiled/air/shader.airPlanet1.vert.js';
import airPlanet1Frag from '../../shaders/compiled/air/shader.airPlanet1.frag.js';
import airPlanet2Vert from '../../shaders/compiled/air/shader.airPlanet2.vert.js';
import airPlanet2Frag from '../../shaders/compiled/air/shader.airPlanet2.frag.js';
import airPlanet3Vert from '../../shaders/compiled/air/shader.airPlanet3.vert.js';
import airPlanet3Frag from '../../shaders/compiled/air/shader.airPlanet3.frag.js';

/** FIRE SHADERS **/
import firePlanet1Vert from '../../shaders/compiled/fire/shader.firePlanet1.vert.js';
import firePlanet1Frag from '../../shaders/compiled/fire/shader.firePlanet1.frag.js';
import firePlanet2Vert from '../../shaders/compiled/fire/shader.firePlanet2.vert.js';
import firePlanet2Frag from '../../shaders/compiled/fire/shader.firePlanet2.frag.js';
import firePlanet3Vert from '../../shaders/compiled/fire/shader.firePlanet3.vert.js';
import firePlanet3Frag from '../../shaders/compiled/fire/shader.firePlanet3.frag.js';

/** WATER SHADERS */
import waterPlanet1Vert from '../../shaders/compiled/water/shader.waterPlanet1.vert.js';
import waterPlanet1Frag from '../../shaders/compiled/water/shader.waterPlanet1.frag.js';
import waterPlanet2Vert from '../../shaders/compiled/water/shader.waterPlanet2.vert.js';
import waterPlanet2Frag from '../../shaders/compiled/water/shader.waterPlanet2.frag.js';
import waterPlanet3Vert from '../../shaders/compiled/water/shader.waterPlanet3.vert.js';
import waterPlanet3Frag from '../../shaders/compiled/water/shader.waterPlanet3.frag.js';

/** DIRT SHADERS */
import dirtPlanet1Vert from '../../shaders/compiled/dirt/shader.dirtPlanet1.vert.js';
import dirtPlanet1Frag from '../../shaders/compiled/dirt/shader.dirtPlanet1.frag.js';
import dirtPlanet2Vert from '../../shaders/compiled/dirt/shader.dirtPlanet2.vert.js';
import dirtPlanet2Frag from '../../shaders/compiled/dirt/shader.dirtPlanet2.frag.js';
import dirtPlanet3Vert from '../../shaders/compiled/dirt/shader.dirtPlanet3.vert.js';
import dirtPlanet3Frag from '../../shaders/compiled/dirt/shader.dirtPlanet3.frag.js';

/** PARTICLE SHADERS */
import particleShaderVert from '../../shaders/compiled/particle/particle.vert';
import particleShaderFrag from '../../shaders/compiled/particle/particle.frag';

/** OTHERS **/
import fractalPlanetVert from '../../shaders/compiled/others/fractalPlanet.vert.js';
import fractalPlanetFrag from '../../shaders/compiled/others/fractalPlanet.frag.js';
import noiseTextureVert from '../../shaders/compiled/others/noiseTexture.vert.js';
import noiseTextureFrag from '../../shaders/compiled/others/noiseTexture.frag.js';
/*import galaxyVert from '../../shaders/compiled/others/galaxy/shader.galaxyFog.vert.js';
import galaxyFrag from '../../shaders/compiled/others/galaxy/shader.galaxyFog.frag.js';*/

class Shaders{
    constructor(){

        window.SHADERS.push(
            {
                air: {
                    planet1:{
                        vertexShader: airPlanet1Vert,
                        fragmentShader : airPlanet1Frag,
                        shaderScaleFactor: 8.1
                    },
                    planet2:{
                        vertexShader: airPlanet2Vert,
                        fragmentShader : airPlanet2Frag,
                        shaderScaleFactor: 7.8
                    },
                    planet3:{
                        vertexShader: airPlanet3Vert,
                        fragmentShader : airPlanet3Frag,
                        shaderScaleFactor: 2
                    }
                },
                fire: {
                    planet1:{
                        vertexShader: firePlanet1Vert,
                        fragmentShader : firePlanet1Frag,
                        shaderScaleFactor: 7.3
                    },
                    planet2:{
                        vertexShader: firePlanet2Vert,
                        fragmentShader : firePlanet2Frag,
                        shaderScaleFactor: 8.7
                    },
                    planet3:{
                        vertexShader: firePlanet3Vert,
                        fragmentShader : firePlanet3Frag,
                        shaderScaleFactor: 7.25
                    }
                },
                water: {
                    planet1:{
                        vertexShader: waterPlanet1Vert,
                        fragmentShader : waterPlanet1Frag,
                        shaderScaleFactor: 7.3
                    },
                    planet2:{
                        vertexShader: waterPlanet2Vert,
                        fragmentShader : waterPlanet2Frag,
                        shaderScaleFactor: 7
                    },
                    planet3:{
                        vertexShader: waterPlanet3Vert,
                        fragmentShader : waterPlanet3Frag,
                        shaderScaleFactor: 7.3
                    }
                },
                dirt:{
                    planet1:{
                        vertexShader: dirtPlanet1Vert,
                        fragmentShader : dirtPlanet1Frag,
                        shaderScaleFactor: 2
                    },
                    planet2:{
                        vertexShader: dirtPlanet2Vert,
                        fragmentShader : dirtPlanet2Frag,
                        shaderScaleFactor: 2
                    },
                    planet3:{
                        vertexShader: dirtPlanet3Vert,
                        fragmentShader : dirtPlanet3Frag,
                        shaderScaleFactor: 2
                    }
                },
                others:{
                    fractalPlanet:{
                        vertexShader: fractalPlanetVert,
                        fragmentShader: fractalPlanetFrag,
                        shaderScaleFactor: 7.8,
                        needsSceneLight:true,
                        baseUniforms:{
                            time:{
                                type:'f',
                                value:0.0
                            },
                            iResolution:{
                                type:"v2",
                                value: new THREE.Vector2(window.innerWidth, window.innerHeight)
                            }
                        }
                    },
                    noisePlanet:{
                        vertexShader: noiseTextureVert,
                        fragmentShader: noiseTextureFrag,
                        shaderScaleFactor: 9.5,
                        needsSceneLight:false,
                        baseUniforms:{
                            time:{
                                type:'f',
                                value:0.0
                            },
                            texture:{
                                type:'t',
                                value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/clouds.png')
                            },
                            amplifier:{
                                type:'f',
                                value: 1.687984651
                            },
                            opacity:{
                                type:'f',
                                value:0.2
                            }
                        }
                    }/*,
                    galaxy:{
                        vertexShader: galaxyVert,
                        fragmentShader: galaxyFrag,
                        shaderScaleFactor: 100,
                        needsSceneLight: false
                    }*/
                },

                particle:{
                        vertexShader: particleShaderVert,
                        fragmentShader : particleShaderFrag
                }
            }
        )
    }
}

export default Shaders;