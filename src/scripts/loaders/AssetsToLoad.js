export default [
    {
        name: 't50',
        path: 'assets/models/planet/t502.dae'
    },
    {
        name: 'banane',
        path: 'assets/models/fruits/banane.dae'
    },
    {
        name: 'air1',
        path: 'assets/models/planet/air/planet_air_1.dae'
    },
    {
        name: 'air2',
        path: 'assets/models/planet/air/planet_air_2.dae'
    },
    {
        name: 'air3',
        path: 'assets/models/planet/air/planet_air_3.dae'
    },
    {
        name: 'fire1',
        path: 'assets/models/planet/fire/planet_fire_1.dae'
    },
    {
        name: 'fire2',
        path: 'assets/models/planet/fire/planet_fire_2.dae'
    },
    {
        name: 'fire3',
        path: 'assets/models/planet/fire/planet_fire_3.dae'
    },
    {
        name: 'water1',
        path: 'assets/models/planet/water/planet_water_1.dae'
    },
    {
        name: 'water2',
        path: 'assets/models/planet/water/planet_water_2.dae'
    },
    {
        name: 'water3',
        path: 'assets/models/planet/water/planet_water_3.dae'
    },
    {
        name: 'dirt1',
        path: 'assets/models/planet/dirt/planet_dirt_1.dae'
    },
    {
        name: 'dirt2',
        path: 'assets/models/planet/dirt/planet_dirt_2.dae'
    },
    {
        name: 'dirt3',
        path: 'assets/models/planet/dirt/planet_dirt_3.dae'
    }
];