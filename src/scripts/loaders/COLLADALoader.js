import * as THREE from 'three';
import ColladaLoader from '../libs/ColladaLoader';

class COLLADALoader {

    constructor(params) {
        colladaLoader.load(this.pathToModel, (object) => {

            if (params.isDragged) {
                //need to reassign path of material & textures
                //find if object has images / materials / texture property to
                //reset path of image texture according to current path of file dragged
                this.checkForTexture(object.scene);
            }

            params.callback(object);
        }, () => {
            //onProgress
        }, (e) => {
            //onError
            console.error(e);
        });
    }

    checkForTexture(element) {
        if (element.hasOwnProperty("children")) {
            for (let child of element.children) {
                if (child.hasOwnProperty("material")) {
                    if (child.material.length > 0) {
                        for (let material of child.material) {
                            if (material.hasOwnProperty("map")) {
                                if (material.map !== undefined && material.map !== null) {
                                    if (material.map.hasOwnProperty("image")) {
                                        this.modifyTexturePath(material.map.image);
                                    }
                                }
                            }
                        }
                    } else {
                        if (child.material.hasOwnProperty("map")) {
                            if (child.material.map !== null && child.material.map !== undefined) {
                                if (child.material.map.hasOwnProperty("image")) {
                                    this.modifyTexturePath(child.material.map.image);
                                }
                            }
                        }
                    }
                }

                if (child.hasOwnProperty("texture")) {
                    console.log(child, "has texture");
                }

                this.checkForTexture(child);
            }
        } else {
            console.log("no children in passed element");
        }
    }

    modifyTexturePath(element) {


        //let isMac = /^darwin/.test(process.platform);
        if (process.platform === "darwin") {
            return;
        }

        let currentPath = this.pathToModel.substr(0, this.pathToModel.lastIndexOf("\\"));
        let currentSrc = element.src.substr(element.src.lastIndexOf("/") + 1, element.src.length);
        let completeUrl = currentPath.replace(/\\/g, "/") + '/' + currentSrc;

        element.src = completeUrl;
    }
}

export default COLLADALoader;