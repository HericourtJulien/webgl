import *  as THREE from 'three';
import * as OBJLoader from '../libs/OBJLoader';
import * as MTLLoader from '../libs/MtlLoader';

class OBJMTLLoader{

    constructor(params){

        this.pathModels = '../../assets/models/';

        let loaderMTL = new THREE.MTLLoader(params.loadingManager);
        loaderMTL.setPath(this.pathModels);

        // Load Material(s)
        loaderMTL.load(params.mtl, (mtl) => {

            mtl.preload();

            let loaderOBJ = new THREE.OBJLoader(params.loadingManager);
            loaderOBJ.setPath(this.pathModels);

            // Set default Material mtl to futur object
            loaderOBJ.setMaterials(mtl);

            // Load object
            loaderOBJ.load(params.model, (object) => {
                params.callback(object);
            });

        }, () => {
            //onProgress
        }, (e) => {
            //onError
            console.error(e);
        });
    }
}

export default OBJMTLLoader;