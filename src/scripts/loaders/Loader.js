import * as THREE from 'three';
import * as AssetsToLoad from '../loaders/AssetsToLoad';
import Shaders from '../loaders/ShadersToLoad';
import * as JsonAssetsToLoad from '../loaders/JsonModelsToLoad';
import Planet from '../models/planet/Planet';
import AnimationsManager from '../managers/AnimationsManager';

class Loader {

    constructor(params) {

        this.elements = params.elements;
        this.galaxyManager = params.galaxyManager;
        this.elementsPickables = params.elementsPickables;
        this.callback = params.callback;
        this.modelsToLoad = AssetsToLoad.default;
        this.jsonModelsToLoad = JsonAssetsToLoad.default;

        this.loadingManager = new THREE.LoadingManager();

        this.shaders = new Shaders(); //add shaders to a global variable window.SHADERS

        //CREATE COLLADA MODELS
        /*for( let model of this.modelsToLoad){

            let colladaLoader = new THREE.ColladaLoader(this.loadingManager);

            colladaLoader.load(model.path, ( object ) => {

                window.MODELS[model.name] = object;

            }, () => {
                //onProgress
            }, (e) => {
                //onError
                console.warn("error while loading .dae models");
                console.error(e);
            });
        }*/

        this.loadingManager.onStart = (url, itemsLoaded, itemsTotal) => {
            // console.log('INFO in Main.main() : Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');
        };

        this.loadingManager.onLoad = () => {
            console.warn('All stuffs are loaded.');

            this.callback && this.callback();

        };

        this.loadingManager.onError = (url) => {
            console.error('ERROR in Main : _ThreeLoadingManager - url =', url);
        };


        /** ! ---------------- JSONS -------------------- ! **/
        for(let jsonModel of this.jsonModelsToLoad){
            console.info("loading json models");
            let jsonLoader = new THREE.ObjectLoader(this.loadingManager);

            jsonLoader.load(jsonModel.path, (object) => {
                window.JSONMODELS[jsonModel.name] = object;
            }, () => {
                //progress
            }, (e) => {
                //error
                console.warn("error parsing json while loading models");
                console.error(e);
            });
        }


        //setting planets to be a global accesible variable



        /************** EAU *****************/

        /*let waterPlanet1 = new ShadedPlanet({
            name: 'water1',
            clock: this.clock,
            datGui: this.datGui,
            id:1,
            planetType: 'water',
            applyShaderToMesh: 'ocean',
            replaceMesh: true,
            doubleSided: true,
            shaderOpacity: 0.9,
            uniforms:{
                time:{
                    type: 'f',
                    value: 0.0
                },
                texture:{
                    type: 't',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/vague3.png')
                },
                amplifier:{
                    type: 'f',
                    value: 1.68451
                },
                maxOpacity:{
                    type: 'f',
                    value: 0.85
                }
            },
            renderUniforms:true
        });*/


        /************** FEU *****************/

        /*let firePlanet2 = new ShadedPlanet({
            name: 'fire2',
            clock: this.clock,
            datGui: this.datGui,
            id:2,
            planetType:'fire',
            applyShaderToMesh:'lava',
            replaceMesh: false,
            shaderOpacity: 1,
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                texture:{
                    type:'t',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/explosion.png')
                }
            },
            renderUniforms: true
        });*/

        /*let firePlanet3 = new ShadedPlanet({
            name: 'fire3',
            clock: this.clock,
            datGui: this.datGui,
            id:3,
            planetType:'fire',
            applyShaderToMesh:'lava',
            uniforms:{
                time:{
                    type:'f',
                    value:0.0
                },
                tExplosion:{
                    type:'t',
                    value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/explosion.png')
                }
            },
            renderUniforms:true
        });*/

        /************** TERRE *****************/

        /*let dirtPlanet1 = new TexturedPlanet({
            name: 'dirt1',
            clock: this.clock,
            datGui: this.datGui,
            id: 1,
            planetType: 'dirt',
            texturedMesh: "ground",
            hasShader: false,
            texture: "./assets/models/planet/textures/dirt_1_texture.png",
            bumpMap: "./assets/models/planet/textures/dirt_1_bump.png"
        });

        this.addElement(dirtPlanet1);*/ // OK
    }

}


export default Loader;