export default [
    {
        name: 'air1',
        path: 'assets/models/planet/air/planet_air_1.json'
    },
    {
        name: 'air2',
        path: 'assets/models/planet/air/planet_air_2.json'
    },
    {
        name: 'air3',
        path: 'assets/models/planet/air/planet_air_3.json'
    },
    {
        name: 'fire1',
        path: 'assets/models/planet/fire/planet_fire_1.json'
    },
    {
        name: 'fire2',
        path: 'assets/models/planet/fire/planet_fire_2.json'
    },
    {
        name: 'fire3',
        path: 'assets/models/planet/fire/planet_fire_3.json'
    },
    {
        name: 'water1',
        path: 'assets/models/planet/water/planet_water_1.json'
    },
    {
        name: 'water2',
        path: 'assets/models/planet/water/planet_water_2.json'
    },
    {
        name: 'water3',
        path: 'assets/models/planet/water/planet_water_3.json'
    },
    {
        name: 'dirt1',
        path: 'assets/models/planet/dirt/planet_dirt_1.json'
    },
    {
        name: 'dirt2',
        path: 'assets/models/planet/dirt/planet_dirt_2.json'
    },
    {
        name: 'dirt3',
        path: 'assets/models/planet/dirt/planet_dirt_3.json'
    },
    {
        name: 'cratere',
        path: 'assets/models/elements/cratere.json'
    },
    {
        name: 'flaque',
        path: 'assets/models/elements/flaque.json'
    },
    {
        name: 'lac',
        path: 'assets/models/elements/lac.json'
    },
    {
        name: 'montagne',
        path: 'assets/models/elements/montagne.json'
    },
    {
        name: 'nuages',
        path: 'assets/models/elements/nuages.json'
    },
    {
        name: 'rocher',
        path: 'assets/models/elements/rocher.json'
    },
    {
        name: 'tornade',
        path: 'assets/models/elements/tornade.json'
    },
    {
        name: 'volcan',
        path: 'assets/models/elements/volcan.json'
    }
];