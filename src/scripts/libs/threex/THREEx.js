// @Remastered by Benjamin qui en avait BIEN MARRE

import * as THREE from 'three';

const THREEx  = THREEx || {};

THREEx.geometricGlowMesh = function(mesh, color, outColor, coef, power, outCoef, outPow) {
    let object3d = new THREE.Object3D;

    let geometry = mesh.geometry.clone();
    this.dilateGeometry(geometry, 0.01);
    let material = this.createAtmosphereMaterial();
    material.uniforms.glowColor.value = new THREE.Color(color);
    material.uniforms.coeficient.value = coef;
    material.uniforms.power.value = power;
    let insideMesh = new THREE.Mesh(geometry, material);
    object3d.add(insideMesh);


    let geometry2 = mesh.geometry.clone();
    this.dilateGeometry(geometry, 0.1);
    let material2 = this.createAtmosphereMaterial();
    material2.uniforms.glowColor.value = new THREE.Color(outColor);
    material2.uniforms.coeficient.value = outCoef;
    material2.uniforms.power.value = outPow;
    material2.side = THREE.BackSide;
    let outsideMesh = new THREE.Mesh(geometry2, material2);
    object3d.add(outsideMesh);

    // expose a few variable
    this.object3d = object3d;
    this.insideMesh = insideMesh;
    this.outsideMesh = outsideMesh;

    return this.object3d;
};

THREEx.dilateGeometry = function(geometry, length) {
    // gather vertexNormals from geometry.faces
    let vertexNormals = new Array(geometry.vertices.length);

    geometry.faces.forEach(function (face) {
        if (face instanceof THREE.Face4) {
            vertexNormals[face.a] = face.vertexNormals[0];
            vertexNormals[face.b] = face.vertexNormals[1];
            vertexNormals[face.c] = face.vertexNormals[2];
            vertexNormals[face.d] = face.vertexNormals[3];
        } else if (face instanceof THREE.Face3) {
            vertexNormals[face.a] = face.vertexNormals[0];
            vertexNormals[face.b] = face.vertexNormals[1];
            vertexNormals[face.c] = face.vertexNormals[2];
        } else console.assert(false);
    });
    // modify the vertices according to vertextNormal
    geometry.vertices.forEach(function (vertex, idx) {
        let vertexNormal = vertexNormals[idx];
        vertex.x += vertexNormal.x * length;
        vertex.y += vertexNormal.y * length;
        vertex.z += vertexNormal.z * length;
    });
};

THREEx.createAtmosphereMaterial = function(){
    let vertexShader	= [
        'varying vec3	vVertexWorldPosition;',
        'varying vec3	vVertexNormal;',

        'varying vec4	vFragColor;',

        'void main(){',
        '	vVertexNormal	= normalize(normalMatrix * normal);',

        '	vVertexWorldPosition	= (modelMatrix * vec4(position, 1.0)).xyz;',

        '	// set gl_Position',
        '	gl_Position	= projectionMatrix * modelViewMatrix * vec4(position, 1.0);',
        '}',

    ].join('\n');
    let fragmentShader	= [
        'uniform vec3	glowColor;',
        'uniform float	coeficient;',
        'uniform float	power;',

        'varying vec3	vVertexNormal;',
        'varying vec3	vVertexWorldPosition;',

        'varying vec4	vFragColor;',

        'void main(){',
        '	vec3 worldCameraToVertex= vVertexWorldPosition - cameraPosition;',
        '	vec3 viewCameraToVertex	= (viewMatrix * vec4(worldCameraToVertex, 0.0)).xyz;',
        '	viewCameraToVertex	= normalize(viewCameraToVertex);',
        '	float intensity		= pow(coeficient + dot(vVertexNormal, viewCameraToVertex), power);',
        '	gl_FragColor		= vec4(glowColor, intensity);',
        '}',
    ].join('\n');

    // create custom material from the shader code above
    //   that is within specially labeled script tags
    let material	= new THREE.ShaderMaterial({
        uniforms: {
            coeficient	: {
                type	: "f",
                value	: 1.0
            },
            power		: {
                type	: "f",
                value	: 1.0
            },
            glowColor	: {
                type	: "c",
                value	: new THREE.Color('pink')
            },
        },
        vertexShader	: vertexShader,
        fragmentShader	: fragmentShader,
        blending	: THREE.AdditiveBlending,
        transparent	: true,
        depthWrite	: false,
    });
    return material
};

export default THREEx;