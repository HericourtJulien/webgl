
class SoundManager{
    constructor(params){
        this.path = "./assets/sounds/";
        this.backgroundAudio = null;
        this.backgroundTrack = params.backgroundMusic ? params.backgroundMusic : "";
        this.backgroundMusicEnded = false;
        this.duration = 0;

        this.canBeat = false;

        this.clickAudio = null;
        this.heartAudio = null;

        this.initBackgroundAudio();
        this.initClickAudio();
    }

    initBackgroundAudio(){
        if(this.backgroundTrack === null){
            console.error("no background music");
            return
        }

        this.backgroundAudio = new Audio(this.path + this.backgroundTrack);
    }

    playBackground(){
        this.backgroundAudio.play();
        this.backgroundAudio.volume = 0.2;

        let duration = 30;

        this.duration = this.backgroundAudio.duration;

        this.backgroundAudio.addEventListener('ended', () => {
            setTimeout(this.replayBackground.bind(this), duration * 1000);
        });


    }

    pauseBackground(){
        if(this.backgroundAudio){
            this.backgroundAudio.pause();
        }
    }

    replayBackground(){
        console.log("replay");
        this.pauseBackground();
        this.playBackground();
    }

    initClickAudio(){
        this.clickAudio = new Audio(this.path + "clic.mp3");
        this.clickAudio.volume = 0.5;
    }

    playClick(){
        this.clickAudio.play();
    }


    playHeartBeat(){
        if(!this.canBeat){
            return;
        }
        let heartBeat = new Audio(this.path + "beat.mp3");
        heartBeat.play();
    }
}

export default SoundManager;