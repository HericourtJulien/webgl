import {TweenMax, TimelineMax} from "gsap";
import Vivus from "vivus";
import CONSTANTS from "../datas/Constants";
import * as THREE from 'three';

import PlanetGenerator from "./PlanetGenerator";
import Particle from '../models/Particle';
import TWEEN from 'tween.js';
import GalaxyManager from '../managers/GalaxyManager';

class AnimationsManager {
    constructor(dataManager) {
        this.timeline = new TimelineMax();
        this.timelineSVG = new TimelineMax();
        this.clockStart = Date.now();
        this.wrapperSize = document.querySelector('.byo-wrapper').offsetWidth;
        this.dataManager = dataManager;
        this.buildSVG('byo_intro_constellation_');

        this.planetGenerator = new PlanetGenerator(this.dataManager);
    }


    launchNextAnimation(nbAnimation, family) {
        switch (nbAnimation) {
            case -1: {
                this.startLoader();
                break;
            }
            case 0: {
                this.finishLoader();
                this.startIntro();
                break;
            }
            case 1: {
                this.finishIntro();
                this.startBirthdate();
                break;
            }
            case 2: {
                this.finishBirthdate();
                this.startFamily();
                break;
            }
            case 3: {
                this.rebaseCamera(()=>{
                    this.finishFamily();
                    this.startName();
                });
                break;
            }
            case 4: {
                this.rebaseCamera( () => {
                    this.finishName();
                    this.planetGenerator.instantiatePlanet(this.dataManager.familyManager.family);
                    this.planetGenerator.planet.enablePlanet(new THREE.Vector3(7.3, -1.0, 0), this.startStoryOne.bind(this), null, 3000);
                    this.planetGenerator.updateCurrentPlanet();
                    this.allowExploration = true;
                    this.explorationPhase();
                });

                break;
            }
            case 5: {
                this.rebaseCamera( () =>{
                    this.finishStoryOne();

                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(4.5, 1.5, 0), this.startStoryTwo(), null, 2000);
                    this.planetGenerator.updateCurrentPlanet();

                });
                break;
            }
            case 6: {
                this.rebaseCamera( () =>{

                    this.finishStoryTwo();
                    this.allowExploration = false;

                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(0, -6.5, 0), this.startStoryThree(), null, 2000);

                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(5.5, 5.5, 5.5), ()=>{} , null, 2000, true);
                    this.planetGenerator.stopAmbiantLight( this.planetGenerator.setLightChoice(),null,2000);
                    this.planetGenerator.setSpline(this.planetGenerator.updateCurrentPlanet());

                });

                break;
            }
            case 7: {

                this.rebaseCamera( () =>{

                    this.finishStoryThree();
                    this.startStoryFour();

                    this.planetGenerator.removeSpline();
                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(6.0, 6.0, 6.0), ()=>{
                        //shake
                        this.planetGenerator.planet.shakePlanet(new THREE.Vector3(-1, -9, 0), () => {}, TWEEN.Easing.Linear.InOut, 64);
                    } , null, 2000, true);
                    let rotationValue = Math.PI/2;

                    switch(this.dataManager.user.light){
                        case "0":

                            break;

                        case "1":
                            //lighted
                            this.planetGenerator.planet.orientPlanet(-rotationValue);
                            break;

                        case "2":
                            //darked
                            this.planetGenerator.planet.orientPlanet(rotationValue);
                            break;

                        default:
                            console.error("no choice ?");
                    }
                    this.planetGenerator.updateCurrentPlanet();
                });

                break;
            }
            case 8: {

                this.rebaseCamera( () =>{
                    this.finishStoryFour();
                    this.allowExploration = true;

                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(9.0, 9.0, 9.0), ()=>{} , null, 2000, true);
                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(18, 0, -15),  this.startStoryFive(),null, 2500);
                    this.planetGenerator.planet.rotate = true;
                    this.planetGenerator.updateCurrentPlanet();

                    this.planetGenerator.lightOff(() => {
                        this.planetGenerator.settingMaterials(this.planetGenerator.resetDefaultLight())
                    }, null, 1500);

                });


                break;
            }
            case 9: {

                this.rebaseCamera( () =>{
                    this.planetGenerator.addMoodElement();

                    this.finishStoryFive();
                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(10, 6, 0),  this.startStorySix(),null, 2500);
                    this.planetGenerator.updateCurrentPlanet();
                });

                break;
            }
            case 10: {
                this.rebaseCamera( () =>{
                    this.finishStorySix();
                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(18.1, -3.5, -18.9),this.startStorySeven(),null, 2500);
                    this.planetGenerator.updateCurrentPlanet();
                });

                break;
            }
            case 11: {
               this.rebaseCamera( () =>{
                   this.finishStorySeven();
                   this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(0, -4, -30), this.startStoryEight(),null, 2500);
                   this.planetGenerator.updateCurrentPlanet();
               });

                break;
            }
            case 12: {
               this.rebaseCamera( () =>{
                   this.finishStoryEight();
                   this.startStoryNine();
                });
                break;
            }
            case 13: {
               this.rebaseCamera( () =>{
                   this.finishStoryNine();
                   this.startStoryTen();
                });
                break;
            }
            case 14: {
               this.rebaseCamera( () =>{
                   this.finishStoryTen();
                   this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(-13, 3, -30), this.startStoryEleven(),null, 2500);
                   this.planetGenerator.updateCurrentPlanet();

                   this.dataManager.user.finished = true;
                   this.dataManager.firebase.updateUser(this.dataManager.user);
               });

                break;
            }
            case 15: {
                this.rebaseCamera( () =>{
                    this.finishStoryEleven();
                    this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(0, 0, -30), this.startStoryTwelve(),null, 3000);
                    this.planetGenerator.updateCurrentPlanet();
                });

                break;
            }
            case 16: {
                this.allowExploration = false;


                window.mainController.orbitController.enableRotate = true;
                window.mainController.orbitController.enableZoom = true;

               this.rebaseCamera( () =>{
                   this.finishStoryTwelve();
                   this.planetGenerator.planet.tweenPlanet(new THREE.Vector3(0, 0, 0),  ()=>{

                       for(let child of window.currentPlanet.childs){
                           if(child instanceof Particle){
                               child.setHeartBeats(0);
                           }
                       }
                       this.startStoryThirteen();
                       this.planetGenerator.updateCurrentPlanet();
                       window.mainController.galaxyManager = new GalaxyManager({
                           family: this.dataManager.user.family,
                           orbitController: window.mainController.orbitController,
                           dataManager: window.mainController.dataManager,
                           mainController: window.mainController,
                           datGui: window.mainController.datGui,
                           clock: window.mainController.clock,
                           camera: window.mainController.camera.getCamera(),
                           isShow: true,
                       });
                       window.mainController.galaxyManager.getNewFamily(this.dataManager.user.family);


                   },TWEEN.Easing.Back.In, 3000,true);
                });

                break;
            }
            case 17: {
                console.log('17');
                window.mainController.galaxyManager && window.mainController.galaxyManager.destroyGalaxy( () => {
                    this.rebaseCamera( () =>{
                       this.finishStoryThirteen();
                        // this.finishStoryFifteen();
                        this.startStoryFourteen();
                    });
                });
                break;
            }
            case 18: {
                console.log('18');
                this.finishStoryFourteen();
                this.startStoryFifteen(family);

                break;
            }
            case 19: {
                console.log('19');
                window.mainController.galaxyManager && window.mainController.galaxyManager.destroyGalaxy(
                   () => {
                       this.rebaseCamera(()=>{
                           this.finishStoryFifteen();
                           this.startStoryFourteen();
                       });
                   }
               );
                break;
            }
            default: {
                console.warn('AnimationsManager : Error with requested animation.', nbAnimation);
                break;
            }
        }
    }


    rebaseCamera(cb){
        let vector = new THREE.Vector3(0,0,20);

        if(
            Math.abs(window.mainController.camera.getCamera().position.x - vector.x ) > 1 ||
            Math.abs(window.mainController.camera.getCamera().position.y - vector.y ) > 1 ||
            Math.abs(window.mainController.camera.getCamera().position.z - vector.z ) > 1
        ){

            new TWEEN.Tween(window.mainController.camera.getCamera().position)
                .to(new THREE.Vector3(0,0,20),3000)
                .easing( TWEEN.Easing.Quartic.InOut)
                .onComplete( () => {
                    cb && cb();
                })
                .start();
        }else{
            cb && cb();
        }

    }

    startLoader() {
        let targetId = 'byo_loader_svg';
        let target = document.getElementById(targetId);
        this.endLoader = false;

        this.loaderSVG = new Vivus(targetId, {
            duration: 90,
            start: 'manual'
        }, (element) => {
            if (element.getStatus() === 'end' && !this.endLoader) {
                element.reset();
                element.play();
            }
            // element.play(element.getStatus() === 'end' && !this.endLoader ? -1 : 1);
        });

        this.timeline.to(target, 1, {opacity: 1, onComplete: () => {
            this.loaderSVG.play();
        }});
        this.timeline.to('#app', 2, {opacity: 1});
        this.timeline.to(document.querySelectorAll('.byo-intro-constellation > svg'), 1.5, {opacity: 0.35}, '-=.3');
    }

    finishLoader() {
        let targetId = 'byo_loader';
        let target = document.getElementById(targetId);

        let posTransition = (this.wrapperSize / 2) - 48;
        let timeLoopLoader = 1.2;
        let minDuration = '+=' + (timeLoopLoader * 5 - (Date.now() - this.clockStart) / 1000);

        this.timeline.to(target, 1, {x: -posTransition, onStart: () => {
            this.endLoader = true;
            this.loaderSVG && this.loaderSVG.destroy();
        }}, minDuration);
    }

    startIntro() {
        this.toggleStep('.byo-intro', 'display');
        this.timeline.from(document.querySelector('.byo-intro-slogan'), .7, {opacity: 0});
        this.timeline.from(document.querySelector('.byo-intro-desc'), .7, {opacity: 0});
        this.timeline.from(document.querySelector('.byo-intro-start'), .5, {opacity: 0, y: 20}, '+=.5');

        this.drawSVGs('intro');
    }

    finishIntro() {
        this.timeline.to(document.querySelector('.byo-intro-slogan'), .4, {opacity: 0, y: 20});
        this.timeline.to(document.querySelector('.byo-intro-desc'), .4, {opacity: 0, y: 20}, '-=.1');
        this.timeline.to(document.querySelector('.byo-intro-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-intro', 'hide');
        }}, '-=.1');
        this.timeline.staggerTo(document.querySelectorAll('.byo-intro-constellation > svg'), .5, {opacity: 0}, .1, "-=1");
        this.timeline.to(document.querySelectorAll('.byo-intro-constellation > svg'), 0, {autoAlpha: 0});
    }


    startBirthdate() {
        this.timeline.to(document.getElementById('byo_loader'), .7, {scale: 0.6, y: -160, x: -(window.innerWidth / 2 - 100)}, "+=.3");

        this.toggleStep('.byo-birthdate', 'display');
        this.timeline.from(document.querySelector('.byo-birthdate-desc'), .7, {opacity: 0});
        this.timeline.from(document.querySelector('.byo-birthdate-question'), .7, {opacity: 0});
        this.timeline.staggerFrom(document.querySelectorAll('.byo-birthdate-clock'), .3, {opacity: 0, x: 30}, .1);
    }

    finishBirthdate() {
        this.timeline.to(document.querySelector('.byo-birthdate-desc'), .4, {opacity: 0, y: 20}, '-=.1');
        this.timeline.to(document.querySelector('.byo-birthdate-question'), .4, {opacity: 0, y: 20});
        this.timeline.staggerTo(document.querySelectorAll('.byo-birthdate-clock'), .9, {opacity: 0, rotation: 80}, .2, "-=0.3");
        this.timeline.to(document, 0, {onComplete: () => {
            this.toggleStep('.byo-birthdate', 'hide');
        }});
    }

    startFamily() {
        let currentSignClass = 'byo_current_sign';
        let currentElementClass = 'byo_current_element';
        this.toggleStep('.byo-family', 'display');
        document.querySelector('.byo-family-sign svg').setAttribute('id', currentSignClass);
        document.querySelector('.byo-family-element svg').setAttribute('id', currentElementClass);

        let sign = new Vivus(currentSignClass, {
            start: 'manual'
        });

        let element = new Vivus(currentElementClass, {
            start: 'manual'
        });

        this.timeline.from(document, .3, {onComplete: () => {
            sign.play();
        }});

        this.timeline.from(document.querySelector('.byo-family-desc'), .7, {opacity: 0}, "+=3");

        this.timeline.from(document, 1, {onComplete: () => {
            element.play();
        }},);

        this.timeline.from(document.querySelector('.byo-family-start'), .5, {opacity: 0, y: -20}, "+=3");
    }

    finishFamily() {
        this.timeline.to(document.querySelector('.byo-family-sign'), .4, {opacity: 0});
        this.timeline.to(document.querySelector('.byo-family-desc'), .4, {opacity: 0});
        this.timeline.to(document.querySelector('.byo-intro-constellation-isActive'), .4, {opacity: 0}, "-=.4");
        this.timeline.to(document.querySelector('.byo-family-element'), .4, {opacity: 0, y: 20});
        this.timeline.to(document.querySelector('.byo-family-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-family', 'hide');
        }});
    }

    startName() {
        this.toggleStep('.byo-name', 'display');
        this.timeline.from(document.querySelector('.byo-name-desc'), .7, {opacity: 0});
        this.timeline.from(document.querySelector('.byo-name-families-prev'), .5, {opacity: 0}, '-=.7');
        this.timeline.from(document.querySelector('.byo-name-question'), .7, {opacity: 0});
        this.timeline.from(document.querySelector('.byo-name-families-link'), .5, {scaleY: 0}, '-=.9');
        this.timeline.from(document.querySelector('.byo-name-families-current'), .5, {opacity: 0}, '-=.5');

        this.timeline.from(document.querySelector('.byo-name-form'), .7, {opacity: 0, x: 30});
    }

    finishName() {
        let currentFamily = document.querySelector('.byo-name-families-current');
        this.timeline.to(document.querySelector('.byo-name-desc'), .4, {opacity: 0, y: 20});
        this.timeline.to(document.querySelector('.byo-name-families-prev'), .3, {opacity: 0}, '-=.4');
        this.timeline.to(document.querySelector('.byo-name-question'), .4, {opacity: 0, y: 20});
        this.timeline.to(document.querySelector('.byo-intro-constellation-isActive'), .4, {opacity: 0}, "-=.4");
        this.timeline.to(document.querySelector('.byo-name-families-link'), .3, {scaleY: 0}, '-=.6');
        this.timeline.to(currentFamily, .3, {opacity: 0, onComplete: () => {
            currentFamily.parentNode.removeChild(currentFamily);
        }}, '-=.3');
        this.timeline.to(document.querySelector('.byo-name-form'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-name', 'hide');
        }});
    }

    startStoryOne() {
        this.toggleStep('.byo-story-one', 'display');
        let currentStory = document.querySelector('.byo-story-one');

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.from(currentStory.querySelector('.byo-story-one-gif'), .7, {opacity: 0, y: 20});
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryOne() {
        let currentStory = document.querySelector('.byo-story-one');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-one-gif'), .7, {opacity: 0, y: 20});
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-one', 'hide');
        }}, '-=.3');
    }

    startStoryTwo() {
        this.toggleStep('.byo-story-two', 'display');
        let currentStory = document.querySelector('.byo-story-two');

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryTwo() {
        let currentStory = document.querySelector('.byo-story-two');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-two', 'hide');
        }}, '-=.3');
    }

    startStoryThree() {
        this.toggleStep('.byo-story-three', 'display');
        let currentStory = document.querySelector('.byo-story-three');

        this.timeline.from(currentStory.querySelector('.byo-story-desc'), .7, {opacity: 0});
        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-three-title'), .7, {opacity: 0}, .2);
        this.timeline.from(currentStory.querySelector('.byo-story-action'), .7, {opacity: 0});
    }

    finishStoryThree() {
        let currentStory = document.querySelector('.byo-story-three');

        this.timeline.to(currentStory.querySelector('.byo-story-desc'), .7, {opacity: 0, y: 20});
        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-three-title'), .7, {opacity: 0}, .2);
        this.timeline.to(currentStory.querySelector('.byo-story-action'), .5, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-three', 'hide');
        }});
    }

    startStoryFour() {
        this.toggleStep('.byo-story-four', 'display');
        let currentStory = document.querySelector('.byo-story-four');

        let opacityAsteroide = 0;
        let lightChoice = this.dataManager.user.light;

        if (lightChoice === 0) {
            opacityAsteroide = 0.8;
        } else if (lightChoice === 1) {
            opacityAsteroide = 0.6;
        } else {
            opacityAsteroide = 0.4;
        }

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-four-asteroide'), .7, {opacity: opacityAsteroide}, "+=1.2");
        this.timeline.from(currentStory.querySelector('.byo-story-four-form'), .5, {opacity: 0, y: 20});
    }

    finishStoryFour() {
        let currentStory = document.querySelector('.byo-story-four');

        this.timeline.to(currentStory.querySelector('.byo-story-four-asteroide'), .3, {opacity: 0});
        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-four-form'), .5, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-four', 'hide');
        }});
    }

    startStoryFive() {
        this.toggleStep('.byo-story-five', 'display');
        let currentStory = document.querySelector('.byo-story-five');

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryFive() {
        let currentStory = document.querySelector('.byo-story-five');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-five', 'hide');
        }}, '-=.3');
    }

    startStorySix() {
        this.toggleStep('.byo-story-six', 'display');
        let currentStory = document.querySelector('.byo-story-six');

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStorySix() {
        let currentStory = document.querySelector('.byo-story-six');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-six', 'hide');
        }}, '-=.3');
    }

    startStorySeven() {
        this.toggleStep('.byo-story-seven', 'display');
        let currentStory = document.querySelector('.byo-story-seven');

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStorySeven() {
        let currentStory = document.querySelector('.byo-story-seven');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-seven', 'hide');
        }}, '-=.3');
    }

    startStoryEight() {
        this.toggleStep('.byo-story-eight', 'display');
        let currentStory = document.querySelector('.byo-story-eight');

        this.dataManager.storyManager.initHeartInteraction();

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-eight-interaction > div'), .7, {opacity: 0, x: 20}, .3);
    }

    finishStoryEight() {
        let currentStory = document.querySelector('.byo-story-eight');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .4, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-eight-interaction-chrono'), .4, {opacity: 0});
        this.timeline.to(currentStory.querySelector('.byo-story-eight-interaction-heart'), .4, {opacity: 0}, '-=.4');
        this.timeline.to(currentStory.querySelector('.byo-story-eight-interaction-arrow'), .4, {opacity: 0}, '-=.4');
        this.timeline.to(currentStory.querySelector('.byo-story-eight-interaction-space'), .4, {opacity: 0, onComplete: () => {
            this.toggleStep('.byo-story-eight', 'hide');
        }}, '-=.4');
    }

    startStoryNine() {
        this.toggleStep('.byo-story-nine', 'display');
        let currentStory = document.querySelector('.byo-story-nine');

        this.timeline.from(currentStory.querySelector('.byo-story-desc'), .7, {opacity: 0});
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryNine() {
        let currentStory = document.querySelector('.byo-story-nine');

        this.timeline.to(currentStory.querySelector('.byo-story-desc'), .7, {opacity: 0, y: 20});
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-nine', 'hide');
        }}, '-=.3');
    }

    startStoryTen() {
        this.toggleStep('.byo-story-ten', 'display');
        let currentStory = document.querySelector('.byo-story-ten');

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0}, .3);
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryTen() {
        let currentStory = document.querySelector('.byo-story-ten');

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-desc'), .7, {opacity: 0, y: 20}, .3);
        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-ten', 'hide');
        }}, '-=.3');
    }

    startStoryEleven() {
        this.toggleStep('.byo-story-eleven', 'display');
        let currentStory = document.querySelector('.byo-story-eleven');

        let position = ((window.innerWidth / 2) - (this.wrapperSize / 2) + 130);
        TweenMax.set(currentStory, {left: position});

        this.dataManager.storyManager.initPlanetCard();

        this.timeline.from(currentStory.querySelector('.byo-story-eleven-number'), .7, {opacity: 0});
        this.timeline.from(currentStory.querySelector('.byo-story-eleven-title'), .7, {opacity: 0}, '-=.3');
        this.timeline.from(currentStory.querySelector('.byo-story-eleven-name'), .7, {opacity: 0});

        this.timeline.staggerFrom(currentStory.querySelectorAll('.byo-story-eleven-list-item'), .7, {opacity: 0, y: 20}, 1.5, "+=1");

        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryEleven() {
        let currentStory = document.querySelector('.byo-story-eleven');

        this.timeline.to(currentStory.querySelector('.byo-story-eleven-number'), .4, {opacity: 0});
        this.timeline.to(currentStory.querySelector('.byo-story-eleven-title'), .4, {opacity: 0}, '-=.1');
        this.timeline.to(currentStory.querySelector('.byo-story-eleven-name'), .4, {opacity: 0});

        this.timeline.staggerTo(currentStory.querySelectorAll('.byo-story-eleven-list-item'), .4, {opacity: 0, y: 20}, .5, "+=.3");

        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-eleven', 'hide');
        }}, '-=.1');
    }

    startStoryTwelve() {
        this.toggleStep('.byo-story-twelve', 'display');
        let currentStory = document.querySelector('.byo-story-twelve');

        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryTwelve() {
        let currentStory = document.querySelector('.byo-story-twelve');

        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-twelve', 'hide');
        }}, '-=.3');
    }

    startStoryThirteen() {
        this.toggleStep('.byo-story-thirteen', 'display');
        let currentStory = document.querySelector('.byo-story-thirteen');

        let loader = document.querySelector('.byo-loader');

        this.timeline.to(loader, .5, {opacity: 0, onComplete: () => {
            // loader.classList.add('byo-loader-isFinalePosition');
        }});

        this.timeline.from(currentStory.querySelector('.byo-story-thirteen-family-icon'), .7, {scale: 0});
        this.timeline.from(currentStory.querySelector('.byo-story-family'), .4, {opacity: 0, y: 20});
        this.timeline.from(currentStory.querySelector('.byo-story-desc'), .7, {opacity: 0});
        this.timeline.from(currentStory.querySelector('.byo-story-start'), .5, {opacity: 0, y: 20});
    }

    finishStoryThirteen() {
        let currentStory = document.querySelector('.byo-story-thirteen');

        this.timeline.to(currentStory.querySelector('.byo-story-thirteen-family-icon'), .4, {scale: 0});
        this.timeline.to(currentStory.querySelector('.byo-story-family'), .4, {opacity: 0, y: 20});
        this.timeline.to(currentStory.querySelector('.byo-story-desc'), .4, {opacity: 0});

        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-thirteen', 'hide');
        }}, '-=.3');
    }

    startStoryFourteen() {
        this.toggleStep('.byo-story-fourteen', 'display');
        let currentStory = document.querySelector('.byo-story-fourteen');

        this.timeline.staggerTo(currentStory.querySelectorAll('path'), .7, {opacity: 0.35}, .3);
        this.timeline.staggerTo(currentStory.querySelectorAll('.g-style'), .7, {opacity: 0.5}, .4);
        this.timeline.to(document.querySelector('.byo-loader'), .7, {autoAlpha: 1});
        this.timeline.from(currentStory.querySelector('.byo-story-desc'), .7, {opacity: 0});
    }

    finishStoryFourteen() {
        let currentStory = document.querySelector('.byo-story-fourteen');
        console.log("finish story Fourteen");
        this.timeline.staggerTo(currentStory.querySelectorAll('.g-style'), .4, {opacity: 0}, .2);
        this.timeline.staggerTo(currentStory.querySelectorAll('path'), .4, {opacity: 0}, .2);
        this.timeline.to(document.querySelector('.byo-loader'), .4, {autoAlpha: 0});
        this.timeline.to(currentStory.querySelector('.byo-story-desc'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-fourteen', 'hide');
        }}, '-=.3');
    }

    startStoryFifteen(family) {
        this.toggleStep('.byo-story-fifteen', 'display');
        let currentStory = document.querySelector('.byo-story-fifteen');

        let loader = document.querySelector('.byo-loader');

        this.timeline.to(loader, .5, {opacity: 0, onComplete: () => {
            // loader.classList.add('byo-loader-isFinalePosition');
        }});

        this.timeline.to(currentStory.querySelector('.byo-story-fifteen-family-icon'), .7, {scale: 1});
        this.timeline.to(currentStory.querySelector('.byo-story-fifteen-family-name'), .4, {opacity: 1, y: 0});

        this.timeline.to(currentStory.querySelector('.byo-story-start'), .5, {opacity: 1, y: 0, onComplete: () => {
            window.mainController.galaxyManager.getNewFamily(family)
        }});
    }

    finishStoryFifteen() {
        let currentStory = document.querySelector('.byo-story-fifteen');

        this.timeline.to(currentStory.querySelector('.byo-story-fifteen-family-icon'), .4, {scale: 0});
        this.timeline.to(currentStory.querySelector('.byo-story-fifteen-family-name'), .4, {opacity: 0, y: 20});

        this.timeline.to(currentStory.querySelector('.byo-story-start'), .3, {opacity: 0, y: 20, onComplete: () => {
            this.toggleStep('.byo-story-fifteen', 'hide');
        }}, '-=.3');
    }

    toggleStep(selector, type) {
        let activeClass = 'byo-step-isActive';

        if (type === 'display') {
            document.querySelector(selector).classList.add(activeClass);
        } else {
            document.querySelector(selector).classList.remove(activeClass);
        }
    }


    buildSVG(base) {
        let currentSVG;
        let currentID;
        let i = 0;
        this.astroSVG = [];
        for (i; i < CONSTANTS.ASTRO.length; i++) {
            currentID = base + i;
            currentSVG = new Vivus(currentID, {
                duration: 50,
                delay: 20,
                start: 'manual'
            }, () => {
            });
            currentSVG.reset();
            this.astroSVG.push({
                currentID: currentID,
                currentSVG: currentSVG
            });
        }
    }

    drawSVGs(type) {
        if (type === 'intro') {
            let randDelay = 0;
            for (let astro of this.astroSVG) {
                randDelay = '+=' + Math.random();
                this.timelineSVG.to(document, 1, {onComplete: () => {
                    astro.currentSVG.duration = 3000;
                    astro.currentSVG.play();
                }}, randDelay);
            }
        }
    }


    explorationPhase(){
        let scaleReference = new THREE.Vector3(4,4,4);
        this.isExplorationPhase = false;
        document.addEventListener('keydown', (event) => {
            if (event.keyCode === 	80 && window.currentPlanet && !this.isExplorationPhase && this.allowExploration) {
                window.mainController.orbitController.enableRotate = true;
                this.isExplorationPhase = true;
                this.oldScaleCurrentPlanet = window.currentPlanet.mesh.scale.clone();
                let scale = this.oldScaleCurrentPlanet;
                console.log(scale.x + ' ' + scale.y + ' ' + scale.z);
                let target = window.mainController.orbitController.target;
                let coords = {
                    x: target.x, y: target.y, z: target.z,
                    planetScaleX: scale.x,  planetScaleY: scale.y,  planetScaleZ: scale.z };

                new TWEEN.Tween(coords)
                    .to({
                        x: window.currentPlanet.mesh.position.x, y: window.currentPlanet.mesh.position.y, z: window.currentPlanet.mesh.position.z,
                        planetScaleX: scaleReference.x,  planetScaleY: scaleReference.y,  planetScaleZ: scaleReference.z
                    }, 700)
                    .easing(TWEEN.Easing.Quadratic.InOut)
                    .onUpdate(() => {

                        window.mainController.orbitController.target.x = coords.x;
                        window.mainController.orbitController.target.y = coords.y;
                        window.mainController.orbitController.target.z = coords.z;

                        window.currentPlanet.mesh.scale.x = coords.planetScaleX;
                        window.currentPlanet.mesh.scale.y = coords.planetScaleY;
                        window.currentPlanet.mesh.scale.z = coords.planetScaleZ;

                    })
                    .onComplete( () => {
                        TweenMax.to(document.querySelector('.byo-step-isActive'), .3, { autoAlpha: 0});
                    })
                    .start();
            }
        });

        document.addEventListener('keyup', (event) => {
            if (event.keyCode === 	80 && window.currentPlanet && this.isExplorationPhase && this.allowExploration ) {
                let target = window.mainController.orbitController.target;
                let camera = window.mainController.camera.getCamera().position;
                let oldScale = window.currentPlanet.mesh.scale.clone();
                let scale = this.oldScaleCurrentPlanet;
                console.log(scale.x + ' ' + scale.y + ' ' + scale.z);
                let coords = {
                    x: target.x, y: target.y, z: target.z,
                    cameraX: camera.x, cameraY: camera.y, cameraZ: camera.z,
                    planetScaleX: oldScale.x,  planetScaleY: oldScale.y,  planetScaleZ: oldScale.z,

                };

                new TWEEN.Tween(coords)
                    .to({
                        x: 0, y: 0, z: 0,
                        cameraX: 0, cameraY:0, cameraZ: 20,
                        planetScaleX: scale.x,  planetScaleY: scale.y,  planetScaleZ: scale.z

                    }, 700)
                    .easing(TWEEN.Easing.Quadratic.InOut)
                    .onUpdate(() => {
                        window.mainController.orbitController.target.x = coords.x;
                        window.mainController.orbitController.target.y = coords.y;
                        window.mainController.orbitController.target.z = coords.z;

                        window.mainController.camera.getCamera().position.x = coords.cameraX;
                        window.mainController.camera.getCamera().position.y = coords.cameraY;
                        window.mainController.camera.getCamera().position.z = coords.cameraZ;

                        window.currentPlanet.mesh.scale.x = coords.planetScaleX;
                        window.currentPlanet.mesh.scale.y = coords.planetScaleY;
                        window.currentPlanet.mesh.scale.z = coords.planetScaleZ;

                    })
                    .onComplete( () => {
                        this.isExplorationPhase = false;
                        window.mainController.orbitController.enableRotate = false;
                        TweenMax.to(document.querySelector('.byo-step-isActive'), .3, { autoAlpha: 1});
                    })
                    .start();

            }
        });
    }


}

export default AnimationsManager;