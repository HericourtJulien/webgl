import * as firebase from "firebase";
import CONSTANTS from "../datas/Constants";

class FirebaseManager {

    constructor() {
        this.initFirebase();
        this.count = 0;

        return this;
    }

    initFirebase() {
        let config = {
            apiKey: "AIzaSyDEBxKXgS6-QFWFzvPWO8YqoJvs5TEBKNQ",
            authDomain: "byo-project.firebaseapp.com",
            databaseURL: "https://byo-project.firebaseio.com",
            projectId: "byo-project",
            storageBucket: "",
            messagingSenderId: "384389430660"
        };
        firebase.initializeApp(config);

        this.bdd = firebase.database();
        this.bindGlobalCount();
    }

    addUser(user, callback) {
        this.bdd.ref(CONSTANTS.PATHS[user.astro] + user.id).once('value').then((existingUser) => {
            if (existingUser.val()) {
                console.warn('Classe Firebase : Utilisateur déjà enregistré.');
            } else {
                this.count++;
                this.updateGlobalAccount();

                this.updateUser(user);
                callback();
            }
        });
    }

    updateUser(user) {
        this.bdd.ref(CONSTANTS.PATHS[user.astro] + user.id).set(user);
    }

    getStaticUsersOfSign(sign, callback) {
        this.bdd.ref(CONSTANTS.PATHS[sign]).once('value').then((users) => {
            callback(users.val());
        })
    }

    bindGlobalCount() {
        this.bdd.ref(CONSTANTS.PATHS.count).once('value').then((data) => {
           this.count = data.val();
        });
    }

    updateGlobalAccount() {
        this.bdd.ref(CONSTANTS.PATHS.count).set(this.count);
        window.globalCount = this.count;
    }

    getSyncUserOfSign(astro, userId, callback) {
        this.bdd.ref(CONSTANTS.PATHS[astro] + userId).on('value', (user) => {
            callback(user.val());
        });
    }

    getStaticFinishedUsersByFamily(family, callback) {
        let signsOfFamily = CONSTANTS.FAMILYSIGN[family];
        let usersFinishedOfFamily = [];
        let cpt = 0;

        for (let sign of signsOfFamily) {
            this.bdd.ref(CONSTANTS.PATHS[sign]).once('value').then((data) => {
                let allUsersOfFamily = data.val();
                cpt++;
                if (allUsersOfFamily !== null) {
                    for (let key of Object.keys(allUsersOfFamily)) {
                        if (allUsersOfFamily[key].finished) {
                            usersFinishedOfFamily.push(allUsersOfFamily[key]);
                        }
                    }
                }
                if (cpt === 3) {
                    callback(usersFinishedOfFamily);
                }
            });
        }
    }
}

export default FirebaseManager;
