import crypto from "crypto";
import User from "../datas/User";
import CONSTANTS from "../datas/Constants";
import FirebaseManager from "./FirebaseManager";
import IndicoManager from "./IndicoManager"
import AnimationsManager from "./AnimationsManager";
import BirthdateManager from "./BirthdateManager";
import FamilyManager from "./FamilyManager";
import NameManager from "./NameManager";
import StoryManager from "./StoryManager";
import {getFamilyByAstroSign} from "../datas/Helpers";


class DataManager {

    constructor() {
        this._bindEvents();

        // Init managers
        this.initAnimationsManager();
        this.initIndicoManager();
        this.initBirthdateManager();

        this.animationsManager.launchNextAnimation(-1);
    }

    initFirebase(birthdate, astroSign) {
        this.firebase = new FirebaseManager();
        this.generateNewUser(birthdate, astroSign);
        this.initFamilyManager(astroSign);
    }

    initIndicoManager() {
        this.indicoManager = new IndicoManager();
    }

    initAnimationsManager() {
        this.animationsManager = new AnimationsManager(this);
    }

    initBirthdateManager() {
        this.birthdateManager = new BirthdateManager(this.animationsManager, this);
    }

    initFamilyManager(astroSign) {
        this.familyManager = new FamilyManager(astroSign, getFamilyByAstroSign(astroSign), this.animationsManager, this);
    }

    initNameManager(family) {
        this.nameManager = new NameManager(family, this.animationsManager, this, (name) => {
            this.user.name = name;
            this.firebase.updateUser(this.user);
        });
    }

    initStoryManager() {
        this.storyManager = new StoryManager(this.animationsManager, this, (propertyName, property) => {
            this.user[propertyName] = property;
            this.firebase.updateUser(this.user);
        });
    }

    _bindEvents() {
        document.getElementById('byo_loader').addEventListener('click', () => location.reload());
        let targets = document.querySelectorAll('[data-target]');
        let myFunction;

        for (let target of targets) {
            myFunction = (event) => this.changeStep(target, event);
            target.myFunction = myFunction;
            target.addEventListener('click', myFunction);
        }
    }

    changeStep(elm) {

        let dataNumber = parseInt(elm.getAttribute('data-target'));
        if(dataNumber != 19){
            elm.removeEventListener('click', elm.myFunction);
        }

        window.audioManager.playClick();

        switch (dataNumber) {
            case 1:
                this.birthdateManager.buildRings(dataNumber);
                break;
            default:
                this.animationsManager.launchNextAnimation(dataNumber);
                break;
        }
    }


    generateNewUser(birthdate, astroSign) {
        let obj = {
            id: crypto.randomBytes(16).toString('hex'),
            birthdate: birthdate,
            astro: astroSign,
            family: getFamilyByAstroSign(astroSign)
        };

        this.user = new User(obj);
        this.firebase.addUser(this.user, () => {
            this.watchUser(astroSign, this.user.id);
        });

        console.warn('Classe DataManager : New user created.');
    }

    watchUser(astro, userId) {
        /*this.firebase.getSyncUserOfSign(astro, userId, (user) => {
            this.user = new User(user);
            document.querySelector('#user').innerHTML = JSON.stringify(this.user);
        });*/
    }

    getAllUsersOfSign(astro) {
        this.firebase.getStaticUsersOfSign(astro, (users) => {
            this.allUsers = users;
        });
    }

}

export default DataManager;