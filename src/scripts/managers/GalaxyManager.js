import * as THREE from 'three';
import Planet from '../models/planet/Planet';
import Fruits from '../models/fruits/Fruits';
import TWEEN from 'tween.js';

import Particle from '../models/Particle'

import FirebaseManager from '../managers/FirebaseManager'

import User from "../datas/User";
import PlanetGenerator from '../managers/PlanetGenerator';



class GalaxyManager {

    constructor(params) {

        // Get Params
        this.family = params.family ? params.family : 'air';
        this.camera = params.camera;
        this.orbitController = params.orbitController;
        this.dataManager = params.dataManager;
        this.datGui = params.datGui;
        this.clock = params.clock;
        this.mainController = params.mainController;
        this.galaxyName = params.galaxyName;
        this.isShow = params.isShow || false;

        // AnimationToggler to avoid multi-animations planets
        this.isAnimate = false;

        this.planets = [];

        // Settings for custom Galaxy
        this.gridWith = 10;
        this.gridColMax = 10;
        this.totalLines = Math.ceil(this.nbUsers/this.gridColMax);

        // Create PlanetGenerator
        console.log("GalaxyManager : Load PlanetGenerator");
        this.planetGenerator = new PlanetGenerator(this.dataManager, true);
    }

    getNewFamily(family){
        this.planets = [];
        this.family = family;
        console.log("GalaxyManager : Load Family " + this.family);
        this.dataManager.firebase.getStaticFinishedUsersByFamily(this.family, this.onFireBaseResult.bind(this));
    }

    onFireBaseResult(users){

        this.users = users;
        this.nbUsers = this.users.length;
        this.isShow && this.createGalaxy();
        this.updateDatGui();
    }

    // Call when loadingManager has load every ressources
    onLoadingManagerReady(){
        this.tweenCameraPosition(new THREE.Vector3(this.camera.position.x,this.camera.position.y, 200), null , TWEEN.Easing.Quintic.InOut, 3500 );
    }


    // Animate Camera position
    // Callback (optional) onComplete
    tweenCameraPosition(position, cb, easing, duration){

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;

        new TWEEN.Tween(this.camera.position)
            .to(new THREE.Vector3(position.x,position.y,position.z),_duration)
            .easing(_easing)
            .onComplete( () => {
               cb && cb();
            })
            .start();
    }

    // Animate OritContoller to lookAt camera position
    // Callback (optional) onComplete
    tweenLookAtPosition(position, cb){


        new TWEEN.Tween(this.orbitController.target)
            .to(new THREE.Vector3(position.x,position.y,position.z),1000)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onComplete( () => {
                cb && cb();
            })
            .start();
    }

    // Call from requestAnimationFrame()
    update() {
        TWEEN.update();
    }

    // Create Galaxy
    createGalaxy() {

        for(let i = 0 ; i <= this.nbUsers - 1 ; i++){

            if(!this.users[i].modelId){
                continue;
            }
            if(i > 150){
                break;
            }

            this.dataManager.user = new User(this.users[i]);

            // --------- Create Grid ---------
            let x = i, y = 1;
            while(x > this.gridColMax){
                x = x - this.gridColMax;
                y ++;
            }

            // ------------------------------------------------------------------------------------

            // -------- Create Planet -------------------------------------------------------------

            let planet = this.planetGenerator.instantiatePlanet(this.family,{
                galaxy: this,
                isPickable: true,
                onPick: (mesh) => {
                    window.currentPlanetPickedOnGalaxy = mesh;
                    // TODO add customEvent on change
                    if(!this.isAnimate){
                        this.isAnimate = !this.isAnimate;
                        this.tweenCameraPosition(new THREE.Vector3(mesh.position.x ,mesh.position.y, mesh.position.z + 15 ), () => {
                            this.tweenPlanet(mesh, new THREE.Vector3(mesh.position.x + 6.5 ,mesh.position.y, mesh.position.z),()=>{

                                if(window.oldPlanetPickedOnGalaxy){
                                    this.tweenPlanet(window.oldPlanetPickedOnGalaxy,
                                        new THREE.Vector3(mesh.position.x - 6.5 ,mesh.position.y, mesh.position.z),()=> {
                                            window.oldPlanetPickedOnGalaxy = mesh;
                                        }, TWEEN.Easing.Quintic.Out,null,false,true)
                                }else{
                                    window.oldPlanetPickedOnGalaxy = mesh;
                                }
                            },TWEEN.Easing.Quintic.In,null,false,true);
                        });
                        this.tweenLookAtPosition(mesh.position, ()=>{ this.isAnimate = !this.isAnimate});
                    }
                },
                placeThisPlaneteOnMyGalaxy: (mesh) => {

                    /* Place it with grid and center it */

                    mesh.position.x = x * this.gridWith - (this.gridWith * (this.gridColMax + 1) / 2);
                    mesh.position.y = y * this.gridWith - (this.gridWith * (this.totalLines + 1) / 2);
                    mesh.position.z = 1;
                    //mesh.position.z = Math.cos(y) * this.gridWith;
                },

                astro: (this.dataManager.user && this.dataManager.user.astro) ?  this.dataManager.user.astro : '',
                light: (this.dataManager.user && this.dataManager.user.light) ?  this.dataManager.user.light : '',
                lightColor: (this.dataManager.user && this.dataManager.user.lightColor) ?  this.dataManager.user.lightColor : '',
                personality: (this.dataManager.user && this.dataManager.user.personality) ?  this.dataManager.user.personality : '',
                particles: (this.dataManager.user && this.dataManager.user.particles) ?  this.dataManager.user.particles : '',
                name: ( this.dataManager.user.attributs && this.dataManager.user.attributs.name) ? name  : '',
                planetId: ( this.dataManager.user.attributs && this.dataManager.user.attributs.planetId) ? this.dataManager.user.attributs.planetId : '',
                textureMeshScale: ( this.dataManager.user.attributs && this.dataManager.user.attributs.textureMeshScale) ? this.dataManager.user.attributs.textureMeshScale : '',
                glow: ( this.dataManager.user.attributs && this.dataManager.user.attributs.glow) ? this.dataManager.user.attributs.glow : '',
                glowPower: ( this.dataManager.user.attributs && this.dataManager.user.attributs.glowPower) ? this.dataManager.user.attributs.glowPower : '',
                outGlow: ( this.dataManager.user.attributs && this.dataManager.user.attributs.outGlow) ? this.dataManager.user.attributs.outGlow : '',
                outPow: ( this.dataManager.user.attributs && this.dataManager.user.attributs.outPow) ? this.dataManager.user.attributs.outPow : '',
                glowingMesh:  ( this.dataManager.user.attributs && this.dataManager.user.attributs.glowingMesh ) ? this.dataManager.user.attributs.glowingMesh  : '',
                glowColor: ( this.dataManager.user.attributs && this.dataManager.user.attributs.glowColor) ? this.dataManager.user.attributs.glowColor : '',
                outGlowColor: ( this.dataManager.user.attributs && this.dataManager.user.attributs.outGlowColor) ? this.dataManager.user.attributs.outGlowColor : '',
                meshOpacity: ( this.dataManager.user.attributs && this.dataManager.user.attributs.meshOpacity) ? this.dataManager.user.attributs.meshOpacity : '',
                texture: ( this.dataManager.user.attributs && this.dataManager.user.attributs.texture) ? this.dataManager.user.attributs.texture : '',
                planetType: ( this.dataManager.user.attributs && this.dataManager.user.attributs.planetType) ? this.dataManager.user.attributs.planetType : '',
                applyShaderToMesh: ( this.dataManager.user.attributs && this.dataManager.user.attributs.applyShaderToMesh) ? this.dataManager.user.attributs.applyShaderToMesh : '',
                replaceMesh: ( this.dataManager.user.attributs && this.dataManager.user.attributs.replaceMesh) ? this.dataManager.user.attributs.replaceMesh : '',
                doubleSided: ( this.dataManager.user.attributs && this.dataManager.user.attributs.doubleSided) ? this.dataManager.user.attributs.doubleSided : '',
                shaderOpacity: ( this.dataManager.user.attributs && this.dataManager.user.attributs.shaderOpacity) ? this.dataManager.user.attributs.shaderOpacity : '',
                //uniforms: ( this.dataManager.user.attributs && this.dataManager.user.attributs.uniforms) ? this.dataManager.user.attributs.uniforms : '',
                shaderTexture: (this.dataManager.user.attributs && this.dataManager.user.attributs.shaderTexture) ? this.dataManager.user.attributs.shaderTexture : '',
                renderUniforms: ( this.dataManager.user.attributs && this.dataManager.user.attributs.renderUniforms) ? this.dataManager.user.attributs.renderUniforms : '',
                texturedMesh: ( this.dataManager.user.attributs && this.dataManager.user.attributs.texturedMesh) ? this.dataManager.user.attributs.texturedMesh : '',
                bumpScaleFactor: ( this.dataManager.user.attributs && this.dataManager.user.attributs.bumpScaleFactor) ? this.dataManager.user.attributs.bumpScaleFactor : '',
                hasShader: ( this.dataManager.user.attributs && this.dataManager.user.attributs.hasShader) ? this.dataManager.user.attributs.hasShader : '',
                bumpMap: ( this.dataManager.user.attributs && this.dataManager.user.attributs.bumpMap) ? this.dataManager.user.attributs.bumpMap : '',
            });

            // ------------------------------------------------------------------------------------

            window.mainController.addElement(planet);
            this.planets.push(planet);
             // this.mainController.addElement(planet,false);
        }

        // Default presentation of galaxy is GRID
        // Uncomment if you want to show them in spirale or sphere
         this.spirale();
        // this.sphere();

        console.log("Galaxy loaded");
        this.onLoadingManagerReady();
    }

    destroyGalaxy(cb){
        for(let i = 0 ;  i < this.planets.length ; i++){
            let planet = this.planets[i];
            this.tweenPlanet(planet,new THREE.Vector3(0, 0, 0),  ()=>{
                for(let child of planet.childs){
                    if(child instanceof Particle){
                        child.setHeartBeats(0);
                    }
                }
                this.mainController.scene.remove(planet.mesh);
                if(i == this.planets.length -1 ){
                    cb && cb();
                    this.planets = [];
                }
            },TWEEN.Easing.Back.In, 2000,true);
        }
    }



    // Display planets in sphere
    sphere(){

        let vector = new THREE.Vector3();
        let spherical = new THREE.Spherical();

        for ( let i = 0, l = this.planets.length; i < l; i ++ ) {
            let phi = Math.acos( -1 + ( 2 * i ) / l );
            let theta = Math.sqrt( l * Math.PI ) * phi;
            let object = this.planets[ i ].mesh;

            spherical.set( 10, phi, theta );
            object.position.setFromSpherical( spherical );
            vector.copy( object.position ).multiplyScalar( 2 );
            object.lookAt( vector );

        }
    }

    // Display planets on spirale
    spirale(){

        for ( let i = 0, l = this.planets.length; i < l; i ++ ) {

            let angle = .4 * i;
            let x = (( 1 + angle ) * Math.cos(angle)) * this.gridWith;
            let y = (( 1 + angle ) * Math.sin(angle)) * this.gridWith;
            let object = this.planets[ i ].mesh;

            object.position.x = x;
            object.position.y = y;
            object.position.z = 1;

        }
    }

    // DATGUI
    updateDatGui() {

        if(this.datGui){
            let guiConfig = {
                GridWidth: this.gridWith
            };

            this.datGui.add(guiConfig, 'GridWidth', 1, 100).onChange((v) => {
                this.gridWith = v;
                this.spirale();
            }).step(0.005).name('GridWidth');
        }
    }

    tweenPlanet(planet,position, cb, easing, duration, isScale = false, isMesh = false){

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;
        let param = isScale ? planet.mesh.scale : isMesh ? planet.position : planet.mesh.position;

        new TWEEN.Tween(param)
            .to(new THREE.Vector3(position.x,position.y,position.z),_duration)
            .easing(_easing)
            .onComplete( () => {
                cb && cb();
            })
            .start();
    }
}

export default GalaxyManager;
