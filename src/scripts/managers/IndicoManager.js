class IndicoManager {

    constructor() {
        this.config = {
            key: "8e08cec15e762125f7348ee55d8c01c4",
            baseUrl: "https://apiv2.indico.io/"
        };

        this.isPositive = null;
        this.personnality = null;
        this.emotion = null;
    }

    getPersonnality(text, callback) {
        let data = JSON.stringify({
            'api_key': this.config.key,
            'data': text
        });

        this.url = this.config.baseUrl + 'personality';
        let xhr = this.createHttpRequest(this.url, (response, trueAnswer) => {
            if (trueAnswer) {
                response = JSON.parse(response);
                response = response.results;

                this.personnality = {
                    'openness': Math.round(response.openness * 100),
                    'extraversion': Math.round(response.extraversion * 100),
                    'agreeableness': Math.round(response.agreeableness * 100),
                    'conscientiousness': Math.round(response.conscientiousness * 100)
                }  
            } else {
                this.personnality = response;
            }

            callback(this.personnality);
        });
        xhr.send(data);
    }

    getEmotion(text, callback) {
        let data = JSON.stringify({
            'api_key': this.config.key,
            'data': text
        });

        this.url = this.config.baseUrl + 'emotion';
        let xhr = this.createHttpRequest(this.url, (response) => {
            response = JSON.parse(response);
            response = response.results;
            this.emotion = (response) ? Object.keys(response).reduce(function (a, b) {
                return response[a] > response[b] ? a : b
            }) : this.emotion;
            callback(this.emotion);
        });
        xhr.send(data);
    }

    getHumor(text, callback) {
        let data = JSON.stringify({
            'api_key': this.config.key,
            'data': text
        });

        this.url = this.config.baseUrl + 'sentimenthq';
        let xhr = this.createHttpRequest(this.url, (response) => {
            response = JSON.parse(response);
            response = response.results;
            this.isPositive = (response) ? response > 0.5 : false;
            callback(this.isPositive);
        });

        xhr.send(data);
    }


    createHttpRequest(url, callback) {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        //xhr.setRequestHeader("Content-type", "application/x-www-constellation-urlencoded");

        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                console.info('Prediction Done');
                callback(xhr.response, true);
            } else if (xhr.readyState === XMLHttpRequest.DONE && xhr.status !== 200) {
                console.info('Prediction Error IndicoManager l.86');
                callback({
                    'openness': Math.round(Math.random() * 100),
                    'extraversion': Math.round(Math.random() * 100),
                    'agreeableness': Math.round(Math.random() * 100),
                    'conscientiousness': Math.round(Math.random() * 100)
                }, false);
            }
        };

        return xhr;
    }

}

export default IndicoManager;