import {TweenMax, TimelineMax} from "gsap";
import CONSTANTS from '../datas/Constants';
import Vivus from "vivus";

class FamilyManager {

    constructor(astroSign, family, animationManager, dataManager) {
        this.astroSign = astroSign;
        this.family = family;
        this.animationsManager = animationManager;
        this.dataManager = dataManager;
        this.startAnimationNb = 2;

        this.buildFamily();
    }

    buildFamily() {
        this.displayInText();
        this.loadSign();
    }

    displayInText() {
        document.querySelector('.byo-family-desc-sign').innerHTML = this.astroSign;
        document.querySelector('.byo-family-desc-symbol').innerHTML = this.family;
    }

    loadSign() {
        let xhr = new XMLHttpRequest;
        xhr.open('get', './assets/img/astro/sign/' + this.astroSign + '.svg', true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            this.astroSignSVG = xhr.responseXML.documentElement;
            document.querySelector('.byo-family-sign').appendChild(this.astroSignSVG);

            this.loadFamily();
        };
        xhr.send();
    }

    loadFamily() {
        let xhr = new XMLHttpRequest;
        xhr.open('get', './assets/img/family/' + this.family + '.svg', true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            this.elementSVG = xhr.responseXML.documentElement;
            document.querySelector('.byo-family-element').appendChild(this.elementSVG);

            this.launchFamily();
        };
        xhr.send();
    }

    launchFamily() {
        let index = CONSTANTS.ASTRO.indexOf(this.astroSign);
        let currentConstellation = new Vivus('byo_intro_constellation_' + index, {
            duration: 50
        });
        TweenMax.to(document, 1.2, {
            onComplete: () => {
                currentConstellation.selfDestroy;
                document.getElementById('byo_intro_constellation_' + index).classList.add('byo-intro-constellation-isActive');
                TweenMax.to(document.querySelector('.byo-intro-constellation-isActive'), .4, {opacity: 0.05});
            }
        });

        this.dataManager.initNameManager(this.family);

        this.animationsManager.launchNextAnimation(this.startAnimationNb);
    }

}

export default FamilyManager;