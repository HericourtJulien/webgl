import CONSTANTS from '../datas/Constants';
import {TweenMax, TimelineMax} from "gsap";
import * as THREE from 'three';
import Particle from '../models/Particle';
import Vivus from "vivus";
import Translate from 'translate';
import GalaxyManager from  '../managers/GalaxyManager';

class StoryManager {

    constructor(animationsManager, dataManager, callback) {
        this.animationsManager = animationsManager;
        this.dataManager = dataManager;
        this.callback = callback;
        this.countDownStarted = false;
        this.particles = 0;
        this.isAnimate = false;

        this.bindEvents();
        this.buildStory();
    }

    bindEvents() {
        let storyThreeChoices = document.querySelectorAll('.byo-story-three-choice');

        for (let choice of storyThreeChoices) {
            choice.addEventListener('click', (event) => this.handleLightChoice(event));
        }
        document.querySelector('.byo-story-four-form-submit').addEventListener('click', (event) => this.handleSubmitIndico(event));

        let galaxyFamilyChoices = document.querySelectorAll('.g-style');

        for (let choice of galaxyFamilyChoices) {
            choice.addEventListener('click', (event) => this.handleGalaxyChoice(event));
        }
    }

    buildStory() {
        this.insertUserDatasInStory();
    }

    insertUserDatasInStory() {
        let storyNames = document.querySelectorAll('.byo-story-name');
        for (let name of storyNames) {
            name.innerHTML = this.dataManager.user.name;
        }

        let storyFamilies = document.querySelectorAll('.byo-story-family');
        for (let family of storyFamilies) {
            family.innerHTML = this.dataManager.user.family;
        }

        let storySigns = document.querySelectorAll('.byo-story-sign');
        for (let sign of storySigns) {
            sign.innerHTML = this.dataManager.user.astro;
        }
    }

    handleLightChoice(event) {
        let indexLight = event.srcElement.getAttribute('data-light');
        this.callback('light', indexLight);
        this.callback('lightColor', CONSTANTS.COLORS[this.dataManager.user.family][indexLight]);
        this.animationsManager.launchNextAnimation(7);
    }

    handleSubmitIndico(event) {
        event.preventDefault();

        let input = document.querySelector('.byo-story-four-form-input');
        let inputValue = input.value;

        if (inputValue.replace(/ /g, '') === "") {
            input.placeholder = "Veuillez insérer un réponse valide";
            return;
        }

        Translate(inputValue, {
            engine: 'yandex',
            key: 'trnsl.1.1.20180107T162226Z.551be43632caa276.d54da791349ea9af7c668a8702f5452c6c14c0b0',
            from: 'fr',
            to: 'en'
        }).then((answer) => {
            inputValue = answer;
            this.dataManager.indicoManager.getPersonnality(inputValue, (answer) => {
                this.callback('personality', answer);
                this.animationsManager.launchNextAnimation(8);
            });
        }).catch((error) => {
            console.error('Error GoogleTranslate : ' + error);
        });
    }

    initPlanetCard() {
        document.querySelector('.byo-story-number').innerHTML = window.globalCount || Math.round(Math.random() * 9999);
        document.querySelector('.byo-story-birthdate').innerHTML = this.dataManager.user.birthdate;
        document.querySelector('.byo-story-light').innerHTML = CONSTANTS.LIGHT[parseInt(this.dataManager.user.light)];

        console.log(this.dataManager.user.light);
        console.log(CONSTANTS.LIGHT);

        document.querySelector('.byo-story-heart').innerHTML = parseInt(this.dataManager.user.particles) + ' bpm';
        this.getPercentages();
    }

    getPercentages() {
        let personality = this.dataManager.user.personality;

        let orderPersonality = Object.keys(personality).sort((a, b) => {
            return personality[b] - personality[a];
        });

        let firstPersonality = orderPersonality[0];
        let secondPersonality = orderPersonality[1];

        this.setPercentages(personality[firstPersonality], firstPersonality, true);
        this.setPercentages(personality[secondPersonality], secondPersonality, false);

        document.querySelector('.byo-story-elements').innerHTML = CONSTANTS.SUPPLEMENTS[this.dataManager.user.family][firstPersonality];
    }

    setPercentages(percentage, mind, isFirst) {
        let currentSelector = isFirst ? document.querySelector('.byo-story-eleven-list-item-percentage-isFirst') : document.querySelector('.byo-story-eleven-list-item-percentage-isSecond');

        currentSelector.querySelector('.byo-story-eleven-list-item-percentage-number').innerHTML = percentage + ' %';
        currentSelector.querySelector('.byo-story-eleven-list-item-percentage-mind').innerHTML = CONSTANTS.TRADS[mind];
        currentSelector.querySelector('.byo-story-eleven-list-item-percentage-name').innerHTML = CONSTANTS.SUPPLEMENTS[this.dataManager.user.family][mind];
    }

    initHeartInteraction() {

        this.heartSVG = new Vivus('byo_heart_animation', {}, (element) => {
            if (element.getStatus() === 'end') {
                element.reset();
                element.play();
            }
        });

        document.addEventListener('keydown', (event) => {this.earthBeating(event)});
    }

    earthBeating(event){
        console.log("earthbeat");

        if (event.keyCode === 32 && !this.countDownStarted) {
            event.preventDefault();
            this.startCountDown();
            this.countDownStarted = true;

            let tl = new TimelineMax();
            tl.to(document.querySelector('.byo-story-eight-interaction-chrono'), .2, {scale: 1.1});
            tl.to(document.querySelector('.byo-story-eight-interaction-chrono'), .2, {scale: 1});

            window.audioManager.canBeat = true;
            window.audioManager.playHeartBeat();

        } else if (event.keyCode === 32 && this.countDownStarted && !this.isAnimate) {
            this.isAnimate = true;
            event.preventDefault();
            this.particles++;
            let scale = window.currentPlanet.mesh.scale;
            for (let child of window.currentPlanet.childs) {
                console.log(child instanceof Particle);
                if (child instanceof Particle) {
                    child.setHeartBeats(this.particles);
                }
            }

            window.currentPlanet.tweenPlanet(new THREE.Vector3(
                scale.x * 1.1,
                scale.y * 1.1,
                scale.z * 1.1
                ), () => {
                    window.currentPlanet.tweenPlanet(new THREE.Vector3(
                        scale.x * 0.9,
                        scale.y * 0.9,
                        scale.z * 0.9
                        ), () => {
                            this.isAnimate = false;
                        },
                        null, 250, true)
                },
                null, 250, true);
            window.audioManager.playHeartBeat();
        }
    }

    startCountDown() {
        let target = document.querySelector('.byo-story-eight-interaction-chrono span');
        let i = 10;
        let tl = new TimelineMax();

        this.loopCountDown(target, i, tl);
    }

    loopCountDown(target, i, tl) {
        tl.to(document, 1, {
            onComplete: () => {
                if (i > 0) {
                    i--;
                    target.innerHTML = i;
                    this.loopCountDown(target, i, tl);
                } else {
                    this.countDownStarted = false;
                    window.audioManager.canBeat = false;
                    this.callback('particles', String(this.particles * 6));
                    this.heartSVG.selfDestroy;
                    TweenMax.to(document, 1, {onComplete: () => this.dataManager.animationsManager.launchNextAnimation(12)});

                    document.removeEventListener("keydown", (event) => {this.earthBeating(event)});
                }


            }
        });
    }

    handleGalaxyChoice(event) {
        let family = event.srcElement.parentNode.getAttribute('data-family');

        console.log(family);


        this.loadFamily(family);
    }

    loadFamily(family) {
        let xhr = new XMLHttpRequest;
        xhr.open('get', './assets/img/family/' + family + '.svg', true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            let elementSVG = xhr.responseXML.documentElement;
            document.querySelector('.byo-story-fifteen-family-icon').innerHTML = '';
            document.querySelector('.byo-story-fifteen-family-icon').appendChild(elementSVG);
            document.querySelector('.byo-story-fifteen-family-name').innerHTML = family;

            console.log(elementSVG);
            this.animationsManager.launchNextAnimation(18, family);

        };
        xhr.send();



    }
}

export default StoryManager;