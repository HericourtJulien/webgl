import CONSTANTS from '../datas/Constants';
import {TweenMax} from "gsap";

class NameManager {

    constructor(family, animationsManager, dataManager, callback) {
        this.family = family;
        this.animationsManager = animationsManager;
        this.dataManager = dataManager;
        this.callback = callback;
        this.nextAnimation = 4;

        this.buildNameForm();
        this.bindEvents();
    }

    bindEvents() {
        document.querySelector('.byo-name-form-submit').addEventListener('click', (event) => this.handleFormSubmit(event));
    }

    buildNameForm() {
        document.querySelector('.byo-name-desc').innerHTML = CONSTANTS.DESC[this.family];
        this.loadPreviousFamily();
    }

    loadPreviousFamily() {
        let xhr = new XMLHttpRequest;
        let index = CONSTANTS.FAMILIES.indexOf(this.family);
        let prevFamily;
        if (index === 0) {
            prevFamily = CONSTANTS.FAMILIES[CONSTANTS.FAMILIES.length - 1];
        } else {
            prevFamily = CONSTANTS.FAMILIES[index - 1];
        }
        xhr.open('get','./assets/img/family/' + prevFamily + '.svg',true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            let prevElement = xhr.responseXML.documentElement;
            document.querySelector('.byo-name-families-prev').appendChild(prevElement);

            this.loadCurrentFamily();
        };
        xhr.send();
    }

    loadCurrentFamily() {
        let xhr = new XMLHttpRequest;
        xhr.open('get','./assets/img/family/' + this.family + '.svg',true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            let currentElement = xhr.responseXML.documentElement;

            document.querySelector('.byo-name-families-current').appendChild(currentElement);
            document.querySelector('.byo-story-thirteen-family-icon').appendChild(currentElement.cloneNode(true));
        };
        xhr.send();
    }

    handleFormSubmit(event) {
        event.preventDefault();

        let input = document.querySelector('.byo-name-form-input');
        let inputValue = input.value.replace(/ /g,'');

        if (inputValue === "") {
            input.placeholder = "Veuillez insérer un nom valide";
            return;
        }

        this.name = inputValue;
        this.callback(this.name);

        this.animationsManager.launchNextAnimation(this.nextAnimation);
        this.dataManager.initStoryManager();
    }

}

export default NameManager;