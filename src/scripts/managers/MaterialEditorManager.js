import * as THREE from 'three';

class MaterialEditorManager{
    constructor(params){
        this.datGui = params.datGui || console.error("no datGui reference passed in param constructor for MaterialEditorManager");
        this.scene = params.scene || console.error("no scene passed");

        let geometry = new THREE.BoxGeometry(5, 5, 5);
        let material = new THREE.MeshLambertMaterial({color: 0xe33b3b, emissive: 0x000000});

        this.mesh = new THREE.Mesh( geometry, material );

        this.ambientLight = new THREE.AmbientLight(0xffffff, 1);
        this.ambientLight.position.set(0, 0, 0);

        this.updateDatGui();
    }

    updateDatGui() {

        if(this.datGui){
            let currentMat = this.mesh.material;
            console.log(currentMat);

            //init scene background
            if(this.scene.background === null){
                this.scene.background = new THREE.Color(0x000000);
            }

            let guiConfig = {
                sceneBackground: this.scene.background.getHex(),
                addSkyBox: false,

                scale: this.mesh.scale.x,

                color: '#'+currentMat.color.getHexString(),
                emissive: currentMat.emissive.getHex(),
                transparent: currentMat.transparent,
                opacity: currentMat.opacity,
                wireframeMode: currentMat.wireframe,

                hasAmbientLight: false,
                ambientLightIntensity: this.ambientLight.intensity,
                ambientLightColor: this.ambientLight.color.getHex(),

                hasReflection: false,
                refractionRatio: currentMat.refractionRatio,
                reflectivity: currentMat.reflectivity
            };

            this.datGui.remember(guiConfig);

            /**------------------ SCENE PARAMS ------------------**/

            let sceneParams = this.datGui.addFolder("Scene parameters");

            sceneParams.addColor(guiConfig, 'sceneBackground').onChange((val) => {
                this.scene.background = new THREE.Color(val);
            }).name("background");

            sceneParams.add(guiConfig, 'addSkyBox').onChange((val) => {
                if(val){
                    this.createSkyBox();
                }else{
                    this.scene.remove(this.skybox);
                }
            });


            /**------------------ LIGHT PARAMS ------------------**/
            let lightParams = this.datGui.addFolder("Light parameters");

            lightParams.add(guiConfig, 'hasAmbientLight').onChange((val) => {
                if(val){
                    this.scene.add(this.ambientLight);
                }else{
                    this.scene.remove(this.ambientLight);
                }
            }).name("add ambient light");

            lightParams.add(guiConfig, 'ambientLightIntensity', 0, 10).onChange((val) => {
                this.ambientLight.intensity = val;
            }).step(0.1).name("light intensity");

            lightParams.addColor(guiConfig, 'ambientLightColor').onChange((val) => {
                this.ambientLight.color.setHex(val);
            }).name('light color');


            /**------------------ MESH PARAMS ------------------**/
            let meshParams = this.datGui.addFolder("Mesh parameters");

            meshParams.add(guiConfig, 'scale', 0.05, 5).onChange((val) => {
                this.mesh.scale.x = val;
                this.mesh.scale.y = val;
                this.mesh.scale.z = val;
            }).step(0.01).name('scale');

            meshParams.add(guiConfig, 'wireframeMode').onChange((val) => {
                currentMat.wireframe = val;
            }).name("wireframe");


            /**------------------ MATERIAL PARAMS ------------------**/
            let materialParams = this.datGui.addFolder("Material parameters");

            materialParams.addColor(guiConfig, 'color').onChange((val) => {
                currentMat.color.setHex(val.replace("#", "0x"));
            }).name('color (#)');

            materialParams.addColor(guiConfig, 'emissive').onChange((val) => {
                currentMat.emissive.setHex(val);
            }).name('emissive (#)');

            materialParams.add(guiConfig, 'transparent').onChange((val) => {
                currentMat.transparent = val;
            });

            materialParams.add(guiConfig, 'opacity', 0, 1).onChange((val) => {
                currentMat.opacity = val;
            }).step(0.01).name("opacity");

            materialParams.add(guiConfig, 'hasReflection', 0, 1).onChange((val) => {
                this.createCubeCameraReflection(val);
            }).name("reflection");

            materialParams.add(guiConfig, 'reflectivity', 0, 1).onChange((val) => {
                currentMat.reflectivity = val;
            }).step(0.01).name("reflectivity");

            materialParams.add(guiConfig, 'refractionRatio', 0, 1).onChange((val) => {
                currentMat.refractionRatio = val;
            }).step(0.01).name('refraction ratio');

            //console.log(THREE);

        }else{
            console.error("dat gui not passed in params");
        }
    }

    place(scene){
        scene.add(this.mesh);
        this.scene = scene;
    }

    createSkyBox(){

        let reflectionCube = this.createSkyBoxMap();

        reflectionCube.format = THREE.RGBFormat;

        let shader = THREE.ShaderLib[ "cube" ];
        shader.uniforms[ "tCube" ].value = reflectionCube;

        let material = new THREE.ShaderMaterial( {
            fragmentShader: shader.fragmentShader,
            vertexShader: shader.vertexShader,
            uniforms: shader.uniforms,
            depthWrite: false,
            side: THREE.BackSide
        });

        this.skybox = new THREE.Mesh(new THREE.BoxGeometry(100, 100, 100), material);
        this.scene.add(this.skybox);
    }

    createSkyBoxMap(){
        const path = "../../assets/models/skybox/";
        const format = '.jpg';

        /**
         px (positive x) : right
         nx (negative x) : left
         py (positive y) : top
         ny (negative y) : bottom
         pz (positive z) : back
         nz (negative z) : front
         */
        const urls = [
            'right' + format,
            'left' + format,
            'top' + format,
            'bottom' + format,
            'back' + format,
            'front' + format
        ];

        let reflectionCube = new THREE.CubeTextureLoader().setPath(path).load( urls );

        return reflectionCube;
    }

    createCubeCameraReflection(hasReflection){

        let mirrorCubeMaterial;

        if(hasReflection){
            if(!this.scene){
                console.error("no scene for reflection");
                return;
            }

            this.mirrorCubeCamera = new THREE.CubeCamera( 0.1, 5000, 512 );
            mirrorCubeMaterial = new THREE.MeshBasicMaterial( { envMap: this.mirrorCubeCamera.renderTarget } );

            console.log(this.mesh.material);
            console.log("mirror cube material", mirrorCubeMaterial);

            this.mesh.material = mirrorCubeMaterial;
            this.mirrorCubeCamera.position = this.mesh.position;
            this.scene.add( this.mirrorCubeCamera );
        }else{

        }

    }

    saveMaterial(){

    }

    loadMaterial(JSON){
        this.datGui = new dat.GUI({ load: JSON });
    }

    render(){

    }
}

export default MaterialEditorManager;