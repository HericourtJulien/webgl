import * as THREE from 'three';

import ShadedPlanet from '../models/planet/ShadedPlanet';
import TexturedPlanet from '../models/planet/TexturedPlanet';
import GlowingPlanet from '../models/planet/GlowingPlanet';

import Particle from './../models/Particle';

import CONSTANTS from '../datas/Constants';

import TWEEN from 'tween.js';



class PlanetGenerator{
    constructor(dataManager, isGalaxy = false){
        this.planet = null;
        this.updateCurrentPlanet();

        this.dataManager = dataManager;
        this.family = '';
        this.isGalaxy = isGalaxy;
        this.bidonMaterial = new THREE.MeshPhongMaterial({
            color: 0x11363f,
            transparent: true,
            opacity: 0
        });
        this.tempMaterial = null;


    }

    /**
     * update the current planet in global variable
     */
    updateCurrentPlanet(){
        window.currentPlanet = this.planet;
    }

    /**
     * instantiate the new planet with params
     * @param family
     * @param attributs
     * @returns {null|GlowingPlanet|ShadedPlanet|TexturedPlanet}
     */
    instantiatePlanet(family, attributs){
		

        this.childsMaterial = [];
        this.family = this.correctingFamilyName(family);

        let rand = Math.floor(Math.random() * (4 - 1) + 1);
        let suffix = (this.isGalaxy) ? this.dataManager.user.modelId : String(rand);

        this.dataManager.user.modelId = (this.isGalaxy) ? suffix : String(rand);
        !this.isGalaxy && this.dataManager.firebase.updateUser(this.dataManager.user);

        console.log("Planet Family : " + this.family);
        console.log("Planet Model Id : " + suffix);
        console.log("Attributes : ", attributs);

        switch(this.family){
            case 'air':

                this.planet =  new GlowingPlanet({
                    galaxy: ( attributs && attributs.galaxy) ? attributs.galaxy : '',
                    placeThisPlaneteOnMyGalaxy: ( attributs && attributs.placeThisPlaneteOnMyGalaxy ) ? attributs.placeThisPlaneteOnMyGalaxy : '',
                    onPick: ( attributs &&attributs.onPick ) ? attributs.onPick : '',
                    isPickable: ( attributs && attributs.isPickable) ? attributs.isPickable : '',

                    datGui: window.mainController.datGui,
                    name: 'air'+suffix,
                    id: suffix,
                    planetType:  ( attributs && attributs.planetType) ? attributs.planetType : 'air',
                    doubleSided:  ( attributs && attributs.doubleSided) ? attributs.doubleSided : true,
                    texturedMesh: ( attributs && attributs.texturedMesh) ? attributs.texturedMesh : "clouds",
                    textureMeshScale: ( attributs && attributs.textureMeshScale) ? attributs.textureMeshScale : 1.1,
                    glow: ( attributs && attributs.glow) ? attributs.glow :  0.825,
                    glowPower: ( attributs && attributs.glowPower) ? attributs.glowPower :  1.795,
                    outGlow: ( attributs && attributs.outGlow) ? attributs.outGlow :  0.34,
                    outPow: ( attributs && attributs.outPow) ? attributs.outPow :  2.23,
                    glowingMesh: ( attributs && attributs.glowingMesh) ? attributs.glowingMesh :  "noyau",
                    glowColor: ( attributs && attributs.glowColor) ? attributs.glowColor : "#162ae3",
                    outGlowColor: ( attributs && attributs.outGlowColor) ? attributs.outGlowColor : "#162ae3",
                    hasShader: ( attributs && attributs.hasShader) ? attributs.hasShader : false,
                    meshOpacity: ( attributs && attributs.meshOpacity) ? attributs.meshOpacity : 0.15,
                    texture: ( attributs && attributs.texture) ? attributs.texture : "./assets/models/planet/textures/clouds.png"
                });

                this.setDefault(this.planet.texturedMesh);

                /*this.planet.makeItGlow();
                this.planet.createTexture();*/

                break;

            case 'water':

                this.planet = new ShadedPlanet({

                    galaxy: ( attributs && attributs.galaxy) ? attributs.galaxy : '',
                    placeThisPlaneteOnMyGalaxy: ( attributs && attributs.placeThisPlaneteOnMyGalaxy ) ? attributs.placeThisPlaneteOnMyGalaxy : '',
                    onPick: ( attributs &&attributs.onPick ) ? attributs.onPick : '',
                    isPickable: ( attributs && attributs.isPickable) ? attributs.isPickable : '',

                    datGui: window.mainController.datGui,
                    name: 'water'+suffix,
                    id: suffix,
                    planetType: ( attributs && attributs.planetType) ? attributs.planetType : 'water',
                    applyShaderToMesh: ( attributs && attributs.applyShaderToMesh) ? attributs.applyShaderToMesh : 'ocean',
                    replaceMesh: ( attributs && attributs.replaceMesh) ? attributs.replaceMesh :  true,
                    doubleSided:  ( attributs && attributs.doubleSided) ? attributs.doubleSided : true,
                    shaderOpacity:( attributs && attributs.shaderOpacity) ? attributs.shaderOpacity :  1,
                    uniforms:{
                        time:{
                            type: 'f',
                            value: 0.0
                        },
                        amplifier:{
                            type: 'f',
                            value: 1.68451
                        },
                        maxOpacity:{
                            type: 'f',
                            value: 0.85
                        }
                    },
                    renderUniforms:true,
                    shaderTexture: './shaders/sources/assets/vague3.png'
                });

                this.setDefault(this.planet.applyShaderToMesh);

                break;

            case 'dirt':

                this.planet = new TexturedPlanet({

                    galaxy: ( attributs && attributs.galaxy) ? attributs.galaxy : '',
                    placeThisPlaneteOnMyGalaxy: ( attributs && attributs.placeThisPlaneteOnMyGalaxy ) ? attributs.placeThisPlaneteOnMyGalaxy : '',
                    onPick: ( attributs &&attributs.onPick ) ? attributs.onPick : '',
                    isPickable: ( attributs && attributs.isPickable) ? attributs.isPickable : '',

                    datGui: window.mainController.datGui,
                    name: 'dirt'+suffix,
                    id: suffix,
                    planetType: ( attributs && attributs.planetType) ? attributs.planetType : 'dirt',
                    texturedMesh: ( attributs && attributs.texturedMesh) ? attributs.texturedMesh : "ground",
                    bumpScaleFactor: ( attributs && attributs.bumpScaleFactor) ? attributs.bumpScaleFactor : 0.25,
                    hasShader: ( attributs && attributs.hasShader) ? attributs.hasShader : false,
                    texture: ( attributs && attributs.texture) ? attributs.texture : "./assets/models/planet/textures/dirt_1_texture.png",
                    bumpMap: ( attributs && attributs.bumpMap) ? attributs.bumpMap : "./assets/models/planet/textures/dirt_1_bump.png"
                });

                this.setDefault(this.planet.texturedMesh);

                break;

            case 'fire':

                this.planet = new ShadedPlanet({

                    galaxy: ( attributs && attributs.galaxy) ? attributs.galaxy : '',
                    placeThisPlaneteOnMyGalaxy: ( attributs && attributs.placeThisPlaneteOnMyGalaxy ) ? attributs.placeThisPlaneteOnMyGalaxy : '',
                    onPick: ( attributs &&attributs.onPick ) ? attributs.onPick : '',
                    isPickable: ( attributs && attributs.isPickable) ? attributs.isPickable : '',

                    datGui: window.mainController.datGui,
                    name: 'fire'+suffix,
                    id:suffix,
                    planetType: ( attributs && attributs.planetType) ? attributs.planetType :  'fire',
                    applyShaderToMesh: ( attributs && attributs.applyShaderToMesh) ? attributs.applyShaderToMesh : 'lava',
                    replaceMesh: ( attributs && attributs.replaceMesh) ? attributs.replaceMesh :  true,
                    shaderOpacity:  ( attributs && attributs.shaderOpacity) ? attributs.shaderOpacity : 1,
                    uniforms:{
                        time:{
                            type:'f',
                            value:0.0
                        }
                    },
                    renderUniforms: true,
                    shaderTexture: './shaders/sources/assets/lava.png'
                });

                this.setDefault(this.planet.applyShaderToMesh);

                break;

            default:
                console.error("can't switch family planet");
        }

        this.updateCurrentPlanet();


        if(this.planet.mesh.children && this.planet.mesh.children.length > 0 && !this.isGalaxy){
            this.saveChildsMaterial();
        }

        return this.planet;

    }

    /**
     *  apply default material
     */
    setDefault(mesh){

        let hearBeats = ( this.dataManager.user && this.dataManager.user.particles && this.isGalaxy ) ? this.dataManager.user.particles/6 : 0 ;
        let scale = this.isGalaxy  ? 0.9 : 1.5;


        let particle = new Particle({
            datGui: window.mainController.datGui,
            scale: scale,
             heartBeats: hearBeats
        });

        this.planet.childs.push(particle.getChild());

        this.planet.needsUpdate = true;
        this.planet.mesh.updateMatrix();


        if(!this.isGalaxy){
            this.tempMaterial = this.planet.getMesh(mesh).material;
            this.planet.getMesh(mesh).material = this.bidonMaterial;
            this.planet.getMesh(mesh).material.needsUpdate = true;

            this.planet.mesh.scale.x = 3.5;
            this.planet.mesh.scale.y = 3.5;
            this.planet.mesh.scale.z = 3.5;

            window.mainController.addElement(this.planet);

        }else{
            this.settingMaterials();
        }

        console.log("Add planet to scene");


    }

    /**
     * save materials of each childs
     */
    saveChildsMaterial(){
        for(let child of this.planet.mesh.children){

            if(child.name === "Particles"){
                continue;
            }

            this.childsMaterial.push({
                name: child.name,
                material: child.material
            });

            child.material = this.bidonMaterial;
            child.material.needsUpdate = true;
        }

        console.log("saved materials", this.childsMaterial);
    }

    /**
     * placing light for three choice
     */
    setLightChoice(){

        this.tempLight = new THREE.SpotLight(0xffffff);

        this.tempLight.intensity = 0;

        this.tempLight.shadow.camera.near = 0;
        this.tempLight.shadow.camera.far = 50;
        this.tempLight.shadow.camera.fov = 10;
        this.tempLight.shadow.mapSize.width = 1024;
        this.tempLight.shadow.mapSize.height = 1024;
        this.tempLight.castShadow = true;

        this.tempLight.position.set( -50, 8, 0 );

        let targetObject = new THREE.Object3D();
        targetObject.position.x = this.planet.mesh.position.x;
        targetObject.position.y = this.planet.mesh.position.y;
        targetObject.position.z = this.planet.mesh.position.z;

        this.tempLight.target = targetObject;

        this.planet.mesh.add(this.tempLight);
        this.planet.mesh.needsUpdate = true;

        let coord = { start: this.tempLight.intensity };
        new TWEEN.Tween(coord)
            .to({ start: 1 }, 1500)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate(() => {
                this.tempLight.intensity = coord.start;
            }).start();
    }

    /**
    * TWEEN STOP AMBIANT LIGHT
    */
    stopAmbiantLight(cb, easing, duration){

        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;

        let coord = { start: window.mainController.globalLight.ambientLight.intensity };
        new TWEEN.Tween(coord)
            .to({ start: 0 }, _duration)
            .easing(_easing)
            .onUpdate(() => {
                window.mainController.globalLight.ambientLight.intensity = coord.start;
            })
            .onComplete( () => {
                cb && cb();
            })
            .start();
    }

    /**
     * resetting ambient light default value (to 1)
     * @param cb
     * @param easing
     * @param duration
     */
    lightOff(cb, easing, duration){
        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;

        let switchOff = {spotOff: 1, ambientOff: 1, tempOff: 1};
        new TWEEN.Tween(switchOff)
            .to({spotOff: 0, ambientOff: 0, tempOff: 0})
            .easing(_easing)
            .onUpdate(() => {
                window.mainController.globalLight.spotLight.intensity = switchOff.spotOff;
                window.mainController.globalLight.ambientLight.intensity = switchOff.ambientOff;
                this.tempLight.intensity = switchOff.tempOff;
            })
            .onComplete( () => {
                cb && cb();
            })
            .start();
    }

    /**
     * resetting default light params
     */
    resetDefaultLight(cb, easing, duration){
        let _easing = easing || TWEEN.Easing.Quadratic.Out;
        let _duration = duration || 1000;

        let coord = { spotStart: 0, ambientStart: 0};
        new TWEEN.Tween(coord)
            .to({ spotStart: 1, ambientStart: 0.8}, _duration)
            .easing(_easing)
            .onUpdate(() => {
                window.mainController.globalLight.spotLight.intensity = coord.spotStart;
                window.mainController.globalLight.ambientLight.intensity = coord.ambientStart;
            })
            .onComplete( () => {
                cb && cb();
            })
            .start();
    }

    /**
     * setting materials to the planet following the user choices & mood
     * @param family
     * @param planet -> three mesh instance !
     * @param callback
     */
    settingMaterials(callback = null, family = this.family, planet = this.planet){
        //called after switching lights off or when need to set the galaxy
        let attributs = {};

        //correcting family name (may be different from DB)
        let correctedFamily = this.correctingFamilyName(family);

        //resetting base material
        for(let child of planet.mesh.children){
            if(child.name === "Particles"){
                continue;
            }

            for(let currentMat of this.childsMaterial){
                if(currentMat.name === "Particles"){
                    continue;
                }

                if(currentMat.name === child.name){
                    currentMat.material.color = new THREE.Color(this.dataManager.user.lightColor);
                    child.material = currentMat.material
                }
            }
        }

        let objToApplyShader = null;
        let id = planet.planetId;

        switch(correctedFamily){
            case 'air':
                if(!this.isGalaxy){
                    if(id === "1"){
                        planet.glowColor = "#162ae3";
                        planet.outGlowColor = "#162ae3";
                    }else if(id === "2"){
                        planet.glowColor = "#e4c1e8";
                        planet.outGlowColor = "#904de5";
                    }else{
                        planet.glowColor = "#dbcfea";
                        planet.outGlowColor = "#aa65b2";
                    }
                }

                if(planet.texture && planet.texturedMesh){
                    //always same texture
                    planet.createTexture();
                }

                planet.makeItGlow();

                attributs = {
                    name: planet.name,
                    planetId: planet.planetId,
                    planetType:  planet.planetType,
                    doubleSided:  planet.doubleSided,
                    texturedMesh: planet.texturedMesh,
                    textureMeshScale: planet.textureMeshScale,
                    glow: planet.glow,
                    glowPower: planet.glowPower,
                    outGlow: planet.outGlow,
                    outPow: planet.outPow,
                    // glowingMesh: planet.glowingMesh,
                    glowColor: planet.glowColor,
                    outGlowColor: planet.outGlowColor,
                    hasShader: planet.hasShader,
                    meshOpacity: planet.meshOpacity,
                    texture: planet.texture
                };

                break;
            case 'water':
                if(!this.isGalaxy) {
                    if (id === "1") {
                        planet.uniforms = {
                            time: {
                                type: 'f',
                                value: 0.0
                            },
                            amplifier: {
                                type: 'f',
                                value: 1.68451
                            },
                            maxOpacity: {
                                type: 'f',
                                value: 0.85
                            }
                        };

                        planet.shaderTexture = './shaders/sources/assets/vague3.png';
                    } else if (id === "2") {
                        planet.uniforms = {
                            time: {
                                type: 'f',
                                value: 0.0
                            },
                            amplifier: {
                                type: 'f',
                                value: 1.986453
                            },
                            maxOpacity: {
                                type: 'f',
                                value: 0.97
                            }
                        };

                        planet.shaderTexture = './shaders/sources/assets/vague2.png';
                    } else {
                        planet.uniforms = {
                            time: {
                                type: 'f',
                                value: 0.0
                            },
                            amplifier: {
                                type: 'f',
                                value: 1.986453
                            },
                            maxOpacity: {
                                type: 'f',
                                value: 0.85
                            }
                        };

                        planet.shaderTexture = './shaders/sources/assets/vague3.png';
                    }
                }

                attributs = {
                    name: planet.name,
                    planetId: planet.planetId,
                    planetType:  planet.planetType,
                    applyShaderToMesh:  planet.applyShaderToMesh,
                    replaceMesh: planet.replaceMesh,
                    doubleSided: planet.doubleSided,
                    shaderOpacity: planet.shaderOpacity,
                    shaderTexture: planet.shaderTexture,
                    renderUniforms: planet.renderUniforms,
                };

                objToApplyShader = planet.getMesh(planet.applyShaderToMesh);
                planet.addShader(objToApplyShader);

                break;

            case 'fire':
                if(!this.isGalaxy) {

                    if (id === "1") {
                        planet.uniforms = {
                            time: {
                                type: 'f',
                                value: 0.0
                            }
                        };

                        planet.shaderTexture = './shaders/sources/assets/lava.png';
                    } else if (id === "2") {
                        planet.replaceMesh = false;
                        planet.uniforms = {
                            time: {
                                type: 'f',
                                value: 0.0
                            }
                        };

                        planet.shaderTexture = './shaders/sources/assets/explosion.png';
                    } else {
                        planet.uniforms = {
                            time: {
                                type: 'f',
                                value: 0.0
                            }
                        };

                        planet.shaderTexture = './shaders/sources/assets/explosion.png';
                    }
                }

                attributs = {
                    name: planet.name,
                    planetId: planet.planetId,
                    planetType:  planet.planetType,
                    applyShaderToMesh:  planet.applyShaderToMesh,
                    replaceMesh: planet.replaceMesh,
                    shaderOpacity: planet.shaderOpacity,
                    shaderTexture: planet.shaderTexture,
                    renderUniforms: planet.renderUniforms,
                };


                objToApplyShader = planet.getMesh(planet.applyShaderToMesh);
                planet.addShader(objToApplyShader);



                break;
            case 'dirt':

                if(!this.isGalaxy) {
                    if (id === "1") {
                        planet.bumpScaleFactor = 0.25;
                        planet.hasShader = false;
                        planet.texture = "./assets/models/planet/textures/dirt_1_texture.png";
                        planet.bumpMap = "./assets/models/planet/textures/dirt_1_bump.png";
                    } else if (id === "2") {
                        planet.doubleSided = true;
                        planet.bumpScaleFactor = 0.35;
                        planet.hasShader = false;
                        planet.texture = "./assets/models/planet/textures/dirt_2_texture.png";
                        planet.bumpMap = "./assets/models/planet/textures/dirt_2_bump.png";
                    } else {
                        planet.doubleSided = true;
                        planet.bumpScaleFactor = 0.1;
                        planet.hasShader = false;
                        planet.texture = "./assets/models/planet/textures/dirt_3_texture.png";
                        planet.bumpMap = "./assets/models/planet/textures/dirt_3_bump.png";
                    }
                }

                planet.createTexture();

                attributs = {
                    name: planet.name,
                    planetId: planet.planetId,
                    planetType:  planet.planetType,
                    texturedMesh:  planet.texturedMesh,
                    bumpScaleFactor: planet.bumpScaleFactor,
                    hasShader: planet.hasShader,
                    texture: planet.texture,
                    bumpMap: planet.bumpMap,
                };

                break;
            default:
                console.error("can't reapply main material");
        }

        this.dataManager.user.attributs = attributs;
        this.dataManager.firebase.updateUser(this.dataManager.user);

        callback && callback();

        if(this.isGalaxy){
            this.addMoodElement();
        }
    }

    /**
     * resetting default family type |can be different in the BDD & in the code
     * @param familyName
     * @returns {*}
     */
    correctingFamilyName(familyName){
        switch(familyName){
            case 'eau':
                familyName = 'water';
                break;
            case 'terre':
                familyName = 'dirt';
                break;
            case 'feu':
                familyName = "fire";
                break;
            case 'wind':
                familyName = 'air';
                break;
        }

        return familyName;
    }

    /**
     * set the spline for the light choice
     * @param cb
     */
    setSpline(cb = null){
        /**
         * catmull test -> not working because can't setPoints ... paste from the official doc O.O
         */
        /*const circleGeometry = new THREE.CircleBufferGeometry( 5, 58 );
        let circlePoints = [];

        let i = 1;
        for(i; i < circleGeometry.vertices.length; i++){
            circlePoints.push(circleGeometry.vertices[i]);
        }

        let catmull = new THREE.CatmullRomCurve3(circlePoints);
        catmull.closed = true;
        console.log(circlePoints);
        console.log(catmull);
        alert("catmuled");

        const numberOfPoints = circlePoints.length; // Pour plus ou moins lisser la spline visible
        const smoothedPoints = catmull.getPoints(numberOfPoints);

        const geometry = new THREE.BufferGeometry().setFromPoints(smoothedPoints);
        const material = new THREE.LineBasicMaterial({ color: 0xcacaca });

        console.log("catmull", catmull);
        console.log("points", points);

        let line = new THREE.Line( geometry, material );*/

        let color = 0xCACACA;

        let circleGeometry = new THREE.CircleGeometry( 5, 58 );
        let geometry = new THREE.Geometry();

        let i = 1;
        for(i; i < circleGeometry.vertices.length; i++){
            geometry.vertices.push(circleGeometry.vertices[i]);
        }
        geometry.vertices.push(circleGeometry.vertices[1]);//complete the circle

        let splineMaterial = new THREE.LineBasicMaterial({
            color: color
        });

        let line = new THREE.Line( geometry, splineMaterial );
        line.scale.x = 0.3;
        line.scale.y = 0.3;
        line.scale.z = 0.3;
        line.rotation.y = (90 * Math.PI)/180;
        line.rotation.x = (137 * Math.PI)/180;
        line.name = "line1";

        let line2 = line.clone();
        line2.name = "line2";
        let line3 = line.clone();
        line3.name = "line3";

        //create little sphere
        let sphereGeo = new THREE.SphereBufferGeometry(0.04, 32, 32);
        let sphereMat = new THREE.MeshBasicMaterial({color:color});
        let sphere = new THREE.Mesh(sphereGeo, sphereMat);
        sphere.position.x = 5;

        let sphere2 = sphere.clone();
        sphere2.position.x = -5;
        let sphere3 = sphere.clone();

        line.add(sphere);
        line2.add(sphere2);
        line3.add(sphere3);

        window.mainController.elementsPickables.push(line, line2, line3);

        //line2.rotation.x = (90 * Math.PI)/180;
        line2.rotation.y = (-23 * Math.PI)/180;

        //line3.rotation.x = (90 * Math.PI)/180;
        line3.rotation.y = (23 * Math.PI)/180;

        this.planet.mesh.add(line, line2, line3);
        cb && cb();

        if(!this.isGalaxy){
            this.updateCurrentPlanet();
        }
    }

    /**
     * remove the spline
     */
    removeSpline(){
        this.updateCurrentPlanet();

        let line1 = this.planet.getMesh("line1");
        this.planet.mesh.remove(line1);

        let line2 = this.planet.getMesh("line2");
        this.planet.mesh.remove(line2);

        let line3 = this.planet.getMesh("line3");
        this.planet.mesh.remove(line3);
    }

    /**
     * add element on the planet
     * @param user
     */
    addMoodElement(user = this.dataManager.user){

        let personality = user.personality;

        let orderPersonality = Object.keys(personality).sort((a, b) => {
            return personality[b] - personality[a];
        });

        let firstPersonality = orderPersonality[0];
        let secondPersonnality = orderPersonality[1];

        let moodEl = CONSTANTS.SUPPLEMENTS[user.family][firstPersonality];
        let moodEl2 = CONSTANTS.SUPPLEMENTS[user.family][secondPersonnality];

        let correctedFamily = this.correctingFamilyName(this.family);
        let elementToAdd;
        let elementToAdd2;

        switch(correctedFamily){
            case 'air':

                if(moodEl === "agreeableness" || moodEl === "conscientiousness"){

                    elementToAdd = window.JSONMODELS["nuages"];
                    elementToAdd2 = window.JSONMODELS["tornade"];

                }else{
                    elementToAdd = window.JSONMODELS["tornade"];
                    elementToAdd2 = window.JSONMODELS["nuages"];
                }

                break;

            case 'dirt':

                if(moodEl === "agreeableness" || moodEl === "conscientiousness"){
                    elementToAdd = window.JSONMODELS["montagne"];
                    elementToAdd2 = window.JSONMODELS["cratere"];
                }else{
                    elementToAdd = window.JSONMODELS["cratere"];
                    elementToAdd2 = window.JSONMODELS["montagne"];
                }

                break;

            case'water':

                if(moodEl === "agreeableness" || moodEl === "conscientiousness"){
                    elementToAdd = window.JSONMODELS["lac"];
                    elementToAdd2 = window.JSONMODELS["flaque"];
                }else{
                    elementToAdd = window.JSONMODELS["flaque"];
                    elementToAdd2 = window.JSONMODELS["lac"];
                }

                break;

            case'fire':

                if(moodEl === "agreeableness" || moodEl === "conscientiousness"){
                    elementToAdd = window.JSONMODELS["rocher"];
                    elementToAdd2 = window.JSONMODELS["volcan"];
                }else{
                    elementToAdd = window.JSONMODELS["volcan"];
                    elementToAdd2 = window.JSONMODELS["rocher"];
                }

                break;
        }

        this.addSome(elementToAdd);

        this.planet.mesh.add(elementToAdd2);

        this.updateCurrentPlanet();
    }

    /**
     * adding elements on the planet
     * @param el
     */
    addSome(el){

        let rand;
        let elements = this.dataManager.user.attributs.elements;

        if(elements && elements.length > 0){
            rand = elements.length;
        }else{

            this.dataManager.user.attributs.elements = [];

            rand = Math.floor(Math.random() * (4 - 1) + 1);
        }

        let copy;
        let i = 0;

        for(i; i <= rand; i++){
            copy = el.clone();
            copy.rotation.x = elements ? elements[i].x : Math.floor(Math.random() * (360 - 1) +1);
            copy.rotation.y = elements ? elements[i].y : Math.floor(Math.random() * (360 - 1) +1);
            copy.rotation.z = elements ? elements[i].z : Math.floor(Math.random() * (360 - 1) +1);

            this.planet.mesh.add(copy);

            if(!elements){
                this.dataManager.user.attributs.elements.push({
                    x: copy.rotation.x,
                    y: copy.rotation.y,
                    z: copy.rotation.z
                });
            }

            this.updateCurrentPlanet();
        }

        if(!elements){
            this.dataManager.firebase.updateUser(this.dataManager.user);
        }

        this.updateCurrentPlanet();
    }
}

export default PlanetGenerator;