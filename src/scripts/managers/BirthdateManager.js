import CONSTANTS from "../datas/Constants";
import Draggable from "gsap/Draggable";
import {getAstroSign} from "../datas/Helpers";

class BirthdateManager {
    constructor(animationManager, dataManager) {
        this.animationsManager = animationManager;
        this.dataManager = dataManager;
        this._bindEvents();
    }

    _bindEvents() {
        document.querySelector('.byo-birthdate-start').addEventListener('click', () => this.handleSubmit())
    }

    buildRings(dataNumber) {
        this.buildRingDays();
        this.buildRingMonths();
        this.buildRingYears();

        this.animationsManager.launchNextAnimation(dataNumber);
    }

    buildRingDays() {
        let srcElement = document.querySelector('.byo-birthdate-clock-isDay');

        let textPrev = srcElement.querySelector('.byo-birthdate-clock-text-isPrev');
        let textCurrent = srcElement.querySelector('.byo-birthdate-clock-text-isCurrent');
        let textNext = srcElement.querySelector('.byo-birthdate-clock-text-isNext');

        let textValue;

        let tickRound = srcElement.querySelector('.byo-birthdate-clock-tick-round');
        let tickRoundActiveClass = 'byo-birthdate-clock-tick-round-isHover';
        let snapAngles = 90 / 40;

        Draggable.create(".byo-birthdate-clock-tick-isDay", {
            type:"rotation",
            throwProps:true,
            bounds: {
                minRotation: -45,
                maxRotation: 45
            },
            liveSnap:{
                rotation: (value) => {
                    return Math.round(value / snapAngles) * snapAngles;
                }
            },
            onDragStart: () => {
                tickRound.classList.add(tickRoundActiveClass);
            },
            onDrag:(elm) => {
                textValue = parseInt(textCurrent.innerHTML);

                if (elm.movementY > 0 && textValue > 1) {
                    this.updateValuePosition(srcElement, 'down');
                } else if (elm.movementY < 0 && textValue < 31) {
                    this.updateValuePosition(srcElement, 'up');
                }

                textPrev = srcElement.querySelector('.byo-birthdate-clock-text-isPrev');
                textCurrent = srcElement.querySelector('.byo-birthdate-clock-text-isCurrent');
                textNext = srcElement.querySelector('.byo-birthdate-clock-text-isNext');

                textValue = parseInt(textCurrent.innerHTML);

                textPrev.innerHTML = textValue - 1;
                textNext.innerHTML = textValue + 1;
            },
            onDragEnd: () => {
                tickRound.classList.remove(tickRoundActiveClass);
            }
        });
    }

    buildRingMonths() {
        let srcElement = document.querySelector('.byo-birthdate-clock-isMonth');

        let textPrev = srcElement.querySelector('.byo-birthdate-clock-text-isPrev');
        let textCurrent = srcElement.querySelector('.byo-birthdate-clock-text-isCurrent');
        let textNext = srcElement.querySelector('.byo-birthdate-clock-text-isNext');

        let textValue;

        let tickRound = srcElement.querySelector('.byo-birthdate-clock-tick-round');
        let tickRoundActiveClass = 'byo-birthdate-clock-tick-round-isHover';
        let snapAngles = 90 / 14;

        Draggable.create(".byo-birthdate-clock-tick-isMonth", {
            type:"rotation",
            throwProps:true,
            bounds: {
                minRotation: -45,
                maxRotation: 45
            },
            liveSnap:{
                rotation: (value) => {
                    return Math.round(value / snapAngles) * snapAngles;
                }
            },
            onDragStart: () => {
                tickRound.classList.add(tickRoundActiveClass);
            },
            onDrag:(elm) => {
                textValue = parseInt(textCurrent.getAttribute('data-month'));

                if (elm.movementY > 0 && textValue > 0) {
                    this.updateValuePosition(srcElement, 'down');
                } else if (elm.movementY < 0 && textValue < CONSTANTS.TIME.MONTHS.length - 1) {
                    this.updateValuePosition(srcElement, 'up');
                }

                textPrev = srcElement.querySelector('.byo-birthdate-clock-text-isPrev');
                textCurrent = srcElement.querySelector('.byo-birthdate-clock-text-isCurrent');
                textNext = srcElement.querySelector('.byo-birthdate-clock-text-isNext');

                textValue = parseInt(textCurrent.getAttribute('data-month'));

                textPrev.innerHTML = CONSTANTS.TIME.MONTHS[textValue - 1];
                textPrev.setAttribute('data-month', textValue - 1);

                textCurrent.setAttribute('data-month', textValue);

                textNext.innerHTML = CONSTANTS.TIME.MONTHS[textValue + 1];
                textNext.setAttribute('data-month', textValue + 1);
            },
            onDragEnd: () => {
                tickRound.classList.remove(tickRoundActiveClass);
            }
        });
    }

    buildRingYears() {
        let srcElement = document.querySelector('.byo-birthdate-clock-isYear');

        let textPrev = srcElement.querySelector('.byo-birthdate-clock-text-isPrev');
        let textCurrent = srcElement.querySelector('.byo-birthdate-clock-text-isCurrent');
        let textNext = srcElement.querySelector('.byo-birthdate-clock-text-isNext');

        let textValue;

        let tickRound = srcElement.querySelector('.byo-birthdate-clock-tick-round');
        let tickRoundActiveClass = 'byo-birthdate-clock-tick-round-isHover';
        let snapAngles = parseFloat(90 / (2010 - 1940)).toFixed(2);

        Draggable.create(".byo-birthdate-clock-tick-isYear", {
            type:"rotation",
            throwProps:true,
            bounds: {
                minRotation: -45,
                maxRotation: 45
            },
            liveSnap:{
                rotation: (value) => {
                    return Math.round(value / snapAngles) * snapAngles;
                }
            },
            onDragStart: () => {
                tickRound.classList.add(tickRoundActiveClass);
            },
            onDrag:(elm) => {
                textValue = parseInt(textCurrent.innerHTML);

                if (elm.movementY > 0 && textValue > 1941) {
                    this.updateValuePosition(srcElement, 'down');
                } else if (elm.movementY < 0 && textValue < 2010) {
                    this.updateValuePosition(srcElement, 'up');
                }

                textPrev = srcElement.querySelector('.byo-birthdate-clock-text-isPrev');
                textCurrent = srcElement.querySelector('.byo-birthdate-clock-text-isCurrent');
                textNext = srcElement.querySelector('.byo-birthdate-clock-text-isNext');

                textValue = parseInt(textCurrent.innerHTML);

                textPrev.innerHTML = textValue - 1;
                textNext.innerHTML = textValue + 1;
            },
            onDragEnd: () => {
                tickRound.classList.remove(tickRoundActiveClass);
            }
        });
    }

    updateValuePosition(srcElement, direction) {
        let prevName = 'byo-birthdate-clock-text-isPrev';
        let currentName = 'byo-birthdate-clock-text-isCurrent';
        let nextName = 'byo-birthdate-clock-text-isNext';

        let textPrev = srcElement.querySelector('.' + prevName);
        let textCurrent = srcElement.querySelector('.' + currentName);
        let textNext = srcElement.querySelector('.' + nextName);

        if (direction === 'up') {
            textNext.classList.add(currentName);
            textNext.classList.remove(nextName);

            textCurrent.classList.add(prevName);
            textCurrent.classList.remove(currentName);

            textPrev.classList.add(nextName);
            textPrev.classList.remove(prevName);
        } else {
            textPrev.classList.add(currentName);
            textPrev.classList.remove(prevName);

            textCurrent.classList.add(nextName);
            textCurrent.classList.remove(currentName);

            textNext.classList.add(prevName);
            textNext.classList.remove(nextName);
        }
    }

    handleSubmit() {
        let birthDay = parseInt(document.querySelector('.byo-birthdate-clock-text-isDay.byo-birthdate-clock-text-isCurrent').innerHTML);
        let birthMonth = parseInt(document.querySelector('.byo-birthdate-clock-text-isMonth.byo-birthdate-clock-text-isCurrent').getAttribute('data-month')) + 1;
        let birthYear = parseInt(document.querySelector('.byo-birthdate-clock-text-isYear.byo-birthdate-clock-text-isCurrent').innerHTML);

        let birthDate = birthDay + '/' + birthMonth + '/' + birthYear;
        let astroSign = getAstroSign(birthDay, birthMonth);

        this.dataManager.initFirebase(birthDate, astroSign);
    }
}

export default BirthdateManager;