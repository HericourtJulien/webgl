//#ifdef GL_ES
//precision mediump float;
//#endif
//
//uniform float u_time;
//uniform vec2 u_resolution;
//
//// GLSL doesn't have a random function.  So we have to create one ourself.
//// We would like a random number somewhere between 0.0 and 1.0.
//// We can get that by using the fract function which returns the fractional
//// component of a number.  The constant numbers in this function don't matter
//// too much.  They just need to be random.
//float hash( vec2 p )
//{
//    return fract( sin( dot(p, vec2( 15.79, 81.93  ) ) * 45678.9123 ) );
//}
//
//// Our valueNoise function will bilinearly interpolate a lattice (aka grid)
//// and return a smoothed value. This function will essentially allow us to generate
//// 2D static.  Bilinear interpolation basically allows us to transform our 1D hash function to a value based on a 2D grid.
//// This will eventually be run through an fbm to help us generate a
//// cloud like pattern.
//// For more information about bilinear filtering, check out Scratch A Pixel's article.
//// http://www.scratchapixel.com/old/lessons/3d-advanced-lessons/interpolation/bilinear-interpolation/
//// For more info on Value based noise check this url out
//// http://www.scratchapixel.com/old/lessons/3d-advanced-lessons/noise-part-1/creating-a-simple-2d-noise/
//float valueNoise( vec2 p )
//{
//    // i is an integer that allow us to move along grid points.
//    vec2 i = floor( p );
//    // f will be used as an offset between the grid points.
//    vec2 f = fract( p );
//
//    // Hermite Curve.
//    // The formula 3f^2 - 2f^3 generates an S curve between 0.0 and 1.0.
//    // If we factor out the variable f, we get f*f*(3.0 - 2.0*f)
//    // This allows us to smoothly interpolate along an s curve between our grid points.
//    // To see the S curve graph, go to the following url.
//    // https://www.desmos.com/calculator/mnrgw3yias
//    f = f*f*(3.0 - 2.0*f);
//
//    // Interpolate the along the bottom of our grid.
//    float bottomOfGrid =    mix( hash( i + vec2( 0.0, 0.0 ) ), hash( i + vec2( 1.0, 0.0 ) ), f.x );
//    // Interpolate the along the top of our grid.
//    float topOfGrid =       mix( hash( i + vec2( 0.0, 1.0 ) ), hash( i + vec2( 1.0, 1.0 ) ), f.x );
//
//    // We have now interpolated the horizontal top and bottom grid lines.
//    // We will now interpolate the vertical line between those 2 horizontal points
//    // to get our final value for noise.
//    float t = mix( bottomOfGrid, topOfGrid, f.y );
//
//    return t;
//}
//
//// fbm stands for "Fractional Brownian Motion".
//// Essentially this function calls our valueNoise function multiple
//// times and adds up the results.  By adding various frequences of noise
//// at different amplitudes, we can generate a simple cloud like pattern.
//float fbm( vec2 uv )
//{
//    float sum = 0.00;
//    float amp = 1.0;
//
//    for( int i = 0; i < 2; i++ )
//    {
//        sum += valueNoise( uv ) * amp;
//        uv += uv * 1.2 + cos(u_time) * 0.8;
//        amp *= 0.5;
//    }
//
//    return sum;
//}
//
//// This is where everything starts!
//void main( void )
//{
//
//    // gl_FragCoord.xy is the coordinate of the current pixel being rendered.
//    // It is in screen space.  For example if you resolution is 800x600, gl_FragCoord.xy
//    // could be (300,400).  By dividing the fragcoord by the resolution, we get normalized
//    // coordinates between 0.0 and 1.0.  I would like to work in a -1.0 to 1.0 space
//    // so I multiply the result by 2.0 and subtract 1.0 from it.
//    // if (gl_FragCoord.xy / resolution.xy) equals 0.0, then 0.0 * 2.0 - 1.0 = -1.0
//    // if (gl_FragCoord.xy / resolution.xy) equals 1.0, then 1.0 * 2.0 - 1.0 =  1.0
//    vec2 uv = ( gl_FragCoord.xy / u_resolution.xy ) * 2.0 - 1.0;
//
//    // I am assuming you have more pixels horizontally than vertically so I am multiplying
//    // the x coordinate by the aspect ratio.  This means that the magnitude of x coordinate will probably
//    // be larger than 1.0.  This allows our image to not look squashed.
//    uv.x *= u_resolution.x / u_resolution.y;
//
//    // Pass in a coordinate and get a cloud based scalar value back.
//    // We will use this value to generate a gray-scale based color to display our cloud.
//    float t = fbm( uv * 2.0 );
//
//    vec3 finalColor = vec3( t, t, t );
//
//    // gris : 7d8f99
//    // bleu : 092A3b
//
//    // If you want to add color to your cloud, multiply each t red/green/blue component by some weight.
//    // You can uncomment the following line to see and example.
//    finalColor = vec3( t * 0.03, t * 0.16, t * 0.23 ) * .6;
//
//    // And voila!  We are done!  We should now have a cloud!  =D
//    // gl_FragColor is the final color we want to render for whatever pixel we are currently rendering.
//    gl_FragColor = vec4( finalColor, 1.0 );
//
//
//}




#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

// 2D Random
float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))
                 * 43758.5453123);
}

// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    // Smooth Interpolation

    // Cubic Hermine Curve.  Same as SmoothStep()
    vec2 u = f*f*(3.0-2.0*f);
    u = smoothstep(0.,1.,f);

    // Mix 4 coorners porcentages
    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}


void main() {
    vec3 color = vec3(0.0);
    float n = 0.0;

    vec2 st = gl_FragCoord.xy/u_resolution.xy;

    // Scale the coordinate system to see
    // some noise in action
    vec2 pos = vec2(st* 10.0);

    vec2 vel = vec2(u_time * 0.5);

    // Use the noise function
    n += noise(vec2(pos + vel))*.25+.25;

    // Add a random position
    float a = 0.0;
    n += noise(pos+vel);

    // Add a random position
    //a = noise(pos * vec2(cos(u_time * 0.15), sin(u_time * 0.1)) * 0.1) * 3.1415;
    a = noise(pos * vec2(cos(u_time * 0.15), sin(u_time * 0.1)) * 0.1) * 3.1415;

    vel = vec2(cos(a), sin(a));

    n += noise(pos + vel) ;

    color = vec3( n );

    //color = vec3(smoothstep(0.0, 0.3, fract(n)));
    //color = vec3(step(0.75, fract(n)));


    //gl_FragColor = vec4( n * 0.03, n * 0.16, n * 0.23 , 1.0) * 10.7;
    gl_FragColor = vec4(vec3(color.r * 0.03, color.g * 0.16, color.b * 0.23), 1.0) * 0.5;
}

