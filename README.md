# Build Your Own (BYO) :: _Gobelins Project_

## Presentation
WebGL system based on user's datas to create a custom planet, in a giant galaxy.
The user can see the evolution of the planete with each data it gives to it.
It's an immersive experience that let the user having something representing himself, based on emotions and feelings 
at a precise moment.

## Installation
 1. ` npm install `
 2. ` gulp `

## Commands
* Default command : ` gulp `
* Launch local web server : ` gulp browser-sync `
* Compile ES6 JS files : ` gulp babel `
* Compile shaders : ` gulp shader `
* Copy shaders sources : ` gulp shader-source `

## Authors
* GUZAL Julie
* MESA--GOUIFFES Lucas
* H�RICOURT Julien
* PADERNOZ Benjamin  

## Introduction 

BYO is an application that collect some of our personnal datas with original interactions and interprets them with a 3D world.

BYO use two contexts : 
 * 2D with the main navigation and storyTelling, interactions and animations
 * 3D with some interactions, create planets, animations.
 
The background is a skybox.
We always can move in the skybox to see our planet in all details.

## Compilations

###Gulp - gulpfile.js

The application is divided into two main folders : `src` & `dist`
The main code is on the `src` with ES6 syntax and is then compiled & minified thanks to GULP & BABEL into `dist` folder.

###Shaders GL/SL

Shaders (fragment & vertex) can be created in the `/src/shaders` folder with the `.glsl` extension (Ex: `shaderFirePlaner.frag.glsl`, `shaderFirePlaner.vert.glsl` ) and will be automatically compiled in JS thanks to the `gulp-glsl` plugin and can be imported in ES6

### CSS
`CSS` files are in `src/assets/scss` and compiled into `dist/assets/scss`

**If you use scss files in other folders, they will not be compiled.** 


##Loaders

For a better experience, all ressources are load when the user start the website
and stored in a global variable : `window.MODELS`. Each mesh wich need to get a ressource use this variables.

There are different loaders for each of the resources needed (`src/scripts/loaders`)

Models
* COLLADALoader
* JSONLoader
* OJMTLLoader
* AssetsToLoad

Shaders
* ShadersToLoad


##Controllers


###AppController

The `AppController` ( `./src/scripts/controllers/AppController.js`) is the main file of the application that groups the logic of the code
All scenes, renderer, skybox,  models, managers, etc... are created in this file.


##Managers

###GalaxieManager

`Galaxiemanager` create & manage Galaxies
It can be instanciable with a new `GalaxyManager` everywhere and create galaxy with the `getNewFamily()` function. 

For this function, we have to define its family and the manager will generate from the DB the different planets belonging to this family.
 
GalaxyManager use the `PlanetGenerator` class to create all planets with its function `instantiatePlanet`.

Galaxies can be displayed in different forms: grid (default), shere, spiral.
There is a different function to call depending on the form to be displayed (`this.spirale()`, `this.sphere`).

When you want to destroy your Galaxy, use the `destroyGalaxy()` function. This will remove all planets of the galaxy from the scene.

###PlanetGenerator

The `PlanetGenerator` is the main class to create planets and start when `instantiatePlanet` function is call.

###AnimationsManager

The `AnimationsManager` could also be called `StepManager` and manage all the animations and transitions between steps to navigate through the story.

###FirebaseManager

The `FirebaseManager` is the main class to manage the noSQL Database working with Firebase that allow us to keep datas from the constructed planet and use it to rebuild it in the solar system.
            
###IndicoManager

`IndicoManager` class use Indico.io API and get informations for an user : 

 * Humor ( with `getHumor()` function)
 * Personnality ( with `getPersonnality()` function)
 * Emotion ( with `getEmotion()` function)
 * Humor ( with `getHumor()` function)
 
 ##Models
 
 ###Particles
 
 Particles are meshs to apply as child of planet ( see **Utilities** part ).
 They use texture(.png) and shader.
 
 The particles gravitate around planets.
 
 In its constructor, we can define datGui (optional), color (optional), scale (optional) and heartBeats (optional)
 HeartBeats is a number of heartbeats for 10 seconds.
 
 We can modify dynamically the number of particles with the `setHeartBeats(number)` function.

 ###Planets
 
 Main Planet class define the different properties of a planet and let childs class use different methods to access their mesh.
 It also have methods to render the planet and animate it.
 
 The different childs class define how the mesh are loaded, and waht kind of process they need to pass through to be visible and to apply shader, texture, or other elements.
 They let planet have uniforms to use custom shaders and render the uniforms to modify the planet aspect in real time.

 The Shader used are load an modified following the user interactions in the global experience, and most of them use Samplers2D to load texture and apply them following the projection matrix. 
 
 ###GlowingPlanet
 
 Planet who needs a Glowing shader based on its parameters
 
 ###ShadedPlanet
 
 Planet who needs a Shader based on its parameters.
 
 ###TexturedPlanet
 
 Planet who needs a Texture on its surface
 
 ## Utilities
 
 ### Add child to a mesh
 
  1. Make sure the class you want to add a mesh child has a ` this.childs ` propertie
  2. Copy the function in the target class
 ```
     addChilds(){
         for(let child of this.childs) {
             this.mesh.add(child.mesh);
         }
     }
 ```
  3. Create the child you want to add, and push in the "parent" class the child in its ` this.childs ` propertie.
 
  4. Once your parent class mesh is loaded, call the addChild method.
 
 #### COMPLETE SAMPLE (adding a child):
 
 In the Parent class :
 ```
 class Parent{
     
     constructor(params){
         ...
         this.childs = [];
         ...
         
         let parentObject = {
             ...
             callback: this.loaded.bind(this)
         };
 
         new COLLADALoader(parentObject);
     }
     
      addChilds(){
         for(let child of this.childs) {
             this.mesh.add(child.mesh);
         }
     }
     
     loaded(object){
         ...
         this.addChilds(); //to be sure it is called only when parent mesh is loaded
     }
 
 }
 ```
 
 In the AppController :
 ```
     let parent = new Parent({
         ...
     });
 
     let child = new Child().getChild(); //return the cube and all its properties and methods (including render)
     parent.childs.push(child);
 ```
 
 In the Child class :
 ```
 
 class Child{
 
     constructor(){
         let geometry = new THREE.BoxBufferGeometry(1, 1, 1);
         let material = new THREE.MeshBasicMaterial({color: 0xe33b3b});
         this.mesh = new THREE.Mesh( geometry, material );
     }
     
     getChild(){
         return this;
     }
 
     render(){
         ...
     }
 
 }
 
 ```
 
 ### Add something to the ThreeJS Scene
 1. Check if your model is present in `AssetsToLoad.js` with a name and a path to the `.dae`, if not, add it
 2. Make sure you import the corresponding file in `AppController.js`
 3. Go to the method `loadModels()`
 4. Create your object as a new instance of the its corresponding class 
 
 #### Example
 * In `AssetsToLoad.js`
 ```
 {
     name: 'air1',
     path: 'assets/models/planet/air/planete_air_1.dae'
 }
 ```
 
 * In `AppController.js`
 
 ```
 import Planet from '../models/planet/Planet';
 ``` 
 
 * In `loadModel(el){}` create a new instance of your object, following the description in its corresponding class
 ```
 let firePlanet = new Planet({
     name: 'air1',
     clock: this.clock,
     datGui: this.datGui,
     planetType: "air",
     shader: 'airplanet1',
     planetId:3
 });
 ```
 
 And don't forget to ad the current element to the scene with
 ```
 this.addElement(firePlanet);
 ``` 
 It will push it through an array of all elements that THREEJS must render, and call for each of theme their render methods (in their class)
 
 ### Playing around with the Material Editor
 1. In `AppController.js` add `import MaterialEditor from '../managers/MaterialEditorManager';`
 2. In `loadModels(el){}` add a new instance of MaterialEditor
  ```
  let materialEditor = new MaterialEditor({
      datGui: this.datGui,
      scene: this.scene
  });
 
  this.addElement(materialEditor);
  ``` 
 You will have access to a bunch of parameters to play with
 
 * TODO : Add Reflection an Refraction mechanism
 
 ### Playing with Planets and Shaders
 * always a shader by planet for the moment !
 TODO : add planet without shader
 
 1. In `ShadersToLoad.js` check if your shader already exists
 2. in `AppController.js` in `loadModels(){}` add your Planet and respect this parameters format :
 
 ```
 let waterPlanet1 = new Planet({
     name: 'water1',
     clock: this.clock,
     datGui: this.datGui,
     id:1,
     planetType:'water',
     applyShaderToMesh:'ocean',
     shaderScaleFactor: 2,
     uniforms:{
         time:{
             type:'f',
             value:0.0
         },
         tExplosion:{
             type:'t',
             value: THREE.ImageUtils.loadTexture('./shaders/sources/assets/vague.png')
         }
     },
     renderUniforms:function(scope){
         scope.uniforms.time.value = 0.00025 * (Date.now() - this.t0);
     }
 });
 ```
 
 
 
