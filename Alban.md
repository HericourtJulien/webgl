Dossier Technique

Problèmes rencontrés : 

* Galaxie chargement : 
    * Pour charger la galaxie, nous réimportions les models 3D pour chaque planète affichée. Cela provoquait des erreurs et les models ne s'afichaient plus correctement.
Ceci provoquait aussi des grosses latences au chargement d'une planète.
    * La solutions a été de repenser entièrement notre loader, qui charge maintenant toutes les ressources necessaires au chargement du site et les mets dans une variale global qui sera utilisée pour les chargement de models 3D.

* Compilation :
    * Temps de compilation
        * Pour la compilation de nos fichiers (scss/JS[ES6]/html), nous utilision Gulp mais toutes les compilations prenaient environ 3/4 minutes pour se terminer. En effet, GULP re-compilait l'ensemble de nos ressources (dont THREE.JS).
        * La solution à été d'utiliser GULP-BRO, qui compile uniquement les lignes de code modifiées de manière incrémentale. La compilation est passé à quelques secondes.
    
    * Latences à la compilation
        * Lors d'une compilation, l'IDE indexait tous les dossiers par défault, provoquant des grosses latences et parfois le plantage de l'IDE.
        * La solution à été d'empecher les dossiers `node_modules` et `dist` d'être indexés par l'IDE, allégant les indexations.

* Shaders :  
    * Les shaders étant en GL/SL doivent être importés en tant que chaine de caractères. Nous crééions un String à chaque nouveau shader. Mais sa modification étant compliquée et longue pour ne pas faire d'erreur.
    * Nous avons alors choisit d'utiliser GULP-GLSLIFY qui nous permet de coder les shaders dans des fichiers a part et des exporter en tant que String pour les importer dans nos fichiers JS. 
 

* DatGui
    * Beaucoup de paramètres devaient être définis visuellement avec la direction artistique.
    * Pour un gain de temps considerable, nous avons mis en place un DatGui permettant de définir les valeurs de nos paramètres de directement depuis le front avec un résultat immédiat. Il ne nous reste donc plus que de recopier les valeurs et les mettre dans le code. 


* Indico
    * L'API indico que nous utilisons n'est accessible qu'en Anglais et une de nos interactions demande à l'utilisateur de répondre à une question pour récupérer sa personnalité. Cependant, les utilisateurs répondront en Français.
    * La solution à été d'utiliser un autre API (translate) qui nous permet de traduire le texte en Anglais avant de l'envoyer à Indico.io.
    
* Matériaux
    * Les différents formats de fichiers ne gèrent pas tous de la même manières l'accès aux matériaux. Le Contexte WebGL ne permet pas non plus d'inclure tous les différents paramètres des matériaux qui peuvent être configurés dans les logiciels de modélisation 3D
    * La solution est d'utiliser exclusivement des matériaux simple, et de personnaliser les paramètres spécifiques directement dans l'environnement WebGL, soit par des shaders, soit par des matériaux créé via ThreeJS.
    
* Formats de fichiers 3D / Modélisation des assets
    * La modélisation des éléments est parfois très complexe, et les moddèles comptaient parfois plusieurs centaines de milliers de faces. 
    * Reprendre les éléments dans Blender pour gérer le nombre de faces ainsi que leur différents paramètres (origines, axes locaux et globaux, tailles, geometry des vertices et des faces, uv mapping et bump map) nous a permis de corriger les éléments et de simplifier leur utilisation. De plus, la gestion des matériaux selon les différents modes de rendu de Blender permmettent d'avoir un premier aperçu proche de celui du WebGL.
    
* Système solaire
    * Les anneaux du système solaire et leur animation était un défi technique pour permettre de faire suivre les éléments le long d'une courbe, ici une ellipse. La propriété animateMotion utilisée permet de faire suivre un élément SVG selon un *path SVG* et les deux doivent se trouver dans la même balise SVG. Il a donc fallu transformer la balise *ellipse* en *path* pour permettre cette animation.

* Date de naissance
    * Le déplacement des anneaux pour saisir sa date de naissance est réalisé en combinant un drag&drop ainsi qu'une rotation sur les anneaux qui sont en réalités des ronds transparents. La partie complexe a été de *cranter* les angles de rotation en fonction du nombre de valeur possible ( 31 jours, 12 mois etc. ). 