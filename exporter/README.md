# Electron App
Used to have a local system running webGL and be able to drag n drop file to quickly see them in WEBGL environment.

## How to

in `AppController.js`
- change `this.scene` with a `THREE.Scene()` instead of `PhysiJS.Scene()`
- remove the `this.initDataManager()` (in constructor)
- in the `render` remove `this.scene.simulate()`

run `gulp exporter` to generate a ` app.js` file.
Copy this generated file in the electron app folder  `js/scripts/`

you can test the electron system (be sure to run `npm install` before) with `npm start` of if you have electron installed globally
you can use `electron .` command
                              
### Notes
- Modify loaders class to let them be able to load a 3D Model from a file (see `COLLADALoader.js`)
- Modify AppController (normally you don't have to) to be sure Drag'n'Drop feature is here (`dragFile` method)
- Check the `Model` class  if you want to load something else than Collada 3D Model