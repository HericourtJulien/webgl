let gulp = require('gulp');
let glsl = require('gulp-glsl');
let babelify = require('babelify');
let sourcemaps = require('gulp-sourcemaps');
let bro = require('gulp-bro');
let browserSync = require('browser-sync');
let sass = require('gulp-sass');
let fileinclude = require('gulp-file-include');

gulp.task('browser-sync', function () {
    browserSync({
        server:{
            baseDir: './dist/'
        }
    })
});

gulp.task('shader', ['shader-source'], function(){
    return gulp.src(['./src/shaders/**/*.glsl'])
        .pipe(glsl({ es6: true }))
        .pipe(gulp.dest('./src/shaders/compiled/'))
});

gulp.task('shader-source', function(){
    return gulp.src('./src/shaders/sources/**/*')
        .pipe(gulp.dest('./dist/shaders/sources/'));
});

gulp.task('babel', function () {
    return gulp.src(['./src/scripts/*.js'])
        .pipe(bro({
            transform: [
                babelify.configure({ presets: ['es2015'] }),
                [ 'uglifyify', { global: true } ]
            ]
        }))
        .pipe(sourcemaps.init())
        .pipe(gulp.dest('./dist/scripts/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('sass', function(){
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(browserSync.stream())
});

gulp.task('assets', function(){
    return gulp.src('./src/assets/!(scss)/**')
        .pipe(gulp.dest('./dist/assets/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('libs-js', function(){
    return gulp.src('./src/scripts/libs/*.js')
        .pipe(gulp.dest('./dist/scripts/libs/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('exporter', function(){
    gulp.src(['./src/scripts/*.js'])
        .pipe(sourcemaps.init())
        .pipe(bro({
            transform: [
                babelify.configure({ presets: ['es2015'], compact: false })
            ]
        }))
        .pipe(gulp.dest('./exporter/'));
});

gulp.task('html', function() {
    gulp.src(['./src/index.html', './src/html_parts/forms/_astro.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('default', ['shader','babel', 'sass', 'html', 'assets', 'libs-js', 'browser-sync'], function(){
    gulp.watch('./src/shaders/**/*.glsl', ['shader', 'babel', browserSync.reload]);
    //gulp.watch('./src/shaders/sources/*', ['shader']);
    gulp.watch('./src/assets/scss/**/*.scss', ['sass']);
    gulp.watch('./src/assets/!(scss)/**/*', ['assets']);
    gulp.watch('./src/scripts/**/*.js', ['babel']);
    gulp.watch('./src/*.html', ['html', browserSync.reload]);
});
